package com.client.levelsapp

interface SimpleCalculation {
    fun apply(x1: Int, x2: Int): Int
}