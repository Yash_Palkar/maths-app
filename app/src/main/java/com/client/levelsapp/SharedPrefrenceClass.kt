package com.client.levelsapp

import android.content.Context
import android.content.SharedPreferences
import java.util.ArrayList
import android.text.method.TextKeyListener.clear



class SharedPrefrenceClass {
    private var sharedPreferences: SharedPreferences? = null
    private var context: Context? = null
    var editor: SharedPreferences.Editor? = null
    internal var levelLockList = ArrayList<Int>()
    internal var achievementLockList = ArrayList<Int>()


    constructor(context: Context)  {
        this.context = context
        sharedPreferences = context.getSharedPreferences(
            context.resources.getString(R.string.first_installed),
            Context.MODE_PRIVATE
        )
    }

    fun clearpreference()
    {
        val pref = context!!.getSharedPreferences(context!!.resources.getString(R.string.first_installed), Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.clear()
        editor.commit()
    }
    fun writeLevelscoreeasy(level: Int,score:Int) {
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListB$i", 0))
        }
        levelLockList.addAll(newList)
        levelLockList.set(level,score)
        editor = sharedPreferences!!.edit()
        editor!!.putInt("KeyOFListB$level", levelLockList.get(level))
        editor!!.commit()
    }

    fun readLevelscoreeasy(level: Int): Int {
        var level_locked = 0
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListB$i", 0))
        }
        level_locked = newList[level]
        return level_locked
    }


    fun writeachievement(achievement_level: Int,value:Int) {
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListAch$i", 0))
        }
        achievementLockList.addAll(newList)
        achievementLockList.set(achievement_level,value)
        editor = sharedPreferences!!.edit()
        editor!!.putInt("KeyOFListAch$achievement_level", achievementLockList.get(achievement_level))
        editor!!.commit()
    }

    fun readachievement(level: Int): Int {
        var level_locked = 0
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListAch$i", 0))
        }
        level_locked = newList[level]
        return level_locked
    }
    fun writeLevelscoretricky(level: Int,score:Int) {
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListTricky$i", 0))
        }
        levelLockList.addAll(newList)
        levelLockList.set(level,score)
        editor = sharedPreferences!!.edit()
        editor!!.putInt("KeyOFListTricky$level", levelLockList.get(level))
        editor!!.commit()
    }

    fun readLevelscoretricky(level: Int): Int {
        var level_locked = 0
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListTricky$i", 0))
        }
        level_locked = newList[level]
        return level_locked
    }

    fun writeLevelstar(level: Int,value:Int) {
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListStar$i", 0))
        }
        levelLockList.addAll(newList)
        levelLockList.set(level,value)
        editor = sharedPreferences!!.edit()
        editor!!.putInt("KeyOFListStar$level", levelLockList.get(level))
        editor!!.commit()
    }

    fun readLevelstar(level: Int): Int {
        var level_locked = 0
        val newList = ArrayList<Int>()
        for (i in 0..17) {
            newList.add(sharedPreferences!!.getInt("KeyOFListStar$i", 0))
        }
        level_locked = newList[level]
        return level_locked
    }




}