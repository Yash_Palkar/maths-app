package com.client.levelsapp


import android.content.Context
import android.widget.TextView
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


class QuestionAdapter(
    private val horizontalItemList: List<String>,
    internal var context: Context
) : RecyclerView.Adapter<QuestionAdapter.ItemViewHolder>() {
    override fun onBindViewHolder(p0: QuestionAdapter.ItemViewHolder, p1: Int) {

        p0.txtview.setText(horizontalItemList[p1])

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        //inflate the layout file
        val groceryProductView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.question_number_item, parent, false)
        return ItemViewHolder(groceryProductView)
    }

    override fun getItemCount(): Int {
        return horizontalItemList.size
    }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var txtview: TextView

        init {
            txtview = view.findViewById(R.id.txt_item)
        }
    }
}