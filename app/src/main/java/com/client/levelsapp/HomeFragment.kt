package com.client.levelsapp

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    var mActivity: Activity? = null
    var level_title = ArrayList(
        Arrays.asList(
            "Level 1",
            "Level 2",
            "Level 3",
            "Level 4",
            "Level 5",
            "Level 6",
            "Level 7",
            "Level 8",
            "Level 9",
            "Level 10",
           "Level 11",
            "Level 12",
            "Level 13",
            "Level 14",
            "Level 15",
            "Level 16",
            "Level 17",
            "Level 18"

        )
    )
    var level_difficulty = ArrayList(
        Arrays.asList(
            "Easy",
            "Easy",
            "Easy",
            "Easy",
            "Easy",
            "Easy",
            "Medium",
            "Medium",
            "Medium",
            "Medium",
            "Medium",
            "Medium",
            "Hard",
            "Hard",
            "Hard",
            "Hard",
            "Hard",
            "Tricky"

        )
    )
    var level_category = ArrayList(
        Arrays.asList(
            "+",
            "-",
            "*",
            "/",
            "+ +",
            "- -",
            "* *",
            "/ /",
            "+ -",
            "+ *",
            "+ /",
            "- *",
            "- /",
            "* /",
            "+ - *",
            "- * /",
            "* / +",
            "+ - * /"

        )
    )


    companion object {
        var homefragment: HomeFragment? = null
        fun getInstance(): HomeFragment {
            homefragment = HomeFragment()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.home_fragment, container, false)
        var recyclerView = view.findViewById(R.id.recycler_menu) as RecyclerView
//        var gridLayoutManager = GridLayoutManager(context, LinearLayoutManager.VERTICAL)
//        recyclerView.setLayoutManager(gridLayoutManager)
        recyclerView.layoutManager = StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL)
        var customAdapter =
            HomeFragmentAdapter(context!!, level_title, level_category, level_difficulty)
        recyclerView.setAdapter(customAdapter)
        return view
    }


}