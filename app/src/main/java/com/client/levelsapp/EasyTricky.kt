package com.client.levelsapp

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.client.levelsapp.MainActivity.Companion.getLevelNumber
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardedVideoAd

class EasyTricky : AppCompatActivity() {

    var mActivity: Activity? = null
    internal var rewardedVideoAd: RewardedVideoAd? = null
    private var interstitial: InterstitialAd? = null
    internal lateinit var adIRequest: AdRequest
    var level_no: Int? = null
    var myValue: String? = null
    var medium: String? = null
    var mybundle:Bundle?=null
    var redirect:String?=null
    var level:String?=null
    var easy:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_easy_tricky)
        MobileAds.initialize(this, resources.getString(R.string.banner_app_id))
        val mAdView: AdView = findViewById(R.id.banner_ad)
        adIRequest = AdRequest.Builder().build()
        mAdView.loadAd(adIRequest)
        loadInterstitialAd()
        var txt_title_name = findViewById(R.id.txt_title_name) as TextView
        var txt_easy = findViewById(R.id.txt_easy) as TextView
        var txt_tricky = findViewById(R.id.txt_tricky) as TextView
        var txt_highscores = findViewById(R.id.txt_highscores) as TextView

        mybundle = intent.extras
//
//        medium = bundle!!.getString("medium")
        Log.d("title", "myValue")

        // intent = intent
        if (mybundle != null) {
            if (mybundle!!.getString("redirect") != null) {
                redirect = mybundle!!.getString("redirect")!!
            }
            if (mybundle!!.getString("title") != null) {
                title = mybundle!!.getString("title")!!
            }
            if (mybundle!!.getString("level") != null) {
                level = mybundle!!.getString("level")!!
            }
            if (mybundle!!.getInt("level_no") != null) {
                level_no = mybundle!!.getInt("level_no")!!
            }
            if (mybundle!!.getString("easy") != null) {
                easy = mybundle!!.getString("easy")!!
            }
            if (mybundle!!.getString("medium") != null) {
                medium = mybundle!!.getString("medium")!!
            }

        }



        myValue = mybundle!!.getString("title")


        txt_title_name.text = MainActivity.setName(myValue!!)

//        val myValue = this.arguments!!.getString("title")
//        if (bundle!!.getInt("getLevelNumber") != null) {
        level_no = mybundle!!.getInt("getLevelNumber")
//        }

        if (level_no!! >= 4) {
            txt_tricky.visibility = View.GONE
        } else {
            txt_tricky.visibility = View.VISIBLE
        }

        txt_tricky.setOnClickListener {
            //            var intent = Intent(applicationContext, MainActivity::class.java)
//            intent.putExtra("redirect", "3")
//            intent.putExtra("title", myValue)
//            intent.putExtra("level", "tricky")
//            intent.putExtra("level_no", "level_no")
//            startActivity(intent)
//
            var intent = Intent(this, TrickyActivity::class.java)
            intent.putExtra("title", myValue)
            intent.putExtra("level", "tricky")
            intent.putExtra("level_no",  getLevelNumber(myValue!!))
            startActivity(intent)
            finish()
            displayInterstitial()
        }

        txt_easy.setOnClickListener {
            var intent = Intent(this, QuestionView::class.java)
            intent.putExtra("title", myValue)
            intent.putExtra("level", "easy")
            intent.putExtra("level_no", getLevelNumber(myValue!!))
            startActivity(intent)
            finish()
//            var intent = Intent(this, MainActivity::class.java)
//            intent.putExtra("redirect", "1")
//            intent.putExtra("title", myValue)
//            intent.putExtra("level", "easy")
//            intent.putExtra("level_no", "level_no")
//          startActivity(intent)
            displayInterstitial()
        }

        txt_highscores.setOnClickListener {

            var intent = Intent(this, MainActivity::class.java)
            intent.putExtra("redirect", "4")
            intent.putExtra("title", myValue)
            intent.putExtra("level", "easy")
            intent.putExtra("level", "medium")
            intent.putExtra("level_no", level_no!!)
            startActivity(intent)
            finish()
            displayInterstitial()
        }

    }

    private fun loadInterstitialAd() {
        interstitial = InterstitialAd(applicationContext)
        interstitial!!.adUnitId = getString(R.string.interstitial_ad)
        interstitial!!.loadAd(adIRequest)
    }

    private fun displayInterstitial() {
        if (interstitial!!.isLoaded) {
            Log.d("TAG", "displayInterstitial")
            interstitial!!.show()
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
      //  var intent = (Intent(this, LevelViewActivity::class.java))

       //   intent.putExtra("title", myValue)
//        //intent.putExtra("level", easy)
//        //intent.putExtra("level", medium)
//        intent.putExtra("level_no", level_no!!)


       // startActivity(intent)


      // finish()
    }
}
