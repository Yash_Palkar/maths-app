package com.client.levelsapp

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v4.app.Fragment
import java.util.*
import android.os.CountDownTimer
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.util.Log

import com.github.ybq.android.spinkit.style.Circle
import kotlinx.android.synthetic.main.questionview_fragment.*
import kotlin.collections.ArrayList
import android.support.v4.os.HandlerCompat.postDelayed
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.util.DisplayMetrics
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import android.widget.*
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.wang.avi.AVLoadingIndicatorView
import kotlin.math.roundToInt


class QuestionViewFragment : Fragment(), View.OnClickListener, RewardedVideoAdListener {
    private var TIME_LEFT: Long = 2000
    var counter: Int = 0
    var timer: CountDownTimer? = null
    var mActivity: Activity? = null
    var txt_timer: TextView? = null
    var emoji: ImageView? = null
    //    var txt_number1: TextView? = null
//    var txt_number2: TextView? = null
//    var txt_number3: TextView? = null
//    var txt_number4: TextView? = null
//    var txt_number5: TextView? = null
    var txt_scorename: TextView? = null
    var recycler_view: RecyclerView? = null
    //    var txt_sign: TextView? = null
//    var txt_sign1: TextView? = null
//    var txt_sign2: TextView? = null
//    var txt_sign3: TextView? = null
    var option1: TextView? = null
    var option2: TextView? = null
    var option3: TextView? = null
    var option4: TextView? = null
    var btn_dismiss: Button? = null
    var txt_question: TextView? = null
    var txt_correct_count: TextView? = null
    var txt_incorrect_count: TextView? = null
    var txt_correct: ImageView? = null
    var txt_incorrect: ImageView? = null;
    var i = 1
    var right_mp: MediaPlayer? = null
    var wrong_mp: MediaPlayer? = null
    var right_animation: Animation? = null
    var wrong_animation: Animation? = null

    private var totalTime: Int = 0
    var correct_count = 0
    var incorrect_count = 0
    var myValue = ""
    var myLevel = ""
    var random: Random? = null
    var numberOne: Int = 0
    var numberTwo: Int = 0
    var numberThree: Int = 0
    var numberfour: Int = 0
    var numberfive: Int = 0
    var final_ans = 0
    var final_ans_float = 0f
    var timer_count = 0
    var myLevel_no = 0
    var statusText: TextView? = null
    var prefConfig: SharedPrefrenceClass? = null
    var avi: AVLoadingIndicatorView? = null
    var questionAdapter: QuestionAdapter? = null
    var itemlist: ArrayList<String>? = null
    var dialog: Dialog? = null
    var q_img_star1: ImageView? = null
    var q_img_star2: ImageView? = null
    var q_img_star3: ImageView? = null
    var added_txt_easy: Button? = null
    var cons_easy_view: ConstraintLayout? = null
    var dialog1: Dialog? = null
    var dialog2: Dialog? = null
    internal var rewardedVideoAd: RewardedVideoAd? = null
    lateinit var myContext: Context

    companion object {
        var homefragment: QuestionViewFragment? = null
        fun getInstance(): QuestionViewFragment {
            homefragment = QuestionViewFragment()

            return homefragment!!
        }
    }

    override fun onStop() {
        super.onStop()
        if (dialog == null || !dialog!!.isShowing)
            setBackDialog()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
//
        myContext = activity!!
        MobileAds.initialize(context, resources.getString(R.string.admob_app_id))
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)
        rewardedVideoAd!!.rewardedVideoAdListener = this
        loadRewardedVideoAd()
        val view: View = inflater.inflate(R.layout.questionview_fragment, container, false)
//        var txt_title_name = view.findViewById(R.id.txt_title_name) as TextView
        myValue = this.arguments!!.getString("title").toString()
        myLevel = this.arguments!!.getString("level").toString()
        myLevel_no = this.arguments!!.getInt("level_no")
        recycler_view = view.findViewById(R.id.recycler_view)
        cons_easy_view = view.findViewById(R.id.cons_easy_view)
        statusText = view.findViewById(R.id.statusId)
        emoji = view.findViewById(R.id.emojiId)
        avi = view.findViewById(R.id.avi) as AVLoadingIndicatorView
        avi!!.show()


        when (myLevel_no) {
            0 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            1 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            2 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            3 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            4 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            5 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            6 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            7 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            8 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            9 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            10 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            11 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            12 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            13 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            14 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            15 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            16 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
            17 -> cons_easy_view!!.setBackgroundResource(R.drawable.mathbg)
        }
        itemlist = ArrayList()
//        txt_number1 = view.findViewById(R.id.txt_number1) as TextView
//        txt_number2 = view.findViewById(R.id.txt_number2) as TextView
//        txt_number3 = view.findViewById(R.id.txt_number3) as TextView
//        txt_number4 = view.findViewById(R.id.txt_number4) as TextView
//        txt_number5 = view.findViewById(R.id.txt_number5) as TextView
//
//        txt_number1!!.visibility = View.GONE
//        txt_number2!!.visibility = View.GONE
//        txt_number3!!.visibility = View.GONE
//        txt_number4!!.visibility = View.GONE
//        txt_number5!!.visibility = View.GONE

        txt_scorename = view.findViewById(R.id.txt_scorename) as TextView
        option1 = view.findViewById(R.id.txt_option1) as TextView
        option2 = view.findViewById(R.id.txt_option2) as TextView
        option3 = view.findViewById(R.id.txt_option3) as TextView
        option4 = view.findViewById(R.id.txt_option4) as TextView

        q_img_star1 = view.findViewById(R.id.q_img_star1) as ImageView
        q_img_star2 = view.findViewById(R.id.q_img_star2) as ImageView
        q_img_star3 = view.findViewById(R.id.q_img_star3) as ImageView
//        txt_sign = view.findViewById(R.id.txt_sign) as TextView

//        txt_sign1 = view.findViewById(R.id.txt_sign1) as TextView
//        txt_sign2 = view.findViewById(R.id.txt_sign2) as TextView
//        txt_sign3 = view.findViewById(R.id.txt_sign3) as TextView
//
//        txt_sign!!.visibility = View.GONE
//        txt_sign1!!.visibility = View.GONE
//        txt_sign2!!.visibility = View.GONE
//        txt_sign3!!.visibility = View.GONE


        txt_question = view.findViewById(R.id.txt_question) as TextView
        txt_correct_count = view.findViewById(R.id.txt_correct_count) as TextView
        txt_incorrect = view.findViewById(R.id.txt_incorrect);
//        txt_incorrect!!.setBackgroundColor(resources.getColor(R.color.tablebtcolor))
        txt_incorrect_count = view.findViewById(R.id.txt_incorrect_count) as TextView
        txt_correct = view.findViewById(R.id.txt_correct)
//        txt_correct!!.setBackgroundColor(resources.getColor(R.color.achivbt1color))
        added_txt_easy = view.findViewById(R.id.btn_text) as Button

        prefConfig = SharedPrefrenceClass(context!!)
        option1!!.setOnClickListener(this)
        option2!!.setOnClickListener(this)
        option3!!.setOnClickListener(this)
        option4!!.setOnClickListener(this)
        random = GenerateRandomNumber.getInstance().initRandom()
//        var txt_easy = view.findViewById(R.id.txt_easy) as TextView
        txt_timer = view.findViewById(R.id.txt_timer) as TextView
        val img_pause = view.findViewById(R.id.img_pause) as ImageView
        img_pause.setOnClickListener {
            setBackDialog()
        }
//        val img = view.findViewById(R.id.img_progress) as ImageView
        val progressBar = view.findViewById(R.id.spin_kits) as ProgressBar
        val circle = Circle()
        progressBar.indeterminateDrawable = circle as Drawable?

        val handler = Handler()
        handler.postDelayed(Runnable {
            avi!!.hide()
            option1!!.visibility = View.VISIBLE
            option2!!.visibility = View.VISIBLE
            option3!!.visibility = View.VISIBLE
            option4!!.visibility = View.VISIBLE
            if (mActivity != null && isVisible){
                setQuestion()
            }
        }, 4000)


//        val myValue = this.arguments!!.getString("title")
//        txt_title_name.text=myValue
//     updateCountdownText()

        return view
    }

    override fun onClick(v: View?) {
        when (myValue) {
            "+", "-", "*", "+ +", "- -", "* *", "+ -", "+ *", "- *", "+ - *" -> {
                when (v!!.id) {
                    R.id.txt_option1 -> setscoreInt(
                        txt_option1,
                        txt_option1.text.toString(),
                        final_ans
                    )
                    R.id.txt_option2 -> setscoreInt(
                        txt_option2,
                        txt_option2.text.toString(),
                        final_ans
                    )
                    R.id.txt_option3 -> setscoreInt(
                        txt_option3,
                        txt_option3.text.toString(),
                        final_ans
                    )
                    R.id.txt_option4 -> setscoreInt(
                        txt_option4,
                        txt_option4.text.toString(),
                        final_ans
                    )
                }
            }
            "/", "/ /", "+ /", "* /", "- * /", "* / +", "+ - * /" -> {
                when (v!!.id) {
                    R.id.txt_option1 -> setscoreFloat(
                        txt_option1,
                        txt_option1.text.toString(),
                        final_ans_float
                    )
                    R.id.txt_option2 -> setscoreFloat(
                        txt_option2,
                        txt_option2.text.toString(),
                        final_ans_float
                    )
                    R.id.txt_option3 -> setscoreFloat(
                        txt_option3,
                        txt_option3.text.toString(),
                        final_ans_float
                    )
                    R.id.txt_option4 -> setscoreFloat(
                        txt_option4,
                        txt_option4.text.toString(),
                        final_ans_float
                    )
                }
            }
        }
        txt_option1.isEnabled = false
        txt_option2.isEnabled = false
        txt_option3.isEnabled = false
        txt_option4.isEnabled = false
    }

    fun tapToAnimate() {

    }

    fun setAnimation() {
        var slide: Animation? = null
        slide = TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
            0.0f, Animation.RELATIVE_TO_SELF, -15.0f
        )

        slide.duration = 1000
        slide.fillAfter = true
        slide.isFillEnabled = true
        added_txt_easy!!.startAnimation(slide)

        slide.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                added_txt_easy!!.clearAnimation()
                added_txt_easy!!.visibility = View.GONE
            }

            override fun onAnimationStart(animation: Animation?) {

            }


        });
//        var animation = TranslateAnimation(0F, 0F, -50F, 0F)
//        animation.setDuration(700)
//        animation.setFillAfter(false)
//        animation.setAnimationListener(MyAnimationListener())
//        added_txt!!.startAnimation(animation)
    }

    override fun onPause() {
        super.onPause()

    }

    fun setstar(finalscore: Int) {
//        var star_count = 0
        if (finalscore >= 300) {
//            star_count = 1
            q_img_star1!!.setImageResource(R.drawable.green_star)
        } else {
            q_img_star1!!.setImageResource(R.drawable.black_star)
        }
        if (finalscore >= 600) {
//            star_count = star_count + 1
            q_img_star2!!.setImageResource(R.drawable.green_star)
        } else {
            q_img_star2!!.setImageResource(R.drawable.black_star)
        }
        if (finalscore >= 900) {
//            star_count = star_count + 1
            q_img_star3!!.setImageResource(R.drawable.green_star)
        } else {
            q_img_star3!!.setImageResource(R.drawable.black_star)
        }
//        prefConfig!!.writeLevelstar(myLevel_no, star_count)

    }

    fun setscoreInt(txtview: TextView, selected_option: String, answer: Int) {
        if (selected_option.toInt().equals(answer)) {
            txtview.setTextColor(context!!.resources.getColor(R.color.green))
            correct_count = correct_count + 1
            txt_correct_count!!.text = correct_count.toString()
            right_animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in)
            txt_correct!!.startAnimation(right_animation)
//           to do by sachin for right answer sound
            right_mp = MediaPlayer.create(view!!.context, R.raw.rightanswer)
            right_mp!!.start()
//
            var timervalue = txt_timer!!.text.toString()

            val parts = txt_scorename!!.text.toString().split(":")
            var score_old = parts[1].trim()


            var timer = timervalue.toInt() * 2


            added_txt_easy!!.visibility = View.VISIBLE
            added_txt_easy!!.setText("" + timer)
            setAnimation()
            var finalscore = score_old.toInt() + timer
            txt_scorename!!.text = "Score: " + finalscore.toString()
            setstar(finalscore)
            if (finalscore > prefConfig!!.readLevelscoreeasy(myLevel_no)) {
                prefConfig!!.writeLevelscoreeasy(myLevel_no, finalscore)
            }
        } else {
            txtview.setTextColor(context!!.resources.getColor(R.color.red))
            incorrect_count = incorrect_count + 1
            txt_incorrect_count!!.text = incorrect_count.toString()
            // to do by sachin for wrong  answer sound
            wrong_animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in)
            txt_incorrect!!.startAnimation(wrong_animation)
            wrong_mp = MediaPlayer.create(view!!.context, R.raw.wronganswer)
            wrong_mp!!.start()
        }
        var total = correct_count + incorrect_count
        var percentage: Int = ((correct_count * 100 / total))

        if (percentage >= 75 && percentage <= 100) {

            statusText!!.text = ("VeryGood").toString()
            emoji!!.setImageResource(R.drawable.supersym)

        } else if (percentage >= 50 && percentage < 75) {
            statusText!!.text = ("Good").toString()
            emoji!!.setImageResource(R.drawable.verygoodsym)
        } else if (percentage >= 25 && percentage < 50) {
            statusText!!.text = ("Bad").toString()
            emoji!!.setImageResource(R.drawable.goodsym)
        } else if (percentage >= 0 && percentage < 25) {
            statusText!!.text = ("VeryBad").toString()
            emoji!!.setImageResource(R.drawable.badsym)
        }
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }

        Handler().postDelayed({
            if (mActivity != null && isVisible){
                setQuestion()
            }
        }, 1000)
    }

    fun setscoreFloat(txtview: TextView, selected_option: String, answer: Float) {
        if (selected_option.toFloat().equals(answer)) {
            txtview.setTextColor(context!!.resources.getColor(R.color.green))
            correct_count = correct_count + 1
            txt_correct_count!!.text = correct_count.toString()
            right_animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in)
            txt_correct!!.startAnimation(right_animation)
//           to do by sachin for right answer sound
            right_mp = MediaPlayer.create(view!!.context, R.raw.rightanswer)
            right_mp!!.start()

            var timervalue = txt_timer!!.text.toString()


            val parts = txt_scorename!!.text.toString().split(":")
            var score_old = parts[1].trim()
            var timer = timervalue.toInt() * 2
            added_txt_easy!!.visibility = View.VISIBLE
            added_txt_easy!!.setText("" + timer)
            setAnimation()
            var finalscore = score_old.toInt() + timer
            txt_scorename!!.text = "Score: " + finalscore.toString()
            setstar(finalscore)
            if (finalscore > prefConfig!!.readLevelscoreeasy(myLevel_no)) {
                prefConfig!!.writeLevelscoreeasy(myLevel_no, finalscore)
            }
        } else {
            txtview.setTextColor(context!!.resources.getColor(R.color.red))
            incorrect_count = incorrect_count + 1
            txt_incorrect_count!!.text = incorrect_count.toString()
            // to do by sachin sound added
            wrong_animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in)
            txt_incorrect!!.startAnimation(wrong_animation)
            wrong_mp = MediaPlayer.create(view!!.context, R.raw.wronganswer)
            wrong_mp!!.start()

        }
        var total = correct_count + incorrect_count
        var percentage: Int = ((correct_count * 100 / total))

        if (percentage >= 75 && percentage <= 100) {

            statusText!!.text = ("VeryGood").toString()
            emoji!!.setImageResource(R.drawable.supersym)

        } else if (percentage >= 50 && percentage < 75) {
            statusText!!.text = ("Good").toString()
            emoji!!.setImageResource(R.drawable.verygoodsym)


        } else if (percentage >= 25 && percentage < 50) {
            statusText!!.text = ("Bad").toString()
            emoji!!.setImageResource(R.drawable.goodsym)

        } else if (percentage >= 0 && percentage < 25) {
            statusText!!.text = ("VeryBad").toString()
            emoji!!.setImageResource(R.drawable.badsym)

        }
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
        Handler().postDelayed({
            if (mActivity != null && isVisible){
                setQuestion()
            }

        }, 1000)
    }

    fun setQuestion() {
        option1!!.setTextColor(context!!.resources.getColor(R.color.black))
        option2!!.setTextColor(context!!.resources.getColor(R.color.black))
        option3!!.setTextColor(context!!.resources.getColor(R.color.black))
        option4!!.setTextColor(context!!.resources.getColor(R.color.black))
        txt_option1.isEnabled = true
        txt_option2.isEnabled = true
        txt_option3.isEnabled = true
        txt_option4.isEnabled = true
        if (i <= 50) {
            var value_new = 0
            when {
                i < 5 -> value_new = 1
                i < 10 -> value_new = 2
                i < 15 -> value_new = 4
                i < 20 -> value_new = 10
                i < 25 -> value_new = 20
                i < 30 -> value_new = 30
                i < 35 -> value_new = 50
                i < 40 -> value_new = 100
                i < 45 -> value_new = 200
                else -> value_new = 500
            }
            numberOne = GenerateRandomNumber.getInstance()
                .getRandomDoubleDigitNumber(random!!, (6 * value_new), (10 * value_new))
            numberTwo = GenerateRandomNumber.getInstance()
                .getRandomDoubleDigitNumber(random!!, (1 * value_new), (5 * value_new))
            numberThree =
                GenerateRandomNumber.getInstance()
                    .getRandomDoubleDigitNumber(random!!, (value_new), (3 * value_new))
            numberfour = GenerateRandomNumber.getInstance()
                .getRandomDoubleDigitNumber(random!!, (1), (4 * value_new))
            numberfive =
                if (value_new < 10) GenerateRandomNumber.getInstance().getRandomDoubleDigitNumber(
                    random!!,
                    (1),
                    (2)
                ) else GenerateRandomNumber.getInstance().getRandomDoubleDigitNumber(
                    random!!,
                    (2),
                    (5)
                )

            when (myValue) {
                "+", "-", "*", "/" -> {
//                    txt_sign!!.visibility = View.VISIBLE
//                    txt_sign1!!.visibility = View.GONE
//                    txt_sign2!!.visibility = View.GONE
//                    txt_sign3!!.visibility = View.GONE
//                    txt_number1!!.visibility = View.VISIBLE
//                    txt_number2!!.visibility = View.VISIBLE
//                    txt_number3!!.visibility = View.GONE
//                    txt_number4!!.visibility = View.GONE
//                    txt_number5!!.visibility = View.GONE
//                    txt_sign!!.text = myValue
                    if (myValue.equals("/"))
                        final_ans_float = setName1(myValue, numberOne, numberTwo, 0, 0, 0)
                    else
                        final_ans = setName(myValue, numberOne, numberTwo, 0, 0, 0)
//                    txt_number1!!.text = numberOne.toString()
//                    txt_number2!!.text = numberTwo.toString()

                    itemlist!!.clear()
                    itemlist!!.add(numberOne.toString())
                    itemlist!!.add(myValue)
                    itemlist!!.add(numberTwo.toString())
                    questionAdapter = QuestionAdapter(itemlist!!, context!!)
                    var horizontalLayoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    recycler_view!!.setLayoutManager(horizontalLayoutManager)
                    recycler_view!!.setAdapter(questionAdapter)

                }
                "+ +", "- -", "* *", "/ /", "+ -", "+ *", "+ /", "- *", "* /" -> {
//                    txt_sign!!.visibility = View.VISIBLE
//                    txt_sign1!!.visibility = View.VISIBLE
//                    txt_sign2!!.visibility = View.GONE
//                    txt_sign3!!.visibility = View.GONE
//                    txt_number1!!.visibility = View.VISIBLE
//                    txt_number2!!.visibility = View.VISIBLE
//                    txt_number3!!.visibility = View.VISIBLE
//                    txt_number4!!.visibility = View.GONE
//                    txt_number5!!.visibility = View.GONE
                    val parts =
                        myValue.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val part1 = parts[0] // +
                    val part2 = parts[1] // +
//                    txt_sign!!.text = part1
//                    txt_sign1!!.text = part2

                    if (myValue.equals("/ /") || myValue.equals("+ /") || myValue.equals("* /"))
                        final_ans_float = setName1(myValue, numberOne, numberTwo, numberThree, 0, 0)
                    else
                        final_ans = setName(myValue, numberOne, numberTwo, numberThree, 0, 0)
//                    txt_number1!!.text = numberOne.toString()
//                    txt_number2!!.text = numberTwo.toString()
//                    txt_number3!!.text = numberThree.toString()
                    itemlist!!.clear()
                    itemlist!!.add(numberOne.toString())
                    itemlist!!.add(part1)
                    itemlist!!.add(numberTwo.toString())
                    itemlist!!.add(part2)
                    itemlist!!.add(numberThree.toString())

                    questionAdapter = QuestionAdapter(itemlist!!, context!!)
                    var horizontalLayoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    recycler_view!!.setLayoutManager(horizontalLayoutManager)
                    recycler_view!!.setAdapter(questionAdapter)



                    txt_question!!.setText("" + i + "/50")
                    when {
                        i < 5 -> counter = 10000
                        i < 10 -> counter = 8000
                        i < 15 -> counter = 11000
                        i < 20 -> counter = 16000
                        i < 30 -> counter = 21000
                        else -> counter = 21000
                    }


                }



                "+ - *", "- * /", "* / +" -> {
//                    txt_sign!!.visibility = View.VISIBLE
//                    txt_sign1!!.visibility = View.VISIBLE
//                    txt_sign2!!.visibility = View.VISIBLE
//                    txt_sign3!!.visibility = View.GONE
//                    txt_number1!!.visibility = View.VISIBLE
//                    txt_number2!!.visibility = View.VISIBLE
//                    txt_number3!!.visibility = View.VISIBLE
//                    txt_number4!!.visibility = View.VISIBLE
//                    txt_number5!!.visibility = View.GONE
                    val parts =
                        myValue.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val part1 = parts[0] // +
                    val part2 = parts[1] // -
                    val part3 = parts[2] // *
//                    txt_sign!!.text = part1
//                    txt_sign1!!.text = part2
//                    txt_sign2!!.text = part3

                    if (myValue.equals("- * /") || myValue.equals("* / +"))
                        final_ans_float =
                            setName1(myValue, numberOne, numberTwo, numberThree, numberfour, 0)
                    else
                        final_ans =
                            setName(myValue, numberOne, numberTwo, numberThree, numberfour, 0)
//                    }
//                    txt_number1!!.text = numberOne.toString()
//                    txt_number2!!.text = numberTwo.toString()
//                    txt_number3!!.text = numberThree.toString()
//                    txt_number4!!.text = numberfour.toString()
                    itemlist!!.clear()
                    itemlist!!.add(numberOne.toString())
                    itemlist!!.add(part1)
                    itemlist!!.add(numberTwo.toString())
                    itemlist!!.add(part2)
                    itemlist!!.add(numberThree.toString())
                    itemlist!!.add(part3)
                    itemlist!!.add(numberfour.toString())

                    questionAdapter = QuestionAdapter(itemlist!!, context!!)
                    var horizontalLayoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    recycler_view!!.setLayoutManager(horizontalLayoutManager)
                    recycler_view!!.setAdapter(questionAdapter)
                }
                "+ - * /" -> {
//                    txt_sign!!.visibility = View.VISIBLE
//                    txt_sign1!!.visibility = View.VISIBLE
//                    txt_sign2!!.visibility = View.VISIBLE
//                    txt_sign3!!.visibility = View.VISIBLE
//                    txt_number1!!.visibility = View.VISIBLE
//                    txt_number2!!.visibility = View.VISIBLE
//                    txt_number3!!.visibility = View.VISIBLE
//                    txt_number4!!.visibility = View.VISIBLE
//                    txt_number5!!.visibility = View.VISIBLE

                    val parts =
                        myValue.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val part1 = parts[0] // +
                    val part2 = parts[1] // -
                    val part3 = parts[2] // *
                    val part4 = parts[3] // /
//                    txt_sign!!.text = part1
//                    txt_sign1!!.text = part2
//                    txt_sign2!!.text = part3
//                    txt_sign3!!.text = part4
                    final_ans_float =
                        setName1(myValue, numberOne, numberTwo, numberThree, numberfour, numberfive)
//                    txt_number1!!.text = numberOne.toString()
//                    txt_number2!!.text = numberTwo.toString()
//                    txt_number3!!.text = numberThree.toString()
//                    txt_number4!!.text = numberfour.toString()
//                    txt_number5!!.text = numberfive.toString()
                    itemlist!!.clear()
                    itemlist!!.add(numberOne.toString())
                    itemlist!!.add(part1)
                    itemlist!!.add(numberTwo.toString())
                    itemlist!!.add(part2)
                    itemlist!!.add(numberThree.toString())
                    itemlist!!.add(part3)
                    itemlist!!.add(numberfour.toString())
                    itemlist!!.add(part4)
                    itemlist!!.add(numberfive.toString())


                    questionAdapter = QuestionAdapter(itemlist!!, context!!)
                    var horizontalLayoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    recycler_view!!.setLayoutManager(horizontalLayoutManager)
                    recycler_view!!.setAdapter(questionAdapter)
                }
            }
            val myAnswerList = ArrayList<Int>()
            val myAnswerListfloat = ArrayList<Float>()
            when (myValue) {
                "+", "-", "*","+ -", "+ *", "- *", "+ - *" -> {

                    myAnswerList.add(final_ans!! + 5)
                    myAnswerList.add(final_ans!! + 2)
                    myAnswerList.add(final_ans!! * 5)
                    myAnswerList.add(final_ans!!)
                    Collections.shuffle(myAnswerList)
                    option1!!.text = myAnswerList.get(0).toString()
                    option2!!.text = myAnswerList.get(1).toString()
                    option3!!.text = myAnswerList.get(2).toString()
                    option4!!.text = myAnswerList.get(3).toString()

                    txt_question!!.setText("" + i + "/50")
                    when {
                        i < 5 -> counter = 6000
                        i < 10 -> counter = 8000
                        i < 15 -> counter = 11000
                        i < 20 -> counter = 16000
                        i < 30 -> counter = 21000
                        else -> counter = 21000
                    }

                }
                "+ +", "- -", "* *", "/ /" -> {

                    myAnswerList.add(final_ans!! + 5)
                    myAnswerList.add(final_ans!! + 2)
                    myAnswerList.add(final_ans!! * 5)
                    myAnswerList.add(final_ans!!)
                    Collections.shuffle(myAnswerList)
                    option1!!.text = myAnswerList.get(0).toString()
                    option2!!.text = myAnswerList.get(1).toString()
                    option3!!.text = myAnswerList.get(2).toString()
                    option4!!.text = myAnswerList.get(3).toString()

                    txt_question!!.setText("" + i + "/50")
                    when {
                        i < 5 -> counter = 10000
                        i < 10 -> counter = 15000
                        i < 15 -> counter = 20000
                        i < 20 -> counter = 25000
                        i < 30 -> counter = 30000
                        else -> counter = 30000
                    }

                }
                "/",  "+ /", "* /", "- * /", "* / +", "+ - * /" -> {
                    myAnswerListfloat.add(final_ans_float!! + 50f)
                    myAnswerListfloat.add(final_ans_float!! + 20f)
                    myAnswerListfloat.add(final_ans_float!! * 50f)
                    myAnswerListfloat.add(final_ans_float!!)
                    Collections.shuffle(myAnswerListfloat)
                    option1!!.text = myAnswerListfloat.get(0).toString()
                    option2!!.text = myAnswerListfloat.get(1).toString()
                    option3!!.text = myAnswerListfloat.get(2).toString()
                    option4!!.text = myAnswerListfloat.get(3).toString()

                    txt_question!!.setText("" + i + "/50")
                    when {
                        i < 5 -> counter = 6000
                        i < 10 -> counter = 8000
                        i < 15 -> counter = 11000
                        i < 20 -> counter = 16000
                        i < 30 -> counter = 21000
                        else -> counter = 21000
                    }


                }
            }



//            txt_question!!.setText("" + i + "/50")
//            when {
//                i < 5 -> counter = 10000
//                i < 10 -> counter = 8000
//                i < 15 -> counter = 11000
//                i < 20 -> counter = 16000
//                i < 30 -> counter = 21000
//                else -> counter = 21000
//            }
            timer = object : CountDownTimer(counter.toLong(), 1000) {
                override fun onTick(millisUntilFinished: Long) {

                    txt_timer!!.setText("" + millisUntilFinished / 1000)
                }

                override fun onFinish() {
                    txt_timer!!.setText("0")
//                    if (dialog2 == null) {
//                        if (!dialog2!!.isShowing)
                    setDialog()
//                    }


                }
            }.start()
            i++
        } else {
            levelcomplete()
        }
    }

    fun levelcomplete() {

        dialog1 = context?.let { Dialog(it) }
        dialog1!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog1!!.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog1!!.setCancelable(false)
        dialog1!!.setContentView(R.layout.dialog_next_question)

        val txt_title = dialog1!!.findViewById(R.id.txt_title) as TextView
        val txt_desc = dialog1!!.findViewById(R.id.txt_desc) as TextView
        val txt_btn_next = dialog1!!.findViewById(R.id.txt_btn_next) as TextView
        txt_title.setText("Level Completed")
        txt_desc.visibility = View.GONE
//        txt_desc.setText("Collect $star star to unlock this level.")
        txt_btn_next.setText("OK")
        txt_btn_next.setOnClickListener {
            dialog1!!.dismiss()
            var i = Intent(context, MainActivity::class.java)
            i.putExtra("redirect", "0")
            i.putExtra("title", myValue)
            startActivity(i)
            if (rewardedVideoAd!!.isLoaded()) {
                Log.d("TAG", "Rewarded ad showing")
                rewardedVideoAd!!.show()
            }
        }

        dialog1!!.show()

    }

    override fun onDestroy() {
        super.onDestroy()
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    fun setName(sign: String, num1: Int, num2: Int, num3: Int, num4: Int, num5: Int): Int {
        var name: Int? = null
        when (sign) {
            "+" -> name = CalculateMethods.calculateInt(EnumClass.OperationInt.ADDITION, num1, num2)
            "-" -> name =
                CalculateMethods.calculateInt(EnumClass.OperationInt.SUBTRACTION, num1, num2)
            "*" -> name =
                CalculateMethods.calculateInt(EnumClass.OperationInt.MULTIPLICATION, num1, num2)
            "+ +" -> name =
                CalculateMethods.calculateDoubleInt(
                    EnumClass.OperationDoubleInt.DOUBLE_ADD,
                    num1,
                    num2,
                    num3
                )
            "- -" -> name =
                CalculateMethods.calculateDoubleInt(
                    EnumClass.OperationDoubleInt.DOUBLE_SUBTRACT,
                    num1,
                    num2,
                    num3
                )
            "* *" -> name =
                CalculateMethods.calculateDoubleInt(
                    EnumClass.OperationDoubleInt.DOUBLE_MULTIPLY,
                    num1,
                    num2,
                    num3
                )
            "+ -" -> name =
                CalculateMethods.calculateDoubleInt(
                    EnumClass.OperationDoubleInt.ADD_SUBTRACT,
                    num1,
                    num2,
                    num3
                )
            "+ *" -> name =
                CalculateMethods.calculateDoubleInt(
                    EnumClass.OperationDoubleInt.ADD_MULTIPLY,
                    num1,
                    num2,
                    num3
                )
            "- *" -> name =
                CalculateMethods.calculateDoubleInt(
                    EnumClass.OperationDoubleInt.SUBTRACT_MULTIPLY,
                    num1,
                    num2,
                    num3
                )
            "+ - *" -> name = CalculateMethods.calculateTripleInt(
                EnumClass.OperationTripleInt.ADD_SUBTRACT_MULTIPLY,
                num1,
                num2,
                num3,
                num4
            )
        }

        return name!!
    }

    fun setName1(sign: String, num1: Int, num2: Int, num3: Int, num4: Int, num5: Int): Float {
        var name: Float? = null
        when (sign) {

            "/" -> name =
                CalculateMethods.calculateFloat(EnumClass.OperationFloat.DIVISION, num1, num2)
            "/ /" -> name =
                CalculateMethods.calculateDoubleFloat(
                    EnumClass.OperationDoubleFloat.DOUBLE_DIVIDE,
                    num1,
                    num2,
                    num3
                )
            "+ /" -> name =
                CalculateMethods.calculateDoubleFloat(
                    EnumClass.OperationDoubleFloat.ADD_DIVIDE,
                    num1,
                    num2,
                    num3
                )
            "- /" -> name =
                CalculateMethods.calculateDoubleFloat(
                    EnumClass.OperationDoubleFloat.SUBTRACT_DIVIDE,
                    num1,
                    num2,
                    num3
                )
            "* /" -> name =
                CalculateMethods.calculateDoubleFloat(
                    EnumClass.OperationDoubleFloat.MULTIPLY_DIVIDE,
                    num1,
                    num2,
                    num3
                )
            "- * /" -> name = CalculateMethods.calculateTripleFloat(
                EnumClass.OperationTripleFloat.SUBTRACT_MULTIPLY_DIVIDE,
                num1,
                num2,
                num3,
                num4
            )
            "* / +" -> name = CalculateMethods.calculateTripleFloat(
                EnumClass.OperationTripleFloat.MULTIPLY_DIVIDE_ADD,
                num1,
                num2,
                num3,
                num4
            )
            "+ - * /" -> name =
                CalculateMethods.calculateBodmass(
                    EnumClass.OperationBodmass.BODMASS,
                    num1,
                    num2,
                    num3,
                    num4,
                    num5
                )
        }

        return name!!
    }

    fun setDialog() {
        dialog2 = activity?.let { Dialog(it) }
        dialog2!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog2!!.setCancelable(false)
        dialog2!!.setContentView(R.layout.dialog_next_question)

        val next = dialog2!!.findViewById(R.id.txt_btn_next) as TextView

        next.setOnClickListener {
            dialog2!!.dismiss()
            if (timer != null) {
                timer!!.cancel()
                timer = null
            }
            if (mActivity != null && isVisible){
                setQuestion()
            }
        }
        if (dialog2 != null && !dialog2!!.isShowing)
            dialog2!!.show()
    }

    override fun onResume() {
        super.onResume()
        if (dialog != null && !dialog!!.isShowing) {
            if (timer_count != 0) {
                timer = object : CountDownTimer(timer_count.toLong(), 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        txt_timer!!.setText("" + millisUntilFinished / 1000)
                    }

                    override fun onFinish() {
                        txt_timer!!.setText("0")

                        setDialog()
//                    }
////                    else {
////                        setDialog()
////                    }
                    }
                }.start()
            }
            else{
                Log.d("myapp","else")
            }
        }
else{
    Log.d("myapp","else")
        }
    }

    fun setBackDialog() {
        if (option1!!.visibility == View.VISIBLE) {
            if (timer != null) {
                timer!!.cancel()
                timer = null
            }
            dialog = activity?.let { Dialog(it) }
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setCancelable(false)
            dialog!!.setContentView(R.layout.back_dialog)
            var displayMetrics = DisplayMetrics()
            if (mActivity != null)
                mActivity!!.windowManager.getDefaultDisplay().getMetrics(displayMetrics)
            var displayWidth = displayMetrics.widthPixels
            var displayHeight = displayMetrics.heightPixels
            var layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog!!.getWindow()?.getAttributes())
            // var dialogWindowWidth = (displayWidth * 0.5f)
            var dialogWindowHeight = (displayHeight * 0.3f)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = dialogWindowHeight.toInt()
            dialog!!.getWindow()?.setAttributes(layoutParams)
            val img_back = dialog!!.findViewById(R.id.img_back) as ImageView
            val img_menu = dialog!!.findViewById(R.id.img_menu) as ImageView
            val img_next = dialog!!.findViewById(R.id.img_next) as ImageView
            val close = dialog!!.findViewById(R.id.close) as ImageView

            close.setOnClickListener {
                dialog!!.dismiss()
                timer_count = ((txt_timer!!.text.toString().toInt()) * 1000)
                onResume()

                if (txt_timer!!.text.equals("0")) {
                    setDialog()
                }
            }

            img_back.setOnClickListener {
                dialog!!.dismiss()
                var i = Intent(mActivity, MainActivity::class.java)
                i.putExtra("redirect", "0")
                i.putExtra("title", myValue)
                mActivity!!.startActivity(i)
                mActivity!!.finish()
            }

            img_menu.setOnClickListener {
                dialog!!.dismiss()
                startActivity(Intent(context, MainActivity::class.java))
            }
            img_next.setOnClickListener {
                dialog!!.dismiss()
                i = i++
                if (mActivity != null && isVisible){
                    setQuestion()
                }
            }
            dialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            if (dialog != null && !dialog!!.isShowing) {
                dialog!!.show()
            }

        }
    }

    private fun loadRewardedVideoAd() {
        Log.d("TAG", "Rewarded ad loading...")
        if (!rewardedVideoAd!!.isLoaded()) {
            rewardedVideoAd!!.loadAd(
                resources.getString(R.string.rewarded_video_Ad),
                AdRequest.Builder().build()
            )
        }
    }

    override fun onRewardedVideoAdClosed() {
        loadRewardedVideoAd()
    }

    override fun onRewardedVideoAdLeftApplication() {}

    override fun onRewardedVideoAdLoaded() {}

    override fun onRewardedVideoAdOpened() {}

    override fun onRewarded(p0: RewardItem?) {}

    override fun onRewardedVideoStarted() {}

    override fun onRewardedVideoAdFailedToLoad(p0: Int) {
        loadRewardedVideoAd()
    }

    override fun onRewardedVideoCompleted() {
        loadRewardedVideoAd()
    }

    fun onBackPressed() {
        setBackDialog()
    }
}















