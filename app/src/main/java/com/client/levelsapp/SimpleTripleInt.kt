package com.client.levelsapp

interface SimpleTripleInt {
    fun apply(x1: Int, x2: Int, x3: Int, x4: Int): Int
}