//package com.digitalgorkha.dgusersociety.fragment;
//
//import android.app.Activity;
//import android.app.DatePickerDialog;
//import android.app.Fragment;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.SharedPreferences;
//import android.graphics.Color;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextUtils;
//import android.text.TextWatcher;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.digitalgorkha.dgusersociety.adapter.NewVisitorAdapter;
//import com.digitalgorkha.dgusersociety.model.Visitor_Pojo;
//import com.digitalgorkha.dgusersociety.util.ConnectionDetector;
//import com.digitalgorkha.dgusersociety.util.progressDialog;
//import com.digitalgorkha.dgusersociety.web_service.APIClient;
//import com.digitalgorkha.dgusersociety.web_service.APIinterface;
//import com.digitalgorkha.dgusersociety.R;
//
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
///**
// * Created by shiv on 23-02-2016.
// */
//public class  Visitors_Fragments extends Fragment
//{
//
//    SharedPreferences sp_Login;
//    Context context;
//    Toast toast;
//    SimpleDateFormat sdf24 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//    SimpleDateFormat sdf12 = new SimpleDateFormat("dd-MM-yyyy,hh:mm a");
//    Date dt;
//    RelativeLayout rltVisitor,rlBack;
//    TextView txtTittle,rlMsg;
//    EditText edtVisitor;
//    List<Visitor_Pojo.Datum> arrVisitor;
//    RecyclerView lvVisitor;
//    NewVisitorAdapter vAdapter;
//    DrawerLayout drawer;
//    APIinterface apIinterface;
//    ConnectionDetector cd;
//    TextView txtToday,txt7days,txt30days,txtFromDate,txttodate;
//    int visiId,checkDate;
//    Calendar myCalendar = Calendar.getInstance();
//    String date_start,date_end,daysBeforeDate;
//    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//    SimpleDateFormat dftoPrint = new SimpleDateFormat("dd-MM-yyyy");
//    String formattedDate = df.format(myCalendar.getTime());
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
//    {
//        View rootview = inflater.inflate(R.layout.activity_visitor,container,false);
//        drawer =  getActivity().findViewById(R.id.drawer_layout);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        Initializer(rootview);
//        setListeners();
//        TextChangedListener();
//        setvisible(1);
//        updateDate(0);
//        return rootview;
//    }
//
//    private void updateDate(int pre_date)
//    {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(new Date());
//        cal.add(Calendar.DAY_OF_YEAR, - pre_date);
//        daysBeforeDate = df.format(cal.getTime());
//        if (cd.isConnectingToInternet())
//        {
//            VisitorRetro(daysBeforeDate,formattedDate);
//        }
//        else
//        {
//            toast = Toast.makeText(getActivity(),
//                    getResources().getString(R.string.CHECK_INTERNET_CONNECTION), Toast.LENGTH_LONG);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
//        }
//    }
//
//    private void setListeners()
//    {
//        rlBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                drawer.openDrawer(Gravity.START);
//            }
//        });
//
//        txtToday.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                setvisible(1);
//                updateDate(0);
//            }
//        });
//
//        txt7days.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setvisible(2);
//                updateDate(7);
//            }
//        });
//
//        txt30days.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setvisible(3);
//                updateDate(30);
//            }
//        });
//
//        txtFromDate.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                checkDate=0;
//                getDate();
//            }
//        });
//
//        txttodate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                checkDate=1;
//                getDate();
//            }
//        });
//    }
//
//
//
//    public void getDate()
//    {
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
//        {
//            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
//                    new DatePickerDialog.OnDateSetListener()
//                    {
//                        @Override
//                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
//                        {
//                            myCalendar.set(Calendar.YEAR, year);
//                            myCalendar.set(Calendar.MONTH, monthOfYear);
//                            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                            setDate();
//                        }
//                    }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
//            datePickerDialog.show();
//
//        }
//        else
//        {
//            final DatePicker datePicker=new DatePicker(getActivity());
//            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder.setCustomTitle(datePicker);
//            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id)
//                {
//                    dialog.dismiss();
//                }
//            });
//
//            builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id)
//                {
//                    myCalendar.set(Calendar.YEAR, datePicker.getYear());
//                    myCalendar.set(Calendar.MONTH, datePicker.getMonth());
//                    myCalendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
//                    setDate();
//                }
//            });
//            builder.show();
//        }
//    }
//
//    public void setDate()
//    {
//        if(checkDate==0)
//        {
//            setvisible(4);
//            txtFromDate.setText(dftoPrint.format(myCalendar.getTime()));
//            date_start=df.format(myCalendar.getTime());
//            checkDate=1;
//        }
//        else
//        {
//            setvisible(5);
//            txttodate.setText(dftoPrint.format(myCalendar.getTime()));
//            date_end=df.format(myCalendar.getTime());
//            checkDate=0;
//        }
//
//        if(!TextUtils.isEmpty(date_start) && !TextUtils.isEmpty(date_end))
//        {
//            if (cd.isConnectingToInternet())
//            {
//                VisitorRetro(date_start, date_end);
//            }
//            else
//            {
//                toast = Toast.makeText(getActivity(),
//                        getResources().getString(R.string.CHECK_INTERNET_CONNECTION),
//                        Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//        else
//        {
//            getDate();
//        }
//    }
//
//    public void setvisible(int check)
//    {
//
//        if(check==1)
//        {
//            txtToday.setBackgroundColor(Color.WHITE);
//            txt7days.setBackgroundResource(R.drawable.sortborder);
//            txt30days.setBackgroundResource(R.drawable.sortborder);
//            txtFromDate.setBackgroundResource(R.drawable.sortborder);
//            txttodate.setBackgroundResource(R.drawable.sortborder);
//
//            txtFromDate.setText("From Date");
//            txttodate.setText("To Date");
//            date_start="";
//            date_end="";
//        }
//        if(check==2)
//        {
//            txtToday.setBackgroundResource(R.drawable.sortborder);
//            txt7days.setBackgroundColor(Color.WHITE);
//            txt30days.setBackgroundResource(R.drawable.sortborder);
//            txtFromDate.setBackgroundResource(R.drawable.sortborder);
//            txttodate.setBackgroundResource(R.drawable.sortborder);
//
//            txtFromDate.setText("From Date");
//            txttodate.setText("To Date");
//            date_start="";
//            date_end="";
//        }
//        if(check==3)
//        {
//            txtToday.setBackgroundResource(R.drawable.sortborder);
//            txt7days.setBackgroundResource(R.drawable.sortborder);
//            txt30days.setBackgroundColor(Color.WHITE);
//            txtFromDate.setBackgroundResource(R.drawable.sortborder);
//            txttodate.setBackgroundResource(R.drawable.sortborder);
//
//            txtFromDate.setText("From Date");
//            txttodate.setText("To Date");
//            date_start="";
//            date_end="";
//        }
//        if(check==4)
//        {
//            txtToday.setBackgroundResource(R.drawable.sortborder);
//            txt7days.setBackgroundResource(R.drawable.sortborder);
//            txt30days.setBackgroundResource(R.drawable.sortborder);
//            txtFromDate.setBackgroundColor(Color.WHITE);
//        }
//        if(check==5)
//        {
//            txtToday.setBackgroundResource(R.drawable.sortborder);
//            txt7days.setBackgroundResource(R.drawable.sortborder);
//            txt30days.setBackgroundResource(R.drawable.sortborder);
//            txttodate.setBackgroundColor(Color.WHITE);
//        }
//    }
//
//    private void Initializer(View rootview)
//    {
//        cd=new ConnectionDetector(getActivity());
//        apIinterface= APIClient.getClient().create(APIinterface.class);
//        sp_Login = this.getActivity().getSharedPreferences("logindetail", Activity.MODE_PRIVATE);
//        rltVisitor =  rootview.findViewById(R.id.rltVisitor);
//        rlBack =  rootview.findViewById(R.id.rlBack);
//        rlMsg =  rootview.findViewById(R.id.rlMsg);
//        txtTittle =  rootview.findViewById(R.id.txtTittle);
//        txtTittle.setText(getActivity().getString(R.string.visitor));
//        context =  getActivity();
//        edtVisitor =  rootview.findViewById(R.id.edtVisitor);
//        lvVisitor =  rootview.findViewById(R.id.lvVisitor);
//        txtToday =  rootview.findViewById(R.id.txtToday);
//        txt7days =  rootview.findViewById(R.id.txt7days);
//        txt30days =  rootview.findViewById(R.id.txt30days);
//        txtFromDate =  rootview.findViewById(R.id.txtFromDate);
//        txttodate =  rootview.findViewById(R.id.txttodate);
//        arrVisitor = new ArrayList<>();
//
//    }
//    private void TextChangedListener() {
//        edtVisitor.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence cs, int arg1, int arg2,
//                                      int arg3) {
//                // When user changed the Text
//                try
//                {
//                    try
//                    {
//                        arrVisitor = vAdapter.filter(edtVisitor.getText().toString());
//                    }
//                    catch (Exception e)
//                    {
//                        e.printStackTrace();
//                    }
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence arg0, int arg1,
//                                          int arg2, int arg3) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable arg0) {
//
//            }
//        });
//    }
//
//    //-----------------------------------------------------------------------------
//
//
//    public void VisitorRetro(String startDate, String endDate)
//    {
//        new progressDialog(getActivity(),true);
//
//        String username = sp_Login.getString("username", "");
//        String password = sp_Login.getString("password", "");
//        String deviceId = sp_Login.getString("device_id", "");
//        String userid = sp_Login.getString("user_id", "");
//        String method="authenticate";
//
//        Call<List<Visitor_Pojo>> call2=apIinterface.LiveVisitor(username,password,deviceId,userid,method,startDate,endDate);
//        call2.enqueue(new Callback<List<Visitor_Pojo>>() {
//            @Override
//            public void onResponse(Call<List<Visitor_Pojo>> call, Response<List<Visitor_Pojo>> response)
//            {
//                try
//                {
//                    if (response.body().toString() != null || !response.body().toString().isEmpty())
//                    {
//                        List<Visitor_Pojo> userList = response.body();
//                        if (userList.get(0).errorcode.equals("0"))
//                        {
//                            for (int i = 0; i < userList.get(0).visitors.size(); i++)
//                            {
//                                userList.get(0).visitors.get(i).check_in_time =  userList.get(0).visitors.get(i).check_in ;
//                                dt = sdf24.parse(userList.get(0).visitors.get(i).check_in);
//                                String checkindate = sdf12.format(dt);
//                                String convertedcheckin = formatToYesterdayOrToday(checkindate);
//                                userList.get(0).visitors.get(i).check_in = convertedcheckin;
//
//                                if (userList.get(0).visitors.get(i).check_out.equalsIgnoreCase("0000-00-00 00:00:00"))
//                                {
//                                    userList.get(0).visitors.get(i).check_out = "";
//                                }
//                                else
//                                {
//                                    dt = sdf24.parse(userList.get(0).visitors.get(i).check_out);
//                                    String checkoutdate = sdf12.format(dt);
//                                    String convertedcheckout = formatToYesterdayOrToday(checkoutdate);
//                                    userList.get(0).visitors.get(i).check_out = convertedcheckout;
//                                }
//                            }
//                            if (userList.get(0).visitors.size() == 0)
//                            {
//                                rlMsg.setVisibility(View.VISIBLE);
//                                lvVisitor.setVisibility(View.GONE);
//                            }
//                            else
//                            {
//                                lvVisitor.setVisibility(View.VISIBLE);
//                                rlMsg.setVisibility(View.GONE);
//
//                                vAdapter = new NewVisitorAdapter(getActivity(), userList.get(0).visitors);
//                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//                                lvVisitor.setLayoutManager(mLayoutManager);
//                                lvVisitor.setItemAnimator(new DefaultItemAnimator());
//                                lvVisitor.setAdapter(vAdapter);
//                            }
//
//                        }else {
//                            toast = Toast.makeText(getActivity(),
//                                    getResources().getString(R.string.SERVER_RESPONDED), Toast.LENGTH_LONG);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
//
//                        }
//
//                    }else {
//                        toast = Toast.makeText(getActivity(),
//                                getResources().getString(R.string.SERVER_RESPONDED), Toast.LENGTH_LONG);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();
//                    }
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//                new progressDialog(getActivity(),false);
//            }
//
//            @Override
//            public void onFailure(Call<List<Visitor_Pojo>> call, Throwable t)
//            {
//                toast = Toast.makeText(getActivity(), t.getMessage().toString(), Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//                new progressDialog(getActivity(),false);
//            }
//        });
//    }
//
//    public static String formatToYesterdayOrToday(String date) throws ParseException {
//        Date dateTime = new SimpleDateFormat("dd-MM-yyyy,hh:mm a").parse(date);
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(dateTime);
//        Calendar today = Calendar.getInstance();
//        Calendar yesterday = Calendar.getInstance();
//        yesterday.add(Calendar.DATE, -1);
//        DateFormat timeFormatter = new SimpleDateFormat("hh:mma");
//        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
//            return "Today" + timeFormatter.format(dateTime);
//
//        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
//            return "Yesterday" + timeFormatter.format(dateTime);
//
//        } else {
//            return date;
//        }
//    }
//}
//
