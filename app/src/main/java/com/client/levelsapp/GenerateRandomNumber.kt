package com.client.levelsapp

import java.util.*


 class GenerateRandomNumber {
     companion object {
         private var instance: GenerateRandomNumber? = null
     fun getInstance(): GenerateRandomNumber {

         if (instance == null) {
             instance = GenerateRandomNumber()

         }
         return instance as GenerateRandomNumber
     }
 }

    fun initRandom():Random {
        val rand = Random()
        return rand
    }

     fun getRandomDoubleDigitNumber(random:Random,min:Int,max:Int):Int
     {
         return random!!.nextInt(max + 1 - min) + min
     }
}