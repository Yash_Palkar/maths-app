package com.client.levelsapp

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import java.util.*
import android.view.Gravity
import android.R.attr.gravity
import android.media.Image
import android.media.MediaPlayer
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.util.DisplayMetrics
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import android.widget.Button
import android.widget.LinearLayout
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import kotlinx.android.synthetic.main.questionview_fragment.*
import kotlinx.android.synthetic.main.tricky_dialog.*


class TrickyFragment : Fragment(), View.OnClickListener, RewardedVideoAdListener {


    var mActivity: Activity? = null
    var i = 1
    var random: Random? = null
    var myValue = ""
    var number1: TextView? = null
    var sign1: ImageView? = null
    var question: ImageView? = null
    var sign2: ImageView? = null
    var score_text:TextView?=null
    var answer: TextView? = null
    var txt_right:ImageView?= null
    var txt_wrong:ImageView?=null
    var right_animation:Animation?=null
    var wrong_animation:Animation?=null
    var right_mp: MediaPlayer? = null
    var wrong_mp: MediaPlayer? = null
    var txt_tricky_option1: TextView? = null
    var txt_tricky_option2: TextView? = null
    var txt_tricky_option3: TextView? = null
    var txt_tricky_option4: TextView? = null
    var txt_total_quest: TextView? = null
    var txt_wrong_count: TextView? = null
    var txt_right_count: TextView? = null
    var txt_timer_tricky: TextView? = null
    var img_pause: ImageView? = null
    var final_ans_float: Float? = null
    var final_ans: Int? = null
    var emoji:ImageView?=null
    var timer_tricky: CountDownTimer? = null
    var correct_count = 0
    var incorrect_count = 0
    var timervalue = 80
    var no2 = 0
    var statusText:TextView?=null
    var timer_count = 0
    var added_txt: TextView? = null
    var prefConfig: SharedPrefrenceClass? = null
    internal var rewardedVideoAd: RewardedVideoAd? = null

    companion object {
        var homefragment: TrickyFragment? = null
        fun getInstance(): TrickyFragment {
            homefragment = TrickyFragment()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        random = GenerateRandomNumber.getInstance().initRandom()
        prefConfig = SharedPrefrenceClass(context!!)
    }

    override fun onClick(v: View?) {
        when (myValue) {
            "+", "-", "*", "+ +", "- -", "* *", "+ -", "+ *", "- *", "+ - *" -> {
                when (v!!.id) {
                    R.id.txt_tricky_option1 -> setscoreInt(
                        txt_tricky_option1!!,
                        txt_tricky_option1!!.text.toString(),
                        no2!!
                    )
                    R.id.txt_tricky_option2 -> setscoreInt(
                        txt_tricky_option2!!,
                        txt_tricky_option2!!.text.toString(),
                        no2!!
                    )
                    R.id.txt_tricky_option3 -> setscoreInt(
                        txt_tricky_option3!!,
                        txt_tricky_option3!!.text.toString(),
                        no2!!
                    )
                    R.id.txt_tricky_option4 -> setscoreInt(
                        txt_tricky_option4!!,
                        txt_tricky_option4!!.text.toString(),
                        no2!!
                    )
                }
            }
            "/", "/ /", "+ /", "* /", "- * /", "* / +", "+ - * /" -> {
                when (v!!.id) {
                    R.id.txt_tricky_option1 -> setscoreFloat(
                        txt_tricky_option1!!,
                        txt_tricky_option1!!.text.toString(),
                        no2!!.toFloat()
                    )
                    R.id.txt_tricky_option2 -> setscoreFloat(
                        txt_tricky_option2!!,
                        txt_tricky_option2!!.text.toString(),
                        no2!!.toFloat()
                    )
                    R.id.txt_tricky_option3 -> setscoreFloat(
                        txt_tricky_option3!!,
                        txt_tricky_option3!!.text.toString(),
                        no2!!.toFloat()
                    )
                    R.id.txt_tricky_option4 -> setscoreFloat(
                        txt_tricky_option4!!,
                        txt_tricky_option4!!.text.toString(),
                        no2!!.toFloat()
                    )
                }
            }
        }
        txt_tricky_option1!!.isEnabled = false
        txt_tricky_option2!!.isEnabled = false
        txt_tricky_option3!!.isEnabled = false
        txt_tricky_option4!!.isEnabled = false
    }

    fun setscoreInt(txtview: TextView, selected_option: String, answer: Int) {
        if (timer_tricky != null) {
            timer_tricky!!.cancel()
            timer_tricky = null
        }
        if (selected_option.toInt().equals(answer)) {
            txtview.setTextColor(context!!.resources.getColor(R.color.green))
            added_txt!!.visibility = View.VISIBLE
            added_txt!!.setText("+5")
            setAnimation()
            correct_count = correct_count + 1
            txt_right_count!!.text = correct_count.toString()
            score_text!!.text=correct_count.toString()
            right_animation= AnimationUtils.loadAnimation(context,R.anim.zoom_in)
            // media player code
            txt_right!!.startAnimation(right_animation)
//           to do by sachin for right answer sound
            right_mp = MediaPlayer.create(view!!.context, R.raw.rightanswer)
            right_mp!!.start()
            timervalue = (txt_timer_tricky!!.text.toString()).toInt()
//            timervalue = (timervalue.plus(5))
            setCounter(timervalue * 1000)

        } else {
            txtview.setTextColor(context!!.resources.getColor(R.color.red))
            added_txt!!.visibility = View.VISIBLE
            added_txt!!.setText("-5")
            setAnimation()
            incorrect_count = incorrect_count + 1
            txt_wrong_count!!.text = incorrect_count.toString()
            wrong_animation= AnimationUtils.loadAnimation(context,R.anim.zoom_in)
            txt_wrong!!.startAnimation(wrong_animation)
//           to do by sachin for right answer sound
            wrong_mp = MediaPlayer.create(view!!.context, R.raw.wronganswer)
            wrong_mp!!.start()

            timervalue = (txt_timer_tricky!!.text.toString()).toInt()
//            timervalue = (timervalue.minus(5))
            setCounter(timervalue * 1000)
        }
        var total = correct_count + incorrect_count
        var percentage:Int =((correct_count * 100 / total))

        if(percentage >= 75 && percentage <= 100){

            statusText!!.text=("VeryGood").toString()
            emoji!!.setImageResource(R.drawable.supersym)

        }else if(percentage >= 50 && percentage < 75){
            statusText!!.text=("Good").toString()
            emoji!!.setImageResource(R.drawable.verygoodsym)
        }else if(percentage >= 25 && percentage < 50){
            statusText!!.text=("Bad").toString()
            emoji!!.setImageResource(R.drawable.goodsym)
        }else if(percentage >= 0 && percentage < 25){
            statusText!!.text=("VeryBad").toString()
            emoji!!.setImageResource(R.drawable.badsym)
        }
        Handler().postDelayed({
            setQuiz()
        }, 1000)

    }

    fun setscoreFloat(txtview: TextView, selected_option: String, answer: Float) {
        if (timer_tricky != null) {
            timer_tricky!!.cancel()
            timer_tricky = null
        }
        if (selected_option.toFloat().equals(answer)) {
            txtview.setTextColor(context!!.resources.getColor(R.color.green))
            added_txt!!.visibility = View.VISIBLE
            added_txt!!.setText("+5")
            setAnimation()
            correct_count = correct_count.plus(1)
            txt_right_count!!.text = correct_count.toString()
            score_text!!.text=correct_count.toString()
            right_animation= AnimationUtils.loadAnimation(context,R.anim.zoom_in)
            // media player code
            txt_right!!.startAnimation(right_animation)
//           to do by sachin for right answer sound
            right_mp = MediaPlayer.create(view!!.context, R.raw.rightanswer)
            right_mp!!.start()
            timervalue = (txt_timer_tricky!!.text.toString()).toInt()
//            timervalue = (timervalue.plus(5))
            setCounter(timervalue * 1000)

        } else {
            txtview.setTextColor(context!!.resources.getColor(R.color.red))
            added_txt!!.visibility = View.VISIBLE
            added_txt!!.setText("-5")
            setAnimation()
            incorrect_count = incorrect_count + 1
            txt_wrong_count!!.text = incorrect_count.toString()
            wrong_animation= AnimationUtils.loadAnimation(context,R.anim.zoom_in)
            txt_wrong!!.startAnimation(wrong_animation)
//           to do by sachin for right answer sound
            wrong_mp = MediaPlayer.create(view!!.context, R.raw.wronganswer)
            wrong_mp!!.start()
            timervalue = (txt_timer_tricky!!.text.toString()).toInt()
//            timervalue = (timervalue.minus(5))
            setCounter(timervalue * 1000)
        }
        var total = correct_count + incorrect_count
        var percentage:Int =((correct_count * 100 / total))

        if(percentage >= 75 && percentage <= 100){

            statusText!!.text=("VeryGood").toString()
            emoji!!.setImageResource(R.drawable.supersym)
        }else if(percentage >= 50 && percentage < 75){
            statusText!!.text=("Good").toString()
            emoji!!.setImageResource(R.drawable.verygoodsym)
        }else if(percentage >= 25 && percentage < 50){
            statusText!!.text=("Bad").toString()
            emoji!!.setImageResource(R.drawable.goodsym)
        }else if(percentage >= 0 && percentage < 25){
            statusText!!.text=("VeryBad").toString()
            emoji!!.setImageResource(R.drawable.badsym)
        }
        Handler().postDelayed({
            setQuiz()
        }, 1000)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        MobileAds.initialize(context, resources.getString(R.string.admob_app_id))
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)
        rewardedVideoAd!!.rewardedVideoAdListener = this
        loadRewardedVideoAd()
        val view: View = inflater.inflate(R.layout.tricky_dialog, container, false)
        myValue = this.arguments!!.getString("title").toString()
        number1 = view.findViewById(R.id.txt_number1_tricky) as TextView
        sign1 = view.findViewById(R.id.txt_sign1_tricky) as ImageView
        txt_right_count=view.findViewById(R.id.txt_right_count)
        question = view.findViewById(R.id.txt_question_tricky) as ImageView
        sign2 = view.findViewById(R.id.txt_sign2_tricky) as ImageView
        answer = view.findViewById(R.id.txt_answer_tricky) as TextView
        txt_tricky_option1 = view.findViewById(R.id.txt_tricky_option1) as TextView
        txt_tricky_option2 = view.findViewById(R.id.txt_tricky_option2) as TextView
        txt_tricky_option3 = view.findViewById(R.id.txt_tricky_option3) as TextView
        txt_tricky_option4 = view.findViewById(R.id.txt_tricky_option4) as TextView
        txt_wrong=view.findViewById(R.id.txt_wrong)
        statusText=view.findViewById(R.id.statusId)
        emoji=view.findViewById(R.id.emojiId)
        txt_wrong_count = view.findViewById(R.id.txt_wrong_count) as TextView
        txt_right_count = view.findViewById(R.id.txt_right_count) as TextView
        added_txt=view.findViewById(R.id.added_txt)
        txt_total_quest = view.findViewById(R.id.txt_total_quest) as TextView
        txt_timer_tricky = view.findViewById(R.id.txt_timer_tricky) as TextView
        score_text=view.findViewById(R.id.score_txt)
        img_pause = view.findViewById(R.id.img_pause) as ImageView
        txt_right=view.findViewById(R.id.txt_right)
        txt_tricky_option1!!.setOnClickListener(this)
        txt_tricky_option2!!.setOnClickListener(this)
        txt_tricky_option3!!.setOnClickListener(this)
        txt_tricky_option4!!.setOnClickListener(this)
        img_pause!!.setOnClickListener {
            setBackDialog()
        }


        setQuiz()
        if (timer_tricky != null) {
            timer_tricky!!.cancel()
            timer_tricky = null
        }
        setCounter(timervalue*1000)

        return view
    }

    private fun loadRewardedVideoAd() {
        Log.d("TAG", "Rewarded ad loading...")
        if (!rewardedVideoAd!!.isLoaded()) {
            rewardedVideoAd!!.loadAd(
                resources.getString(R.string.rewarded_video_Ad),
                AdRequest.Builder().build()
            )
        }
    }


    fun setCounter(countervalue: Int) {
        timer_tricky = object : CountDownTimer(countervalue.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                txt_timer_tricky!!.setText("" + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                txt_timer_tricky!!.setText("0")
                setDialog()

            }
        }.start()

    }

    fun setAnimation() {
        var slide: Animation? = null
        slide = TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
            0.0f, Animation.RELATIVE_TO_SELF, -15.0f
        )

        slide.duration = 1000
        slide.fillAfter = true
        slide.isFillEnabled = true
        added_txt!!.startAnimation(slide)

        slide.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                added_txt!!.clearAnimation()
                added_txt!!.visibility = View.GONE
            }

            override fun onAnimationStart(animation: Animation?) {

            }


        });
//        var animation = TranslateAnimation(0F, 0F, -50F, 0F)
//        animation.setDuration(700)
//        animation.setFillAfter(false)
//        animation.setAnimationListener(MyAnimationListener())
//        added_txt!!.startAnimation(animation)
    }


    fun setQuiz() {
        txt_tricky_option1!!.setTextColor(context!!.resources.getColor(R.color.red))
        txt_tricky_option2!!.setTextColor(context!!.resources.getColor(R.color.red))
        txt_tricky_option3!!.setTextColor(context!!.resources.getColor(R.color.red))
        txt_tricky_option4!!.setTextColor(context!!.resources.getColor(R.color.red))
        txt_tricky_option1!!.isEnabled = true
        txt_tricky_option2!!.isEnabled = true
        txt_tricky_option3!!.isEnabled = true
        txt_tricky_option4!!.isEnabled = true
        if (i <= 20) {
            var level_no = 0
            when {
                i < 5 -> level_no = 1
                i < 10 -> level_no = 2
                i < 15 -> level_no = 4
                i < 20 -> level_no = 10
                else -> level_no = 20
            }
            var no1 = GenerateRandomNumber.getInstance()
                .getRandomDoubleDigitNumber(random!!, (6 * level_no), (10 * level_no))
            no2 = GenerateRandomNumber.getInstance()
                .getRandomDoubleDigitNumber(random!!, (1 * level_no), (5 * level_no))


            when (myValue) {
                "+", "-", "*", "/" -> {
                    if (myValue.equals("/")) {
                        final_ans_float = setName1(myValue, no1, no2)
                        answer!!.text = final_ans_float!!.toString()
                    } else {
                        final_ans = setName(myValue, no1, no2)
                        answer!!.text = final_ans!!.toString()
                    }
                    number1!!.text = no1.toString()

//                    sign1!!.text = myValue

                }

            }
            when (myValue) {
                "+" -> {
                    sign1!!.setImageResource(R.drawable.addition)
                }
                "-" -> {
                    sign1!!.setImageResource(R.drawable.substract)
                }
                "*" -> {
                    sign1!!.setImageResource(R.drawable.multiplication)
                }
                "/" -> {
                    sign1!!.setImageResource(R.drawable.divide)
                }
            }
            val myAnswerList = ArrayList<Int>()
            val myAnswerListfloat = ArrayList<Float>()
            when (myValue) {
                "+", "-", "*" -> {

                    myAnswerList.add(no2!! + 3)
                    myAnswerList.add(no2!! + 4)
                    myAnswerList.add(no2!! * 2)
                    myAnswerList.add(no2!!)
                    Collections.shuffle(myAnswerList)
                    txt_tricky_option1!!.text = myAnswerList.get(0).toString()
                    txt_tricky_option2!!.text = myAnswerList.get(1).toString()
                    txt_tricky_option3!!.text = myAnswerList.get(2).toString()
                    txt_tricky_option4!!.text = myAnswerList.get(3).toString()
                }
                "/" -> {
                    myAnswerListfloat.add(no2!! + 3f)
                    myAnswerListfloat.add(no2!! + 4f)
                    myAnswerListfloat.add(no2!! * 2f)
                    myAnswerListfloat.add(no2!!.toFloat())
                    Collections.shuffle(myAnswerListfloat)
                    txt_tricky_option1!!.text = myAnswerListfloat.get(0).toString()
                    txt_tricky_option2!!.text = myAnswerListfloat.get(1).toString()
                    txt_tricky_option3!!.text = myAnswerListfloat.get(2).toString()
                    txt_tricky_option4!!.text = myAnswerListfloat.get(3).toString()
                }
            }



            txt_total_quest!!.setText("" + i + "/20")

            i++
        } else {
            levelcomplete()
        }
    }

    fun levelcomplete() {

        val dialog1 = Dialog(context!!)
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog1.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog1.setCancelable(false)
        dialog1.setContentView(R.layout.dialog_next_question)

        val txt_title = dialog1.findViewById(R.id.txt_title) as TextView
        val txt_desc = dialog1.findViewById(R.id.txt_desc) as TextView
        val txt_btn_next = dialog1.findViewById(R.id.txt_btn_next) as TextView
        txt_title.setText("Level Completed")
        txt_desc.visibility = View.GONE
//        txt_desc.setText("Collect $star star to unlock this level.")
        txt_btn_next.setText("OK")
        txt_btn_next.setOnClickListener {
            dialog1.dismiss()

            var i = Intent(context, MainActivity::class.java)
            i.putExtra("redirect", "0")
            i.putExtra("title", myValue)
            startActivity(i)
            if (rewardedVideoAd!!.isLoaded()) {
                Log.d("TAG", "Rewarded ad showing")
                rewardedVideoAd!!.show()
            }
        }

        dialog1.show()

    }

    override fun onResume() {
        super.onResume()
        if (rewardedVideoAd != null) {
            rewardedVideoAd!!.resume(context)
        }
        if (timer_count != 0) {
            timer_tricky = object : CountDownTimer(timer_count.toLong(), 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    txt_timer_tricky!!.setText("" + millisUntilFinished / 1000)
                }

                override fun onFinish() {
                    txt_timer_tricky!!.setText("0")
                    setDialog()
                }
            }.start()
        }
    }

    fun setDialog() {
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_next_question)

        val next = dialog.findViewById(R.id.txt_btn_next) as TextView
        val txt_title = dialog.findViewById(R.id.txt_title) as TextView
        val txt_desc = dialog.findViewById(R.id.txt_desc) as TextView
        txt_desc.visibility = View.GONE
        next.text = "OK"
        txt_title.text = "Level 1 completed"
        next.setOnClickListener {
            dialog.dismiss()
            if (timer_tricky != null) {
                timer_tricky!!.cancel()
                timer_tricky = null
            }
            var i = Intent(activity, MainActivity::class.java)
            i.putExtra("title", myValue)
            i.putExtra("redirect", "0")
            startActivity(i)
        }

        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (timer_tricky != null) {
            timer_tricky!!.cancel()
            timer_tricky = null
        }
    }

    fun setName(sign: String, num1: Int, num2: Int): Int {
        var name: Int? = null
        when (sign) {
            "+" -> name = CalculateMethods.calculateInt(EnumClass.OperationInt.ADDITION, num1, num2)
            "-" -> name =
                CalculateMethods.calculateInt(EnumClass.OperationInt.SUBTRACTION, num1, num2)
            "*" -> name =
                CalculateMethods.calculateInt(EnumClass.OperationInt.MULTIPLICATION, num1, num2)
        }

        return name!!
    }

    fun setName1(sign: String, num1: Int, num2: Int): Float {
        var name: Float? = null
        when (sign) {
            "/" -> name =
                CalculateMethods.calculateFloat(EnumClass.OperationFloat.DIVISION, num1, num2)
        }

        return name!!
    }

    fun setBackDialog() {
        prefConfig!!.writeLevelscoretricky(MainActivity.getLevelNumber(myValue), timervalue)
        if (timer_tricky != null) {
            timer_tricky!!.cancel()
            timer_tricky = null
        }
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.back_dialog)
        var displayMetrics = DisplayMetrics()
        if (mActivity != null)
            mActivity!!.windowManager.getDefaultDisplay().getMetrics(displayMetrics)
        var displayWidth = displayMetrics.widthPixels
        var displayHeight = displayMetrics.heightPixels
        var layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog.getWindow()?.getAttributes())
        // var dialogWindowWidth = (displayWidth * 0.5f)
        var dialogWindowHeight = (displayHeight * 0.3f)
        layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
        layoutParams.height = dialogWindowHeight.toInt()
        dialog.getWindow()?.setAttributes(layoutParams)

        val img_back = dialog.findViewById(R.id.img_back) as ImageView
        val img_menu = dialog.findViewById(R.id.img_menu) as ImageView
        val img_next = dialog.findViewById(R.id.img_next) as ImageView
        val close = dialog.findViewById(R.id.close) as ImageView

        close.setOnClickListener {
            timer_count = ((txt_timer_tricky!!.text.toString().toInt()) * 1000)
            onResume()
            dialog.dismiss()
        }

        img_back.setOnClickListener {
            dialog.dismiss()
            var i = Intent(context, MainActivity::class.java)
            i.putExtra("redirect", "0")
            i.putExtra("title", myValue)
            startActivity(i)
        }

        img_menu.setOnClickListener {
            dialog.dismiss()
            startActivity(Intent(context, MainActivity::class.java))
        }
        img_next.setOnClickListener {
            dialog.dismiss()
            i = i++
            setQuiz()
        }
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.show()
    }

    override fun onRewardedVideoAdClosed() {
        loadRewardedVideoAd()
    }


    override fun onRewardedVideoAdLeftApplication() {}

    override fun onRewardedVideoAdLoaded() {}

    override fun onRewardedVideoAdOpened() {}

    override fun onRewarded(p0: RewardItem?) {}

    override fun onRewardedVideoStarted() {}

    override fun onRewardedVideoAdFailedToLoad(p0: Int) {
        loadRewardedVideoAd()
    }

    override fun onRewardedVideoCompleted() {
        loadRewardedVideoAd()
    }
}

