package com.client.levelsapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.io.File

class ShowDataFormulaTrick : Fragment() {

    var mActivity: Activity? = null
    var  show_data: TextView? = null
    var str=""
    var count = 0
    var from_where=""
    var permissionClass: PermissionClass? = null
    companion object {
        var homefragment: ShowDataFormulaTrick? = null
        fun getInstance(): ShowDataFormulaTrick {
            homefragment = ShowDataFormulaTrick()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        askPermission()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.activity_formula_trick, container, false)

        from_where = this.arguments!!.getString("from_where").toString()
         show_data = view.findViewById(R.id.show_data) as TextView
        if (from_where.equals("1")) {
            str = mActivity!!.assets.open("BODMASRULE").bufferedReader().use { it.readText() }
        } else if (from_where.equals("2")) {
            str = mActivity!!.assets.open("LawsofIndices").bufferedReader().use { it.readText() }
        }else if (from_where.equals("3")) {
            str = mActivity!!.assets.open("Sumanddifference").bufferedReader().use { it.readText() }
        }else if (from_where.equals("4")) {
            str = mActivity!!.assets.open("Surds").bufferedReader().use { it.readText() }
        }else if (from_where.equals("5")) {
            str = mActivity!!.assets.open("ComplexNumbers").bufferedReader().use { it.readText() }
        }else if (from_where.equals("6")) {
            str = mActivity!!.assets.open("Variation").bufferedReader().use { it.readText() }
        }else if (from_where.equals("7")) {
            str = mActivity!!.assets.open("ArithmeticalProgression").bufferedReader().use { it.readText() }
        }else if (from_where.equals("8")) {
            str = mActivity!!.assets.open("GeometricalProgression").bufferedReader().use { it.readText() }
        }else if (from_where.equals("9")) {
            str = mActivity!!.assets.open("TheoryofQuadraticEquation").bufferedReader().use { it.readText() }
        }else if (from_where.equals("10")) {
            str = mActivity!!.assets.open("Permutation").bufferedReader().use { it.readText() }
        }else if (from_where.equals("11")) {
            str = mActivity!!.assets.open("Combination").bufferedReader().use { it.readText() }
        }else if (from_where.equals("12")) {
            str = mActivity!!.assets.open("BinomialTheorem").bufferedReader().use { it.readText() }
        }else if (from_where.equals("13")) {
            str = mActivity!!.assets.open("Logarithm").bufferedReader().use { it.readText() }
        }else if (from_where.equals("14")) {
            str = mActivity!!.assets.open("ExponentialSeries").bufferedReader().use { it.readText() }
        }else if (from_where.equals("15")) {
            str = mActivity!!.assets.open("LogarithmicSeries").bufferedReader().use { it.readText() }
        }else if (from_where.equals("16")) {
            str = mActivity!!.assets.open("Trigonometry").bufferedReader().use { it.readText() }
        }
        else if (from_where.equals("17")) {
            str = mActivity!!.assets.open("MathTrick").bufferedReader().use { it.readText() }
        }
        show_data!!.text = str

        return view
    }
    private fun askPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            permissionClass = PermissionClass(mActivity!!).getInstance(mActivity!!)
            if (permissionClass!!.isAllPermissionAvailable()) {

                if (from_where.equals("1")) {
                    str = mActivity!!.assets.open("MathFormula").bufferedReader().use { it.readText() }
                } else if (from_where.equals("2")) {
                    str = mActivity!!.assets.open("MathTrick").bufferedReader().use { it.readText() }
                }
                show_data!!.text = str
            } else {
                permissionClass!!.setActivity(mActivity!!)
                permissionClass!!.requestPermissionIfDenied(PermissionClass(mActivity!!).PERMISSION_READ_EXTERNAL_STORAGE)
            }
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var permissionGranted = true
        for (i in grantResults) {
            if (i != PackageManager.PERMISSION_GRANTED) {
                permissionGranted = false
                break
            }
        }
        if (!permissionGranted) {
            if (count > 4) {
                Toast.makeText(mActivity!!, "Please provide location permissions from the settings", Toast.LENGTH_SHORT).show()
                startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                    )
                )
            }
            permissionClass!!.requestPermissionIfDenied(PermissionClass(mActivity!!).PERMISSION_READ_EXTERNAL_STORAGE)
        }else
        {
            if (from_where.equals("1")) {
                str = mActivity!!.assets.open("MathFormula").bufferedReader().use { it.readText() }
            } else if (from_where.equals("2")) {
                str = mActivity!!.assets.open("MathTrick").bufferedReader().use { it.readText() }
            }
            show_data!!.text = str
        }
        count++
    }




}