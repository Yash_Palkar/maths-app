package com.client.levelsapp

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class AchievementAdapter(
    context: Context, private val numberData: Array<String>
) :
    RecyclerView.Adapter<AchievementAdapter.ViewHolder>() {
    var context: Context? = context

    var downLabelArr = arrayOf(
        "Collect 1 stars in 1st level",
        "Collect 3 stars in 1st and 2nd level",
        "Unlock level 3",
        "Unlock level 5",
        "Collect first five stars",
        "Collect 8 stars in 1 2 3 4 level",
        "Collect 12 stars in easy level",
        "Unlock 1st medium level",
        "Collect 15 star in all",
        "Collect 25 stars in all",
        "score 60 in 1st tricky level",
        "Unlock tricky level of subtract",
        "Score 70 in 2nd tricky level",
        "Score 150 in 1st and 2nd tricky level",
        "Score total 200 in 1 2 3 tricky level",
        "Unlock tricky level of division",
        "Score total 300 in all tricky level",
        "Score total 400 in all tricky level"
    )

    var prefconfig: SharedPrefrenceClass? = null
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.achievement_item, p0, false))
    }

    override fun getItemCount(): Int {
        return numberData.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val data = numberData[p1]
        val data1 = downLabelArr[p1]
        prefconfig = SharedPrefrenceClass(context!!)
        val achlevel = prefconfig!!.readachievement(p1)
        if (achlevel == 1) {
            when (numberData[p1]) {
                "Addition / 1 Star" -> p0.img_entry.setImageResource(R.drawable.trophy_0)
                "Two Levels / 3 Stars" -> p0.img_entry.setImageResource(R.drawable.trophy_1)
                "Open Level 3" -> p0.img_entry.setImageResource(R.drawable.trophy_2)
                "Open Level 5" -> p0.img_entry.setImageResource(R.drawable.trophy_3)
                "5 Stars Challenge" -> p0.img_entry.setImageResource(R.drawable.trophy_4)
                "Level 1 to 4 / 8 Stars" -> p0.img_entry.setImageResource(R.drawable.trophy_5)
                "In All easy / 12 Stars" -> p0.img_entry.setImageResource(R.drawable.trophy_6)
                "Open Level 7" -> p0.img_entry.setImageResource(R.drawable.trophy_7)
                "15 Stars Challenge" -> p0.img_entry.setImageResource(R.drawable.trophy_8)
                "25 Stars Challenge" -> p0.img_entry.setImageResource(R.drawable.trophy_9)
                "Level 1 / Score 60" -> p0.img_entry.setImageResource(R.drawable.trophy_10)
                "Open 2nd Tricky level " -> p0.img_entry.setImageResource(R.drawable.trophy_11)
                "2nd tricky / Score 70" -> p0.img_entry.setImageResource(R.drawable.trophy_12)
                "In Two Tricky/Score 150" -> p0.img_entry.setImageResource(R.drawable.trophy_13)
                "In Three Tricky/Score 200" -> p0.img_entry.setImageResource(R.drawable.trophy_14)
                "Open 4th Tricky level " -> p0.img_entry.setImageResource(R.drawable.trophy_16)
                "In All tricky / Score 300 " -> p0.img_entry.setImageResource(R.drawable.trophy_17)
                "In All tricky / Score 400 " -> p0.img_entry.setImageResource(R.drawable.trophy_18)
            }
            p0.img_wrong_access.setImageResource(R.drawable.checked)
            p0.cons_main.setBackgroundResource(R.drawable.button_shape_brown)
        } else {
            p0.img_entry.setImageResource(R.drawable.no_entry)
            p0.img_wrong_access.setImageResource(R.drawable.wrong_access)
            p0.cons_main.setBackgroundResource(R.drawable.button_shape_darkgrey)
        }
        p0.txt_num1.text = data
        p0.txt_num2.text = data1
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_num1: TextView = itemView.findViewById(R.id.txt_achievement_title)
        var txt_num2: TextView = itemView.findViewById(R.id.txt_achievement_desc)
        var img_entry: ImageView = itemView.findViewById(R.id.img_entry)
        var img_wrong_access: ImageView = itemView.findViewById(R.id.img_wrong_access)
        var cons_main: ConstraintLayout = itemView.findViewById(R.id.cons_main)
    }
}