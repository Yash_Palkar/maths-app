package com.client.levelsapp

import android.net.UrlQuerySanitizer
import android.util.Log
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class HttpApiCalling {



    fun apicall(hashmap:HashMap<String,String>, url: URL): String? {

        var `is`: InputStream? = null
        var response :String= ""
        var counter = 1
        var urlParameters = ""
        val myIterator: Iterator<*> = hashmap.keys.iterator()
        while (myIterator.hasNext()) {
            val key: String = myIterator.next().toString()
            val value: String? = hashmap.get(key)
            if (counter < hashmap.size) urlParameters = urlParameters + key + "=" + value + "&"
            if (counter == hashmap.size) urlParameters = urlParameters + key + "=" + value
            counter++
        }
        try {

            val urlConn = url.openConnection() as HttpURLConnection
            urlConn.requestMethod = "POST"
            urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            urlConn.setRequestProperty("Accept", "applicaiton/json")
            urlConn.doOutput = true
            urlConn.connect()
            val writer = BufferedWriter(OutputStreamWriter(urlConn.outputStream))
            writer.write(urlParameters)
            writer.flush()
            writer.close()
            if (urlConn.responseCode == HttpURLConnection.HTTP_OK) {
                `is` = urlConn.inputStream // is is inputstream
            } else {
                `is` = urlConn.errorStream
            }

        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        try {
            val reader = BufferedReader(
                InputStreamReader(
                    `is`, "UTF-8"
                ), 8
            )
            val sb = java.lang.StringBuilder()
            var line: String? = null
            while (reader.readLine().also { line = it } != null) {
                sb.append(
                    """
                $line
                
                """.trimIndent()
                )
            }

            `is`!!.close()


            response = sb.toString()

            Log.e("JSON", response)
        } catch (e: java.lang.Exception) {
            Log.e("Buffer Error", "Error converting result $e")
        }

        return response
    }
}

