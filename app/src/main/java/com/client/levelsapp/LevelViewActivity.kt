package com.client.levelsapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds

class LevelViewActivity : AppCompatActivity() {

    var txt_title_name: TextView? = null
    var txt_score: TextView? = null
    var txt_score_number: TextView? = null
    var starlin: TextView? = null
    var easyText: TextView? = null
    var meediumText: TextView? = null
    var mybundle: Bundle? = null
    var redirect: String? = null
    var level = ""
    var level_no = 0
    var easy: String? = null
    var medium: String? = null
    var mtitle : String? =null
    var prefconfig: SharedPrefrenceClass? = null
    var myValue:String?=null
    var score:TextView?=null
    var adIRequest:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_level_view)
        txt_title_name=findViewById(R.id.txt_title_name)
        easyText = findViewById(R.id.txt_easy)
        meediumText = findViewById(R.id.txt_medium)
        txt_score_number=findViewById(R.id.txt_score_number)
        MobileAds.initialize(this, resources.getString(R.string.banner_app_id))
        val mAdView: AdView = findViewById(R.id.banner_ad)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
        try {


            mybundle = intent.extras
            // intent = intent

            if (mybundle != null) {
                if (mybundle!!.getString("redirect") != null) {
                    redirect = mybundle!!.getString("redirect")!!
                }
                if (mybundle!!.getString("title") != null) {
                    title = mybundle!!.getString("title")!!
                }
                if (mybundle!!.getString("level") != null) {
                    level = mybundle!!.getString("level")!!
                }
                if (mybundle!!.getInt("level_no") != null) {
                    level_no = mybundle!!.getInt("level_no")!!
                }
                if (mybundle!!.getString("easy") != null) {
                    easy = mybundle!!.getString("easy")!!
                }
                if (mybundle!!.getString("medium") != null) {
                    medium = mybundle!!.getString("medium")!!
                }

            }

            myValue = mybundle!!.getString("title")


            txt_title_name!!.text = MainActivity.setName(myValue!!)
        }
        catch (e:Exception){

            println(e)
        }

        easyText!!.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, EasyTricky::class.java)
            intent.putExtra("title", easy!!)
            intent.putExtra("getLevelNumber", getLevelNumber(easy!!))
            startActivity(intent)
//            finish()
//            var intent = Intent(this, MainActivity::class.java)
//            intent.putExtra("redirect", "0")
//            intent.putExtra("title", easy)
//            startActivity(intent)
//            finish()
        })

        meediumText!!.setOnClickListener(View.OnClickListener {
//            var intent = Intent(this, MainActivity::class.java)
//            intent.putExtra("redirect", "0")
//            intent.putExtra("title", medium)
//            startActivity(intent)

            var intent = Intent(this, EasyTricky::class.java)
            intent.putExtra("title", medium!!)
            intent.putExtra("getLevelNumber", getLevelNumber(medium!!))
            startActivity(intent)
//            finish()

        })
//        if (prefconfig!!.readLevelscoreeasy(p1) >= 300) {
//            p0!!.img_star1!!.setImageResource(R.drawable.star_yellow)
//        } else {
//            p0!!.img_star1!!.setImageResource(R.drawable.star)
//        }
//        if (prefconfig!!.readLevelscoreeasy(p1) >= 600) {
//            p0!!.img_star2!!.setImageResource(R.drawable.star_yellow)
//        } else {
//            p0!!.img_star2!!.setImageResource(R.drawable.star)
//        }
//        if (prefconfig!!.readLevelscoreeasy(p1) >= 900) {
//            p0!!.img_star3!!.setImageResource(R.drawable.star_yellow)
//        } else {
//            p0!!.img_star3!!.setImageResource(R.drawable.star)
//        }
//        var count = prefconfig!!.readLevelscoreeasy(p1)
//        easy!!.txt_score_number!!.setText("$count/1570")

    }

    fun getLevelNumber(sign: String): Int {
        var name = 0
        when (sign) {
            "+" -> name = 0
            "-" -> name = 1
            "*" -> name = 2
            "/" -> name = 3
            "+ +" -> name = 4
            "- -" -> name = 5
            "* *" -> name = 6
            "/ /" -> name = 7
            "+ -" -> name = 8
            "+ *" -> name = 9
            "+ /" -> name = 10
            "- *" -> name = 11
            "- /" -> name = 12
            "* /" -> name = 13
            "+ - *" -> name = 14
            "- * /" -> name = 15
            "* / +" -> name = 16
            "+ - * /" -> name = 17
        }

        return name
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, HomeActivity::class.java))
//        finish()
    }
}
