package com.client.levelsapp

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_formula_all_options.*

class formulaAllOptions : AppCompatActivity(), View.OnClickListener {
    var Laybodmas: LinearLayout? = null
    var Laylawsofindices: LinearLayout? = null
    var Laysumand: LinearLayout? = null
    var Laysurd: LinearLayout? = null
    var Laycomplexno: LinearLayout? = null
    var Layvariations: LinearLayout? = null
    var Layarithprog: LinearLayout? = null
    var Laygeoprog: LinearLayout? = null
    var Laytheoryofq: LinearLayout? = null
    var Laypermutation: LinearLayout? = null
    var Laycombination: LinearLayout? = null
    var Laybinomial: LinearLayout? = null
    var Laylogarithm: LinearLayout? = null
    var Layexposeries: LinearLayout? = null
    var Laylogseries: LinearLayout? = null
    var Laythrignometry: LinearLayout? = null
    var forFrag: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formula_all_options)
        Laybodmas = findViewById(R.id.bodmas) as LinearLayout
        Laylawsofindices = findViewById(R.id.lawsofindices) as LinearLayout
        Laysumand = findViewById(R.id.sumand) as LinearLayout
        Laysurd = findViewById(R.id.surd) as LinearLayout
        Laycomplexno = findViewById(R.id.complexno) as LinearLayout
        Layvariations = findViewById(R.id.variations) as LinearLayout
        Layarithprog = findViewById(R.id.arithprog) as LinearLayout
        Laygeoprog = findViewById(R.id.geoprog) as LinearLayout
        Laytheoryofq = findViewById(R.id.theoryofq) as LinearLayout
        Laypermutation = findViewById(R.id.permutation) as LinearLayout
        Laycombination = findViewById(R.id.combination) as LinearLayout
        Laybinomial = findViewById(R.id.binomial) as LinearLayout
        Laylogarithm = findViewById(R.id.logarithm) as LinearLayout
        Layexposeries = findViewById(R.id.exposeries) as LinearLayout
        Laylogseries = findViewById(R.id.logseries) as LinearLayout
        Laythrignometry = findViewById(R.id.thrignometry) as LinearLayout
        forFrag = findViewById(R.id.content_frame) as FrameLayout
        bodmas.setOnClickListener(this)
        lawsofindices.setOnClickListener(this)
        sumand.setOnClickListener(this)
        surd.setOnClickListener(this)
        complexno.setOnClickListener(this)
        variations.setOnClickListener(this)
        arithprog.setOnClickListener(this)
        geoprog.setOnClickListener(this)
        theoryofq.setOnClickListener(this)
        permutation.setOnClickListener(this)
        combination.setOnClickListener(this)
        binomial.setOnClickListener(this)
        logarithm.setOnClickListener(this)
        exposeries.setOnClickListener(this)
        logseries.setOnClickListener(this)
        thrignometry.setOnClickListener(this)


    }

    fun FragmentInit(showformula:String) {
        val bundle = Bundle()

         bundle.putString("formulaId", "1")
         bundle.putString("from_where", showformula)
        val firstFragment = ShowDataFormulaTrick()
        firstFragment.arguments = bundle
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("activity_formula_trick")
        transaction.remove(Fragment())
        transaction.commit()
        forFrag!!.visibility=View.VISIBLE
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.bodmas ->
                FragmentInit("1")

            R.id.lawsofindices ->
                FragmentInit("2")
            R.id.sumand ->
                FragmentInit("3")
            R.id.surd ->
                FragmentInit("4")
            R.id.complexno ->
                FragmentInit("5")
            R.id.variations ->
                FragmentInit("6")
            R.id.arithprog ->
                FragmentInit("7")
            R.id.geoprog ->
                FragmentInit("8")
            R.id.theoryofq ->
                FragmentInit("9")
            R.id.permutation ->
                FragmentInit("10")
            R.id.combination ->
                FragmentInit("11")
            R.id.binomial ->
                FragmentInit("12")
            R.id.logarithm ->
                FragmentInit("13")
            R.id.exposeries ->
                FragmentInit("14")
            R.id.logseries ->
                FragmentInit("15")
            R.id.thrignometry ->
                FragmentInit("16")

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
       // forFrag=null
        if (forFrag!!.getVisibility() == View.VISIBLE){
            startActivity(Intent(this, formulaAllOptions::class.java))
            finish()

        }else{
            startActivity(Intent(this, Menu::class.java))
            finish()
        }
    }
}