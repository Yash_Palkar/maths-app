package com.client.levelsapp

interface SimpleDivision {
    fun apply(x1: Int, x2: Int): Float
}