package com.client.levelsapp

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class TableAdapter(
    context: Context,
    numberData: ArrayList<Int>
) : RecyclerView.Adapter<TableAdapter.ViewHolder>() {
    var numbersList: ArrayList<Int>? = numberData
    var context: Context? = context
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.table_adapter_item, p0, false))
    }

    override fun getItemCount(): Int {
        return numbersList!!.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val data = numbersList!![p1]
        val data1 = multiplier_numbers!![p1]

        p0!!.txt_num1.text = data.toString()
        p0!!.txt_num2.text = data1.toString()

        p0!!.txt_ans.text = ((data * data1).toString())
    }

    private val multiplier_numbers = arrayOf(
        1,
        2, 3, 4,
        5, 6, 7,
        8, 9, 10
    )

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txt_num1: TextView
        var txt_num2: TextView
        var txt_ans: TextView

        init {
            txt_num1 = itemView.findViewById(R.id.txt_num1)
            txt_num2 = itemView.findViewById(R.id.txt_num2)
            txt_ans = itemView.findViewById(R.id.txt_ans)
        }
    }
}