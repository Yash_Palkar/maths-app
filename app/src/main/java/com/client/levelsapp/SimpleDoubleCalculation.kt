package com.client.levelsapp

interface SimpleDoubleCalculation {
    fun apply(x1: Int, x2: Int,x3:Int): Int
}