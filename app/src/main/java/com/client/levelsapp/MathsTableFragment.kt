package com.client.levelsapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast


class MathsTableFragment : Fragment() {

    var mActivity: Activity? = null
    val numbersList: ArrayList<Int> = ArrayList()
    private lateinit var layoutManager: RecyclerView.LayoutManager
    var edt_num: EditText? = null
    var recycler_menu: RecyclerView? = null

    companion object {
        var homefragment: MathsTableFragment? = null
        fun getInstance(): MathsTableFragment {
            homefragment = MathsTableFragment()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.table_layout, container, false)
        edt_num = view.findViewById(R.id.edt_num) as EditText

        edt_num!!.setSelection(edt_num!!.getText().length); // to do by sachin  for cursor change


        recycler_menu = view.findViewById(R.id.recycler_menu) as RecyclerView
        numbersList.clear()
        var table_of_number = edt_num!!.text.toString()
        try {
            if (table_of_number.toInt() > 0 && table_of_number.toInt() < 100) {
                layoutManager = LinearLayoutManager(context)
                recycler_menu!!.layoutManager = layoutManager
                for (i in 1..10) {
                    numbersList.add(table_of_number.toInt())
                }
                var adapter = TableAdapter(context!!, numbersList)
                recycler_menu!!.adapter = adapter
            } else {
                Toast.makeText(
                    context,
                    "Only single and two digit number multiplication can be done.",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        }
        catch (ex:NumberFormatException){
            Toast.makeText(
                context,
                "Only single and two digit number multiplication can be done.",
                Toast.LENGTH_SHORT
            ).show()
        }

        edt_num!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //avoid triggering event when text is empty

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {


                if (s!!.length > 0 ) {
                    numbersList.clear()
                    var table_of_number = edt_num!!.text.toString()
                    if (table_of_number.toInt() > 0 && table_of_number.toInt() < 100) {
                        layoutManager = LinearLayoutManager(context)
                        recycler_menu!!.layoutManager = layoutManager
                        for (i in 1..10) {
                            numbersList.add(table_of_number.toInt())
                        }
                        var adapter = TableAdapter(context!!, numbersList)
                        recycler_menu!!.adapter = adapter
                    } else {
                        Toast.makeText(
                            context,
                            "Only single and two digit number multiplication can be done.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                }
                catch (ex:java.lang.NumberFormatException){
                    Toast.makeText(
                        context,
                        "Only single and two digit number multiplication can be done.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }


        })

        return view
    }
}