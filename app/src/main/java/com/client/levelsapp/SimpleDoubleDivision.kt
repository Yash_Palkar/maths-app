package com.client.levelsapp

interface SimpleDoubleDivision {
    fun apply(x1: Int, x2: Int,x3:Int): Float
}