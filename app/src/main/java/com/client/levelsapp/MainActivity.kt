package com.client.levelsapp

import android.app.AlarmManager
import android.app.AlertDialog
import android.app.Dialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.net.URL
import java.util.*


class MainActivity : AppCompatActivity() {
    var dLayout: DrawerLayout? = null
    var redirect = ""
    var fragment: HomeFragment? = null
    var title = ""
    var easy: String? = null
    var medium: String? = null
    var level_difficulty = ""
    var mybundle: Bundle? = null
    var level_category = ""
    var drawerToggle: ActionBarDrawerToggle? = null
    private var mToolBarNavigationListenerIsRegistered = false
    var level = ""
    var level_no = 0
    var alert: AlertDialog? = null
    var tableId: String? = null
    var formulaId: String? = null
    var mathsTrick: String? = null
    var achivement_Id: String? = null
    var resetId: String? = null
    var prefconfig: SharedPrefrenceClass? = null
    lateinit var userSession: UserSession
//    var drawer_title: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Configure action bar
        setSupportActionBar(toolbar)

        MobileAds.initialize(this, resources.getString(R.string.banner_app_id))
        val mAdView: AdView = findViewById(R.id.banner_ad)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
        val actionBar = supportActionBar
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        prefconfig = SharedPrefrenceClass(applicationContext)

        userSession = UserSession(applicationContext)


//        actionBar?.title = "Hello Toolbar"
//        drawer_title = findViewById(R.id.drawer_title)
//        drawer_title!!.setOnClickListener { }

//        // Initialize the action bar drawer toggle instance
//        drawerToggle = object : ActionBarDrawerToggle(
//            this,
//            drawer_layout,
//            toolbar,
//            R.string.drawer_open,
//            R.string.drawer_close
//        ) {
//            override fun onDrawerClosed(view: View) {
//                super.onDrawerClosed(view)
//                //toast("Drawer closed")
//            }
//
//            override fun onDrawerOpened(drawerView: View) {
//                super.onDrawerOpened(drawerView)
//                //toast("Drawer opened")
//            }
//        }
//
//        // Configure the drawer layout to add listener and show icon on toolbar
//        drawerToggle!!.isDrawerIndicatorEnabled = true
//        drawer_layout.addDrawerListener(drawerToggle!!)
//        drawerToggle!!.syncState()
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            drawerToggle!!.getDrawerArrowDrawable().setColor(getColor(R.color.white))
//        } else {
//            drawerToggle!!.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white))
//        }

        //alarmService

//        val alarmIntent = Intent(this, AlarmReceiver::class.java)
//        val pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
//        val manager =
//            getSystemService(Context.ALARM_SERVICE) as AlarmManager
//        val calendar = Calendar.getInstance()
//        calendar.timeInMillis = System.currentTimeMillis()
//        calendar[Calendar.HOUR_OF_DAY] = 18
//        calendar[Calendar.MINUTE] = 0
//        manager.setRepeating(
//            AlarmManager.RTC_WAKEUP,
//            calendar.timeInMillis,
//            AlarmManager.INTERVAL_DAY,
//            pendingIntent
//        )



        mybundle = intent.extras
        // intent = intent
        if (mybundle != null) {
            if (mybundle!!.getString("redirect") != null) {
                redirect = mybundle!!.getString("redirect")!!
            }
            if (mybundle!!.getString("title") != null) {
                title = mybundle!!.getString("title")!!
            }
            if (mybundle!!.getString("level") != null) {
                level = mybundle!!.getString("level")!!
            }
            if (mybundle!!.getInt("level_no") != null) {
                level_no = mybundle!!.getInt("level_no")!!
            }
            if (mybundle!!.getString("easy") != null) {
                easy = mybundle!!.getString("easy")!!
            }
            if (mybundle!!.getString("medium") != null) {
                medium = mybundle!!.getString("medium")!!
            }
            if (mybundle!!.getString("tableId") != null) {
                tableId = mybundle!!.getString("tableId")!!
            }
            if (mybundle!!.getString("formulaId") != null) {
                formulaId = mybundle!!.getString("formulaId")!!
            }
            if (mybundle!!.getString("mathsTrick") != null) {
                mathsTrick = mybundle!!.getString("mathsTrick")!!
            }
            if (mybundle!!.getString("achiveId") != null) {
                achivement_Id = mybundle!!.getString("achiveId")!!
            }
            if (mybundle!!.getString("resetId") != null) {
                resetId = mybundle!!.getString("resetId")!!
            }
        }


        if (redirect == "") {
            val firstFragment = HomeFragment()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()
            transaction.add(R.id.content_frame, firstFragment)
//            transaction.addToBackStack("activity_home")
            transaction.remove(Fragment()) // to do by sachin
            transaction.commit()

        } else {
            fragmenttoreplace(redirect)
        }


        // Set navigation view navigation item selected listener
//        navigation_view.setNavigationItemSelectedListener {
//            when (it.itemId) {
//                R.id.action_tables -> {
//                    clearBackStack()
//                    enableViews(true, drawerToggle!!)
//                    val firstFragment = MathsTableFragment()
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_tables")
//                    transaction.commit()
//                }
//                R.id.action_formula -> {
//                    clearBackStack()
//                    enableViews(true, drawerToggle!!)
//                    val bundle = Bundle()
//                    bundle.putString("from_where", "1")
//                    val firstFragment = ShowDataFormulaTrick()
//                    firstFragment.arguments = bundle
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_formula")
//                    transaction.commit()
//                }
//                R.id.action_math -> {
//                    clearBackStack()
//                    enableViews(true, drawerToggle!!)
//                    val bundle = Bundle()
//                    bundle.putString("from_where", "2")
//                    val firstFragment = ShowDataFormulaTrick()
//                    firstFragment.arguments = bundle
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_math")
//                    transaction.commit()
//                }
//                R.id.action_achievement -> {
//                    // Multiline action
//                    clearBackStack()
//                    enableViews(true, drawerToggle!!)
//                    val bundle = Bundle()
//                    bundle.putString("from_where", "2")
//                    val firstFragment = AchievementFragment()
//                    firstFragment.arguments = bundle
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_achievement")
//                    transaction.commit()
//                }
//                R.id.action_reset -> {
//                    prefconfig!!.clearpreference()
//                    val firstFragment = HomeFragment()
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("home_fragment")
//                    transaction.commit()
//                }
////                R.id.privacy_policy -> {
////                    prefconfig!!.clearpreference()
////                    val firstFragment = PrivacyFragment()
////                    val manager = supportFragmentManager
////                    val transaction = manager.beginTransaction()
////                    transaction.add(R.id.content_frame, firstFragment)
////                    transaction.addToBackStack("action_policy")
////                    transaction.commit()
////                }
//
//            }
//            // Close the drawer
//            drawer_layout.closeDrawer(GravityCompat.START)
//            true
//        }
    }

//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//
//        return if (drawerToggle!!.onOptionsItemSelected(item)) {
//            true
//        } else super.onOptionsItemSelected(item)
//
//
//    }

    private fun clearBackStack() {
        var fragmentManager = supportFragmentManager
        for (i in 0 until supportFragmentManager.backStackEntryCount)
            fragmentManager.popBackStack()
    }

    public fun fragmenttoreplace(redirect: String) {
        when (redirect) {
            "0" -> {
                // enableViews(true, drawerToggle!!)
                val bundle = Bundle()
                bundle.putString("title", title)
                bundle.putInt("getLevelNumber", getLevelNumber(title))
                val firstFragment = LevelViewFragment()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("levelview_fragment")
                transaction.remove(Fragment()) // tp
                transaction.commit()

//                enableViews(true, drawerToggle!!)
//                var intent = Intent(this, EasyTricky::class.java)
//                intent.putExtra("title", title)
//                intent.putExtra("getLevelNumber", getLevelNumber(title))
//                startActivity(intent)

            }
            "1" -> {
//                enableViews(true, drawerToggle!!)
//                var intent = Intent(this, QuestionView::class.java)
//                intent.putExtra("title", title)
//                intent.putExtra("level", level)
//                intent.putExtra("level_no", getLevelNumber(title))
//                startActivity(intent)

                val bundle = Bundle()
                bundle.putString("title", title)
                bundle.putString("level", level)
                bundle.putInt("level_no", getLevelNumber(title))
                val firstFragment = QuestionViewFragment()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("questionview_fragment")
                transaction.remove(Fragment()) // tp
                transaction.commit()
            }
            "3" -> {
//                enableViews(true, drawerToggle!!)
//                var intent = Intent(this, QuestionView::class.java)
//                intent.putExtra("title", title)
//                intent.putExtra("level", level)
//                intent.putExtra("level_no", getLevelNumber(title))
//                startActivity(intent)

//                enableViews(true, drawerToggle!!)
//                var intent = Intent(this, QuestionView::class.java)
//                intent.putExtra("title", title)
//                startActivity(intent)

                val bundle = Bundle()
                bundle.putString("title", title)
                val firstFragment = TrickyFragment()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("tricky_fragment")
                transaction.remove(Fragment())
                transaction.commit()
            }
            "4" -> {
                // enableViews(true, drawerToggle!!)
                val bundle = Bundle()
                bundle.putString("title", title)
                bundle.putInt("level_no", level_no)
                bundle.putString("medium", medium)
                val firstFragment = HighScore()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("highscore")
                transaction.remove(Fragment()) // to do by sachin for back presses twice
                transaction.commit()
            }
            "5" -> {
//                enableViews(true, drawerToggle!!)
                val bundle = Bundle()
                bundle.putString("TableId", tableId)
                val firstFragment = MathsTableFragment()
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("table_layout") // to do by sachin
                transaction.remove(Fragment())
                clearBackStack()
                transaction.commit()


            }
            "6" -> {
//                enableViews(true, drawerToggle!!)
                val bundle = Bundle()
                bundle.putString("formulaId", formulaId)
                bundle.putString("from_where", "1")
                val firstFragment = ShowDataFormulaTrick()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("activity_formula_trick")
                transaction.remove(Fragment())
                transaction.commit()
            }
            "7" -> {
//                enableViews(true, drawerToggle!!)
                val bundle = Bundle()
                bundle.putString("mathsTrick", mathsTrick)
                bundle.putString("from_where", "17")
                //ShowDataFormulaTrick

                val firstFragment = ShowDataFormulaTrick()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("activity_formula_trick")
                transaction.remove(Fragment())
                transaction.commit()
            }
            "8" -> {

                val bundle = Bundle()
                bundle.putString("from_where", "2")
                bundle.putString("achiveId", achivement_Id)
                val firstFragment = AchievementFragment()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("activity_achievement")
                transaction.remove(Fragment())

                transaction.commit()
            }
            "9" -> {
                val bundle = Bundle()
                bundle.putString("resetId", resetId)
                prefconfig!!.clearpreference()

                val firstFragment = HomeFragment()
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("home_fragment")
                transaction.remove(Fragment())
                transaction.commit()
            }
            else -> {
                val firstFragment = HomeFragment()
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("home_fragment")
                transaction.remove(Fragment())
                transaction.commit()
            }
        }
    }

    private fun Context.toast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    fun enableViews(enable: Boolean, drawerToggle: ActionBarDrawerToggle) {

        if (enable) {
//            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
//            drawerToggle!!.setDrawerIndicatorEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            if (!mToolBarNavigationListenerIsRegistered) {
                drawerToggle!!.setToolbarNavigationClickListener(View.OnClickListener {
                    val f = this.supportFragmentManager.findFragmentById(R.id.content_frame)
                    if (f is QuestionViewFragment) {
                        (f as QuestionViewFragment).setBackDialog()
                    } else if (f is TrickyFragment) {
                        (f as TrickyFragment).setBackDialog()
                    } else if (f is HighScore) {
                        (f as HighScore).btn_dismiss!!.performClick()
                    } else {
                        onBackPressed()
                    }
                })

                mToolBarNavigationListenerIsRegistered = true
            }

        } else {
//            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            drawerToggle!!.isDrawerIndicatorEnabled = true
            drawerToggle!!.toolbarNavigationClickListener = null
            mToolBarNavigationListenerIsRegistered = false
        }

    }

    companion object {
        fun setName(sign: String): String {
            var name: String = ""
            when (sign) {
                "+" -> name = "Addition"
                "-" -> name = "Subtraction"
                "*" -> name = "Multiplication"
                "/" -> name = "Division"
                "+ +" -> name = "Double Add"
                "- -" -> name = "Double Subtract"
                "* *" -> name = "Double Multiply"
                "/ /" -> name = "Double Divide"
                "+ -" -> name = "Add & Subtract"
                "+ *" -> name = "Add & Multiply"
                "+ /" -> name = "Add & Divide"
                "- *" -> name = "Subtract & Multiply"
                "- /" -> name = "Subtract & Divide"
                "* /" -> name = "Multiply & Divide"
                "+ - *" -> name = "Add, Subtract  & Multiply"
                "- * /" -> name = "Subtract, Multiply  & Divide"
                "* / +" -> name = "Multiply, Divide  & Add"
                "+ - * /" -> name = "Ques On BODMASS"
            }

            return name
        }

        fun getLevelNumber(sign: String): Int {
            var name = 0
            when (sign) {
                "+" -> name = 0
                "-" -> name = 1
                "*" -> name = 2
                "/" -> name = 3
                "+ +" -> name = 4
                "- -" -> name = 5
                "* *" -> name = 6
                "/ /" -> name = 7
                "+ -" -> name = 8
                "+ *" -> name = 9
                "+ /" -> name = 10
                "- *" -> name = 11
                "- /" -> name = 12
                "* /" -> name = 13
                "+ - *" -> name = 14
                "- * /" -> name = 15
                "* / +" -> name = 16
                "+ - * /" -> name = 17
            }

            return name
        }
    }

    fun callExitDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Exit")
        builder.setCancelable(false)
        builder.setMessage("Are you sure you want to exit the application?")
        builder.setPositiveButton("Yes") { dialog, which ->
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }
        builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
        alert = builder.create()
        alert!!.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
//        return super.onSupportNavigateUp()

    }

    override fun onBackPressed() {
        super.onBackPressed()
//       startActivity(Intent(this, HomeActivity::class.java)) // to do by sachin
//        callExitDialog()
    }
    private fun callLoginDialog() {
        var myDialog: Dialog? = null
        myDialog = Dialog(this)
        myDialog.setContentView(R.layout.logindialog)
        myDialog.setCancelable(false)
        val login = myDialog.findViewById(R.id.loginbtn) as Button
        val emailaddr = myDialog.findViewById(R.id.mailtxt) as EditText
        val cancel = myDialog.findViewById(R.id.cancelbtn) as Button
        myDialog.show()
        login.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if ((!emailaddr.text.isEmpty())&& emailaddr.text.toString().isValidEmail()) {
                    val task = someTask()
                    task.execute(emailaddr.text.toString())
                    myDialog.dismiss()

                }
                else{
                    Toast.makeText(applicationContext,"Please Check Email id",Toast.LENGTH_SHORT).show()
                }
            }
        })
        cancel.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                myDialog.cancel()

            }
        })
    }
    fun String.isValidEmail() =
        isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    inner class someTask : AsyncTask<String, String, String>() {
        var geocoder: Geocoder? = null
        var addresses: List<Address>? = null
        var location: Location? = null
        override fun doInBackground(vararg params: String?): String? {
            val country = applicationContext.resources.configuration.locale.getDisplayCountry()
            val sdk_version_number = Build.VERSION.SDK
            val versionCode = BuildConfig.VERSION_NAME
            var response = ""

            val tz = TimeZone.getDefault()
            Log.d("Apploc",tz.getDisplayName())
//            val context: Context = MainActivity().getApplicationContext()
//            val ip: String =Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
//                Formatter.formatIpAddress(context.getSystemService(Context.WIFI_SERVICE).connectionInfo.ipAddress)
//            if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            }
//            try {
//                addresses =
//                    location?.getLatitude()?.let { geocoder?.getFromLocation(it, location!!.getLongitude(), 10) }
//                val address: Address = addresses!!.get(0)
//                 Log.d("Address",address.getCountryCode())
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                applicationContext.getResources().getConfiguration().getLocales().get(0).getCountry()
            } else {
                applicationContext.getResources().getConfiguration().locale.getCountry()
            }
            var hashMap: HashMap<String, String> = HashMap<String, String>()
            hashMap.put("email", params[0].toString())
            hashMap.put("device", "2")
            hashMap.put("version", sdk_version_number)
            hashMap.put("country", country)
            hashMap.put("app_version", versionCode.toString())
            var httpapicall=HttpApiCalling();
            val url = URL("http://198.58.97.191/allapps/api/mathapp/login")

            response= httpapicall.apicall(hashMap,url).toString()

            response
            var array = JSONObject(response)

            var sucess = array.getString("success")
            var islogin:Boolean= sucess!!.toBoolean()
            var data=array.getJSONObject("data")
            var message=data.getString("message")
            var users_details=data.getJSONObject("users_details")
            var created_at=users_details.getString("created_at")
            var updated_at=users_details.getString("updated_at")
            var email=users_details.getString("email")
            userSession.loginSession(islogin,email,created_at,updated_at,"0")
            Log.d("sucess",sucess)

            Log.e("JSON", response)


            return null
        }

//        private fun getQuery(hashMap: HashMap<String, String>): String {
//            val result = StringBuilder()
//            var first = true
//            var counter = 1
//            var urlParameters = ""
//            val myIterator: Iterator<*> = hashMap.keys.iterator()
//            while (myIterator.hasNext()) {
//                val key: String = myIterator.next().toString()
//                val value: String? = hashMap.get(key)
//                if (counter < hashMap.size) urlParameters = urlParameters + key + "=" + value + "&"
//                if (counter == hashMap.size) urlParameters = urlParameters + key + "=" + value
//                counter++
//            }
//
//
//            return urlParameters.toString()
//        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            // ...
        }
    }
}



