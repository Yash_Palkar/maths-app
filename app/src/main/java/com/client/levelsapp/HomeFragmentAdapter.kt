package com.client.levelsapp

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*

class HomeFragmentAdapter : RecyclerView.Adapter<HomeFragmentAdapter.MyViewHolder> {
    var prefconfig: SharedPrefrenceClass? = null
    var nextintent = ""
    var bundle:Bundle?=null
    var alert: AlertDialog? = null

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
//        ChangeColour(context!!, vh!!.rel_main!!).changecommonimage(subtitle_array!!.get(p1))
        when (category_array!!.get(p1)) {
            "+" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_1
            )
            "-" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_1
            )
            "*" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_1
            )
            "/" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_1
            )
            "+ +" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_1
            )
            "- -" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_1
            )
            "* *" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_21
            )
            "/ /" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_21
            )
            "+ -" -> p0!!.card_view_image!!.background = ContextCompat.getDrawable(
                context!!,
                R.drawable.img_21
            )
            "+ *" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_21
                )
            )
            "+ /" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_21
                )
            )
            "- *" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_21
                )
            )
            "- /" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_20
                )
            )
            "* /" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_20
                )
            )
            "+ - *" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_20
                )
            )
            "- * /" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_20
                )
            )
            "* / +" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_20
                )
            )
            "+ - * /" -> p0!!.card_view_image!!.setBackground(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.img_20
                )
            )
        }

        if (prefconfig!!.readLevelscoreeasy(p1) >= 300) {
            p0!!.img_star1!!.setImageResource(R.drawable.star_yellow)
        } else {
            p0!!.img_star1!!.setImageResource(R.drawable.star)
        }
        if (prefconfig!!.readLevelscoreeasy(p1) >= 600) {
            p0!!.img_star2!!.setImageResource(R.drawable.star_yellow)
        } else {
            p0!!.img_star2!!.setImageResource(R.drawable.star)
        }
        if (prefconfig!!.readLevelscoreeasy(p1) >= 900) {
            p0!!.img_star3!!.setImageResource(R.drawable.star_yellow)
        } else {
            p0!!.img_star3!!.setImageResource(R.drawable.star)
        }
        var count = prefconfig!!.readLevelscoreeasy(p1)
        p0!!.txt_score_number!!.setText("$count/1570")

        if (p1 % 3 == 3) {
            val cardViewMarginParams =
                p0!!.lin_main_layout!!.layoutParams as ViewGroup.MarginLayoutParams
            cardViewMarginParams.setMargins(20, 20, 20, 20)
            cardViewMarginParams.height = 350
            p0!!.lin_main_layout!!.requestLayout()
        } else {
            val cardViewMarginParams =
                p0!!.lin_main_layout!!.layoutParams as ViewGroup.MarginLayoutParams
            cardViewMarginParams.setMargins(20, 20, 20, 20)
            p0!!.lin_main_layout!!.requestLayout()
        }
        when (p1) {
            0 -> p0!!.lin_blur_bg!!.setBackgroundResource(0)
            //TODO levels unlock
            1 -> if (getscore(1) >= 0) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            2 -> if (getscore(2) >= 0) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            3 -> if (getscore(3) >= 0) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            4 -> if (getscore(4) >= 0) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            5 -> if (getscore(5) >= 0) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            6 -> if (getscore(6) >= 3300) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            7 -> if (getscore(7) >= 3900) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            8 -> if (getscore(8) >= 4500) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            9 -> if (getscore(9) >= 5100) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            10 -> if (getscore(10) >= 5700) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            11 -> if (getscore(11) >= 6300) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            12 -> if (getscore(12) >= 6900) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            13 -> if (getscore(13) >= 7500) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            14 -> if (getscore(14) >= 8100) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            15 -> if (getscore(15) >= 8700) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            16 -> if (getscore(16) >= 9300) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
            17 -> if (getscore(17) >= 9900) p0!!.lin_blur_bg!!.setBackgroundResource(0) else p0!!.lin_blur_bg!!.setBackgroundColor(
                context!!.resources.getColor(R.color.transparent_white)
            )
        }

        p0!!.level_set!!.setText(title_array!!.get(p1))
        p0!!.category_set!!.setText(category_array!!.get(p1))
        p0!!.difficulty_set!!.setText(difficulty_array!!.get(p1))

        p0!!.lin_main_layout!!.setOnClickListener {
            when (p1) {
                0 -> nextintent = "intent"
                //TODO levels unlocked
                1 -> if (getscore(1) >= 0) nextintent = "intent" else unlocklevel(1)
                2 -> if (getscore(2) >= 0) nextintent = "intent" else unlocklevel(3)
                3 -> if (getscore(3) >= 0) nextintent = "intent" else unlocklevel(5)
                4 -> if (getscore(4) >= 0) nextintent = "intent" else unlocklevel(7)
                5 -> if (getscore(5) >= 0) nextintent = "intent" else unlocklevel(9)
                6 -> if (getscore(6) >= 3300) nextintent = "intent" else unlocklevel(11)
                7 -> if (getscore(7) >= 3900) nextintent = "intent" else unlocklevel(13)
                8 -> if (getscore(8) >= 4500) nextintent = "intent" else unlocklevel(15)
                9 -> if (getscore(9) >= 5100) nextintent = "intent" else unlocklevel(17)
                10 -> if (getscore(10) >= 5700) nextintent = "intent" else unlocklevel(19)
                11 -> if (getscore(11) >= 6300) nextintent = "intent" else unlocklevel(21)
                12 -> if (getscore(12) >= 6900) nextintent = "intent" else unlocklevel(23)
                13 -> if (getscore(13) >= 7500) nextintent = "intent" else unlocklevel(25)
                14 -> if (getscore(14) >= 8100) nextintent = "intent" else unlocklevel(27)
                15 -> if (getscore(15) >= 8700) nextintent = "intent" else unlocklevel(29)
                16 -> if (getscore(16) >= 9300) nextintent = "intent" else unlocklevel(31)
                17 -> if (getscore(17) >= 9900) nextintent = "intent" else unlocklevel(33)


            }
            if (nextintent.equals("intent")) {
                var intent = Intent(context, MainActivity::class.java)
                intent.putExtra("redirect", "0")
                intent.putExtra("title", category_array!![p1])
                context!!.startActivity(intent)
            }



        }
        if (nextintent.equals("intent")) {
            var intent = Intent(context, HomeActivity::class.java)  // to do Main ro Home activity
            intent.putExtra("redirect", "0")
            intent.putExtra("title", category_array)
            context!!.startActivity(intent)
        }
//        vh!!.subtitle!!.setText(subtitle_array!!.get(p1))
    }

    var title_array: ArrayList<String>? = null
    var category_array: ArrayList<String>? = null
    var difficulty_array: ArrayList<String>? = null

    var context: Context? = null
    //    var vh: MyViewHolder? = null
    var i = 1

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
//        var v = LayoutInflater.from(p0.getContext()).inflate(R.layout.home_item, p0, false)
//        vh = MyViewHolder(v)
//        return vh!!
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.home_item_new, p0, false))

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun getItemCount(): Int {
        return title_array!!.size
    }

    constructor(
        context: Context,
        title_array: ArrayList<String>,
        category_array: ArrayList<String>,
        difficulty_array: ArrayList<String>
    ) {
        this.context = context
        this.title_array = title_array
        this.category_array = category_array
        this.difficulty_array = difficulty_array
        prefconfig = SharedPrefrenceClass(context)
        setHasStableIds(true)
    }


    class MyViewHolder : RecyclerView.ViewHolder {
        var level_set: TextView? = null
        var category_set: TextView? = null
        var difficulty_set: TextView? = null
        var rel_main: RelativeLayout? = null
        var txt_score_number: TextView? = null
        var img_star1: ImageView? = null
        var img_star2: ImageView? = null
        var img_star3: ImageView? = null
        var card_view_image: ImageView? = null
        var lin_blur_bg: LinearLayout? = null
        var lin_main_layout: CardView? = null

        constructor(itemView: View) : super(itemView) {
            level_set = itemView.findViewById(R.id.txt_level_set) as TextView
            category_set = itemView.findViewById(R.id.txt_category_set) as TextView
            difficulty_set = itemView.findViewById(R.id.txt_difficulty_set) as TextView
            txt_score_number = itemView.findViewById(R.id.txt_score_number) as TextView
            img_star1 = itemView.findViewById(R.id.img_star1) as ImageView
            img_star2 = itemView.findViewById(R.id.img_star2) as ImageView
            img_star3 = itemView.findViewById(R.id.img_star3) as ImageView
            card_view_image = itemView.findViewById(R.id.card_view_image) as ImageView
            lin_blur_bg = itemView.findViewById(R.id.lin_blur_bg) as LinearLayout
//            subtitle = itemView.findViewById(R.id.txt_dash_subtitle) as TextView
            lin_main_layout = itemView.findViewById(R.id.lin_main_layout) as CardView
        }
    }

    fun getscore(k: Int): Int {
        var totalscore = 0
        for (i in 0..k) {
            totalscore += prefconfig!!.readLevelscoreeasy(i)
            Log.d("fgfgf", "$i::$k" + totalscore!!.toString())
        }
        return totalscore
    }

    fun unlocklevel(star: Int) {

        val dialog1 = context?.let { Dialog(it) }
        dialog1!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog1.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog1.setCancelable(false)
        dialog1.setContentView(R.layout.dialog_next_question)

        val txt_title = dialog1.findViewById(R.id.txt_title) as TextView
        val txt_desc = dialog1.findViewById(R.id.txt_desc) as TextView
        val txt_btn_next = dialog1.findViewById(R.id.txt_btn_next) as TextView
        txt_title.setText("Level Locked")
        txt_desc.setText("Collect $star star to unlock this level.")
        txt_btn_next.setText("OK")
        txt_btn_next.setOnClickListener {
            dialog1.dismiss()
        }

        dialog1.show()

    }
}