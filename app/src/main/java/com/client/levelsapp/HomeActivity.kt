package com.client.levelsapp


import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.media.AudioManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.customtabs.BuildConfig
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*
import org.json.JSONObject
import java.net.URL
import java.util.*


class HomeActivity : AppCompatActivity(), View.OnClickListener {


    var imageView: ImageView? = null
    var text_option1: TextView? = null
    var text_option2: TextView? = null
    var text_option3: TextView? = null
    var text_option4: TextView? = null
    var add_bt1: Button? = null
    var sub_bt2: Button? = null
    var mul_bt3: Button? = null
    var div_bt4: Button? = null
    var moreAps: Button? = null
    var share_app: Button? = null
    var sound: Button? = null
    var rateUs: Button? = null
    var volume: Button? = null
    var share: Button? = null
    var redirect = ""
    var alert: AlertDialog? = null
    var title = ""
    var nextintent = ""
    var level = ""
    var level_no = 0
    var i: Intent? = null
    var context: Context? = null
    var menu: TextView? = null
    var mybundle: Bundle? = null
    var sharedPreferences: SharedPreferences? = null
    var isMute: Boolean? = null
    lateinit var checknet:Checknet
    //    var category_array=ArrayList<String>();
    var mainActivity = MainActivity();
    var easy:String?=null
    var medium:String?=null
    lateinit var userSession: UserSession
    private val channelId = "Personal_Notification"
    private val channelName = "Astrology"
    private val channel_desc = "Astrology Notification"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        checknet= Checknet()
        var add_bt1 = findViewById<Button>(R.id.add_bt1)
        var sub_bt2 = findViewById<Button>(R.id.sub_bt2)
        var mul_bt3 = findViewById<Button>(R.id.mul_bt3)
        var div_bt4 = findViewById<Button>(R.id.div_bt4)
        var allin1_bt5 = findViewById<Button>(R.id.allin1_bt5)
        var menu = findViewById<TextView>(R.id.menu)
        //var backbutton=findViewById<ImageButton>(R.id.backbutton)
        rateUs = findViewById(R.id.rateUs)
        volume = findViewById(R.id.volume)
        share = findViewById(R.id.share)
        moreAps = findViewById(R.id.moreAps)
        userSession = UserSession(applicationContext)
        var isconnnected:Boolean=checknet.checkForInternet(this@HomeActivity)
        var sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        var sh = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        var a = sh.getInt("appcounter", 1)
        var myEdit = sharedPreferences.edit()
        myEdit.putInt("appcounter", (a+1))
        myEdit.commit()
        sh = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        myEdit = sh.edit()
        a = sh.getInt("appcounter", 1)
        if (a % 2 == 0 && !userSession.isUserLoggedIn() && isconnnected) {
            callLoginDialog()
            myEdit.putInt("appcounter", (0))
            myEdit.commit()
        }
        dailyNotification()

        var pref = PreferenceManager.getDefaultSharedPreferences(this)

        var isMute = pref.getBoolean("MuteState", false)
        if (isMute == true) {
            volume!!.setBackgroundResource(R.drawable.mutelogo)
        } else {
            volume!!.setBackgroundResource(R.drawable.soundlogo)
        }
        add_bt1.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, LevelViewActivity::class.java)
            intent.putExtra("redirect", "0")
            intent.putExtra("title", "+")
            intent.putExtra("easy", "+")
            intent.putExtra("medium", "+ +")
            startActivity(intent)
            finish()

        })


        sub_bt2.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, LevelViewActivity::class.java)
            intent.putExtra("redirect", "0")
            intent.putExtra("title", "-")
            intent.putExtra("easy", "-")
            intent.putExtra("medium", "- -")
            startActivity(intent)
        })

        mul_bt3.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, LevelViewActivity::class.java)
            intent.putExtra("redirect", "0")
            intent.putExtra("title", "*")
            intent.putExtra("easy", "*")
            intent.putExtra("medium", "* *")
            startActivity(intent)
        })

        div_bt4.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, LevelViewActivity::class.java)
            intent.putExtra("redirect", "0")
            intent.putExtra("title", "/")
            intent.putExtra("easy", "/")
            intent.putExtra("medium", "/ /")

            startActivity(intent)
        })

        allin1_bt5.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        })

        menu.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, Menu::class.java)
            startActivity(intent)
        })

        share!!.setOnClickListener(View.OnClickListener {

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            var shareMessage =
                "\nLet me recommend you this application\n\n" //todo by sachin share app link added ..
            shareMessage =
                shareMessage + "https://play.google.com/store/apps/details?id=com.client.levelsapp&hl=en_IN" + BuildConfig.APPLICATION_ID + "\n\n"
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            sendIntent.type = "text/plain"
            startActivity(sendIntent)

        })
        rateUs!!.setOnClickListener(View.OnClickListener {

            i = Intent(android.content.Intent.ACTION_VIEW)
            i!!.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.client.levelsapp&hl=en_IN" + BuildConfig.APPLICATION_ID + "\n\n"))
            startActivity(i)

        })

        moreAps!!.setOnClickListener(View.OnClickListener {
            i = Intent(android.content.Intent.ACTION_VIEW)
            i!!.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Techathalon&hl=en_IN"))
            startActivity(i)

        })

        volume!!.setOnClickListener(View.OnClickListener {
            // TODO Auto-generated method stub
            volume!!.setSelected(true);
            var pref = PreferenceManager.getDefaultSharedPreferences(this)

            var isMute = pref.getBoolean("MuteState", false)


            if (isMute != null) {

                if (isMute == true) {
                    // Sound  is disabled

                    volume!!.isSoundEffectsEnabled = false
                    var manager1 = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                    manager1.setStreamMute(AudioManager.STREAM_MUSIC, false)
                    volume!!.setBackgroundResource(R.drawable.soundlogo)
                    var pref = PreferenceManager.getDefaultSharedPreferences(this)
                    var editor = pref.edit()
                    editor
                        .putBoolean("MuteState", false)
                        .apply()

                } else {
                    // Sound is enabled
                    volume!!.isSoundEffectsEnabled = true
                    var manager2 = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                    manager2.setStreamMute(AudioManager.STREAM_MUSIC, true)
                    volume!!.setBackgroundResource(R.drawable.mutelogo)
                    var pref = PreferenceManager.getDefaultSharedPreferences(this)
                    var editor = pref.edit()
                    editor
                        .putBoolean("MuteState", true)
                        .apply()

                }
            } else {

                var manager1 = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                manager1.setStreamMute(AudioManager.STREAM_MUSIC, true)
                volume!!.setBackgroundResource(R.drawable.mutelogo)

                var pref = PreferenceManager.getDefaultSharedPreferences(this)
                var editor = pref.edit()
                editor
                    .putBoolean("MuteState", true)
                    .apply()


            }


        })



//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val notificationChannel = NotificationChannel(
//                MyNewIntentService.channelId,
//                MyNewIntentService.channelName,
//                NotificationManager.IMPORTANCE_HIGH
//            )
//            notificationChannel.description = MyNewIntentService.channel_desc
//            val notificationManager: NotificationManager
//            notificationManager =
//                this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//            notificationManager.createNotificationChannel(notificationChannel)
//        }
   }
    private fun callLoginDialog() {
        var myDialog: Dialog? = null
        myDialog = Dialog(this)
        myDialog.setContentView(R.layout.logindialog)
        myDialog.setCancelable(false)
        val login = myDialog.findViewById(R.id.loginbtn) as Button
        val emailaddr = myDialog.findViewById(R.id.mailtxt) as EditText
        val cancel = myDialog.findViewById(R.id.cancelbtn) as Button
        myDialog.show()
        login.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if ((!emailaddr.text.isEmpty())&& emailaddr.text.toString().isValidEmail()) {
                    val task = someTask()
                    task.execute(emailaddr.text.toString())
                    myDialog.dismiss()

                }
                else{
                    Toast.makeText(applicationContext,"Please Check Email id", Toast.LENGTH_SHORT).show()
                }
            }
        })
        cancel.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                myDialog.cancel()

            }
        })
    }
    fun String.isValidEmail() =
        isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    inner class someTask : AsyncTask<String, String, String>() {
        var geocoder: Geocoder? = null
        var addresses: List<Address>? = null
        var location: Location? = null
        override fun doInBackground(vararg params: String?): String? {
            val country = applicationContext.resources.configuration.locale.getDisplayCountry()
            val sdk_version_number = Build.VERSION.SDK
            val versionCode = com.client.levelsapp.BuildConfig.VERSION_NAME
            var response = ""

            val tz = TimeZone.getDefault()
            Log.d("Apploc",tz.getDisplayName())
//            val context: Context = MainActivity().getApplicationContext()
//            val ip: String =Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
//                Formatter.formatIpAddress(context.getSystemService(Context.WIFI_SERVICE).connectionInfo.ipAddress)
//            if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            }
//            try {
//                addresses =
//                    location?.getLatitude()?.let { geocoder?.getFromLocation(it, location!!.getLongitude(), 10) }
//                val address: Address = addresses!!.get(0)
//                 Log.d("Address",address.getCountryCode())
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                applicationContext.getResources().getConfiguration().getLocales().get(0).getCountry()
            } else {
                applicationContext.getResources().getConfiguration().locale.getCountry()
            }
            var hashMap: HashMap<String, String> = HashMap<String, String>()
            hashMap.put("email", params[0].toString())
            hashMap.put("device", "2")
            hashMap.put("version", sdk_version_number)
            hashMap.put("country", country)
            hashMap.put("app_version", versionCode.toString())
            var httpapicall=HttpApiCalling();
            val url = URL("http://198.58.97.191/allapps/api/mathapp/login")

            response= httpapicall.apicall(hashMap,url).toString()

            response
            var array = JSONObject(response)

            var sucess = array.getString("success")
            var islogin:Boolean= sucess!!.toBoolean()
            var data=array.getJSONObject("data")
            var message=data.getString("message")
            var users_details=data.getJSONObject("users_details")
            var created_at=users_details.getString("created_at")
            var updated_at=users_details.getString("updated_at")
            var email=users_details.getString("email")
            userSession.loginSession(islogin,email,created_at,updated_at,"0")
            Log.d("sucess",sucess)

            Log.e("JSON", response)


            return null
        }

//        private fun getQuery(hashMap: HashMap<String, String>): String {
//            val result = StringBuilder()
//            var first = true
//            var counter = 1
//            var urlParameters = ""
//            val myIterator: Iterator<*> = hashMap.keys.iterator()
//            while (myIterator.hasNext()) {
//                val key: String = myIterator.next().toString()
//                val value: String? = hashMap.get(key)
//                if (counter < hashMap.size) urlParameters = urlParameters + key + "=" + value + "&"
//                if (counter == hashMap.size) urlParameters = urlParameters + key + "=" + value
//                counter++
//            }
//
//
//            return urlParameters.toString()
//        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            // ...
        }
    }

    fun dailyNotification() {
        val alarmIntent = Intent(this, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val manager =
            getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR_OF_DAY] = 14
        calendar[Calendar.MINUTE] = 1
        calendar[Calendar.SECOND]=10
        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent)
    }
//    private fun  exitDialog() {
//        AlertDialog.Builder(this)
//            .setTitle("Confirm Exit?")
//            .setMessage("Really you want to exit the application?")
//            .setPositiveButton("Yes",
//                DialogInterface.OnClickListener { dialog, which -> finish() })
//            .setNegativeButton("No", null)
//            .show()
//    }


    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBackPressed() {
       super.onBackPressed()
        //exitDialog()
       // finish()
    }
}






