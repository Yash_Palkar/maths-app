package com.client.levelsapp

interface SimpleBodmass {
    fun apply(x1: Int, x2: Int, x3: Int, x4: Int, x5: Int): Float
}