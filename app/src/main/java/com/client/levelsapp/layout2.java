//package com.digitalgorkha.guard.Activity;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.content.ContentValues;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.SystemClock;
//import android.provider.MediaStore;
//import android.provider.Settings.Secure;
//import android.support.graphics.drawable.VectorDrawableCompat;
//import android.text.Editable;
//import android.text.InputType;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.digitalgorkha.guard.Compress_Image.FileUtil;
//import com.digitalgorkha.guard.Compress_Image.Image_Compressor;
//import com.digitalgorkha.guard.Compress_Image.Image_upload;
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;
//import com.google.gson.Gson;
//import com.google.gson.JsonArray;
//import com.digitalgorkha.guard.Database.UserSessionManager;
//import com.digitalgorkha.guard.Util.GlideCircleTransformation;
//import com.digitalgorkha.guard.Web_Service.APIClient;
//
//import com.digitalgorkha.guard.Web_Service.APIInterface;
//import com.digitalgorkha.guard.Database.ConstantClass;
//import com.digitalgorkha.guard.Database.DatabaseHelper;
//import com.digitalgorkha.guard.R;
//import com.digitalgorkha.guard.Util.progressDialog;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.sql.Timestamp;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class CheckInActivity extends Activity implements OnClickListener {
//
//    protected int CAMERA_REQUEST = 0;
//    RelativeLayout rlBack;
//    String vImg,multiple_data ;
//    EditText edtName, edtMobile, edtVNo, edtrcvOTP , edtOptionalFlat;
//    EditText edtVNo1, edtVNo2, edtVNo3, edtVNo4;
//    Button btnSubmit, btnResident, btnSKIP,btnresentotp;
//    TextView tvTime;
//    protected static final int GALLERY_PICTURE = 1;
//    private Uri imageUri;
//    private static final String PREF_CITY = "city";
//    protected boolean isUploaded = false;
//    protected boolean isChosen = false;
//    Context context = this;
//    int serverResponseCode = 0;
//    String name, mobile,deltype, vtype, vno, resident, vfrom, device_id, vmsg, user_id, fileName, imagename, gcm_id;
//    private ImageView imgEdit;
//    private TextView txtTittle;
//    private Spinner spinner, spnrDelType;
//    private Bitmap bm;
//    private Uri datauri;
//    private String uploadFilePath;
//    private ImageView imgProfile;
//    private ConstantClass constant;
//    Cursor cursor;
//    Toast toast;
//    private boolean denySociety;
//    private String denyvisitor_name;
//    private String denyvisitor_no;
//    private String denyclient_id;
//    private String fname;
//    private String lname;
//    private String contact_no;
//    private String flateVisiblity;
//    String society_name , axix_id;
//    String wingno, flatno;
//    UserSessionManager userSessionManager;
//    boolean otpvariable = false;
//    boolean isOtpVerified = true;
//    LinearLayout lnrOTP;
//    // timer
//    long startTime =SystemClock.uptimeMillis();
//    long timeInMilliseconds = 500 * 240;
//    long timeSwapBuff = 500 * 240;
//    long updatedTime = 500 * 240;
//    int resendLimit = 0;
//    int secs;
//    int mins ;
//    TextView txtOTPTimer;
//    private Handler customHandler = new Handler();
//    String other_flat , other_type;
//    EditText edtOther;
//    APIInterface apiInterface;
//    LinearLayout liner_spnrDelType;
//    String jsonvalue;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_checkin_otp);
//        constant = new ConstantClass(context);
//        userSessionManager = new UserSessionManager(context);
//        Cursor c1 = constant.getSocietyName();
//        society_name = c1.getString(c1.getColumnIndex(DatabaseHelper.society_name));
//
//        init();
//        setListeners();
//        startDialog();
//        spinneradapt(0);
//        textwatcher();
//    }
//
//    private void init()
//    {
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        spnrDelType = (Spinner) findViewById(R.id.spnrDelType);
////        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.delivery_type, android.R.layout.simple_spinner_item);
////        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
////        spnrDelType.setAdapter(adapter);
//
//        liner_spnrDelType= (LinearLayout) findViewById(R.id.liner_spnrDelType);
//        rlBack = (RelativeLayout) findViewById(R.id.rlBack);
//        lnrOTP = (LinearLayout) findViewById(R.id.lnrOTP);
//
//        edtVNo1 = (EditText) findViewById(R.id.edtVNo1);
//        edtVNo2 = (EditText) findViewById(R.id.edtVNo2);
//        edtVNo3 = (EditText) findViewById(R.id.edtVNo3);
//        edtVNo4 = (EditText) findViewById(R.id.edtVNo4);
//
//        edtVNo1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
//        edtVNo3.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
//
//        edtName = (EditText) findViewById(R.id.edtName);
//        spinner = (Spinner) findViewById(R.id.spinner1);
//        edtOther = (EditText) findViewById(R.id.edtOther);
//        edtMobile = (EditText) findViewById(R.id.edtMobile);
//        edtVNo = (EditText) findViewById(R.id.edtVNo);
//        edtrcvOTP = (EditText) findViewById(R.id.edtrcvOTP);
//        edtOptionalFlat = (EditText) findViewById(R.id.edtOptionalFlat);
//        edtOptionalFlat.setVisibility(View.GONE);
//        btnResident = (Button) findViewById(R.id.btnResident);
//        tvTime = (TextView) findViewById(R.id.tvTime);
//        imgEdit = (ImageView) findViewById(R.id.imgEdit);
//        imgProfile = (ImageView) findViewById(R.id.imgProfile);
//        txtTittle = (TextView) findViewById(R.id.txtTittle);
//        txtOTPTimer = (TextView) findViewById(R.id.txtOTPTimer);
//        txtTittle.setText(getResources().getString(R.string.CHECK_IN));
//        btnSubmit = (Button) findViewById(R.id.btnSubmit);
//        btnSKIP = (Button) findViewById(R.id.btnSKIP);
//        btnresentotp = (Button) findViewById(R.id.resentotp);
//        btnSubmit.setClickable(false);
//        axix_id = getIntent().getStringExtra("xs_id");
////        if(userSessionManager.getLang().equals("2")) {
//        if(userSessionManager.getLang()==2) {
//            edtMobile.setText("");
//            lnrOTP.setVisibility(View.GONE);
//            txtOTPTimer.setVisibility(View.GONE);
//            otpvariable = true;
//            isOtpVerified = false;
//            edtMobile.setEnabled(true);
//        } else {
//            edtMobile.setText(getIntent().getStringExtra("mobile"));
//            edtMobile.setEnabled(false);
//        }
//
//        CheckDenyBySociety(getIntent().getStringExtra("mobile"));
//
//        if (denySociety) {
//            btnResident.setClickable(false);
//            btnResident.setEnabled(false);
//            btnResident.setText(getResources().getString(R.string.DENIED_BY_SOCIETY));
//        }
//        Intent i = getIntent();
//        if(i.getExtras() != null)
//        {
//            flateVisiblity = i.getStringExtra("flateVisiblity");
//            if (!flateVisiblity.equals("true"))
//            {
//                user_id = "";
//                fname = "";
//                lname = "";
//                contact_no = "";
//                btnResident.setEnabled(true);
//                btnResident.setClickable(true);
//            }
//        }
//        customHandler.post(updateTimerThread);
//        resendLimit++;
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                String dda = spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();;
//
//                if(dda.equals("Other"))
//                    edtOther.setVisibility(View.VISIBLE);
//                else
//                    edtOther.setVisibility(View.GONE);
//
//                if (dda.equalsIgnoreCase("Delivery"))
//                {
//                    spinneradapt(1);
//
//                }
//                else
//                {
//                    liner_spnrDelType.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//    }
//    public void textwatcher()
//    {
//        edtVNo1.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            public void onTextChanged(CharSequence sb, int start, int before, int count)
//            {
//                if(sb.toString().trim().length()==2)
//                {
//                    edtVNo1.clearFocus();
//                    edtVNo2.requestFocus();
//                    edtVNo2.setCursorVisible(true);
//                }
//            }
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });
//
//        edtVNo2.setOnKeyListener(new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event)
//            {
//                if (keyCode == KeyEvent.KEYCODE_DEL)
//                {
//                    if(edtVNo2.getText().toString().trim().length()==0)
//                    {
//                        edtVNo2.clearFocus();
//                        edtVNo1.requestFocus();
//                        edtVNo1.setCursorVisible(true);
//                    }
//                }
//                return false;
//            }
//        });
//
//        edtVNo2.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            public void onTextChanged(CharSequence sb, int start, int before, int count)
//            {
//                if(sb.toString().trim().length()==2)
//                {
//                    edtVNo2.clearFocus();
//                    edtVNo3.requestFocus();
//                    edtVNo3.setCursorVisible(true);
//                }
//            }
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });
//
//        edtVNo3.setOnKeyListener(new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event)
//            {
//                if (keyCode == KeyEvent.KEYCODE_DEL)
//                {
//                    if(edtVNo3.getText().toString().trim().length()==0)
//                    {
//                        edtVNo3.clearFocus();
//                        edtVNo2.requestFocus();
//                        edtVNo2.setCursorVisible(true);
//                    }
//                }
//                return false;
//            }
//        });
//
//        edtVNo3.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            public void onTextChanged(CharSequence sb, int start, int before, int count)
//            {
//                if(sb.toString().trim().length()==2)
//                {
//                    edtVNo3.clearFocus();
//                    edtVNo4.requestFocus();
//                    edtVNo4.setCursorVisible(true);
//                }
//            }
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });
//
//        edtVNo4.setOnKeyListener(new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event)
//            {
//                if (keyCode == KeyEvent.KEYCODE_DEL)
//                {
//                    if(edtVNo4.getText().toString().trim().length()==0)
//                    {
//                        edtVNo4.clearFocus();
//                        edtVNo3.requestFocus();
//                        edtVNo3.setCursorVisible(true);
//                    }
//                }
//                return false;
//            }
//        });
//
//
////        edtVNo4.addTextChangedListener(new TextWatcher() {
////            @Override
////            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////            }
////
////            public void onTextChanged(CharSequence sb, int start, int before, int count)
////            {
////            }
////            @Override
////            public void afterTextChanged(Editable s) {
////            }
////        });
//    }
//    public void spinneradapt(int check)
//    {
//        if(check==0)
//        {
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1) {
//                public View getView(int position, View convertView, android.view.ViewGroup parent) {
//                    TextView v = (TextView) super.getView(position, convertView, parent);
//                    v.setTextColor(Color.BLACK);
//                    v.setTextSize(20);
//                    return v;
//                }
//
//                public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
//                    TextView v = (TextView) super.getView(position, convertView, parent);
//                    v.setTextColor(Color.BLACK);
//                    v.setTextSize(20);
//                    return v;
//                }
//
//                @Override
//                public int getCount() {
//                    return super.getCount();
//                }
//            };
//            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
//            adapter.add(getResources().getString(R.string.SELECT_VISITOR_TYPE));
//            adapter.add(getResources().getString(R.string.RELATIVE));
//            adapter.add(getResources().getString(R.string.BUSINESS));
//            adapter.add(getResources().getString(R.string.FRIENDS));
//            adapter.add(getResources().getString(R.string.SERVICE));
//            adapter.add(getResources().getString(R.string.DELIVERY));
//            adapter.add(getResources().getString(R.string.OTHER));
//            spinner.setAdapter(adapter);
//        }
//        else
//        {
//            String list[];
//            list=getResources().getStringArray(R.array.delivery_type);
//            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(CheckInActivity.this,android.R.layout.simple_list_item_1,list)
//            {
//                public View getView(int position, View convertView, android.view.ViewGroup parent) {
//                    TextView v = (TextView) super.getView(position, convertView, parent);
//                    v.setTextColor(Color.BLACK);
//                    v.setTextSize(20);
//                    return v;
//                }
//                public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
//                    TextView v = (TextView) super.getView(position, convertView, parent);
//                    v.setTextColor(Color.BLACK);
//                    v.setTextSize(20);
//                    return v;
//                }
//                @Override
//                public int getCount() {
//                    return super.getCount();
//                }
//            };
//            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            spnrDelType.setAdapter(adapter1);
//            liner_spnrDelType.setVisibility(View.VISIBLE);
//        }
//
//    }
//
//    //timer Thread_________________________________________________________!!!!!
//
//    Runnable updateTimerThread = new Runnable()
//    {
//        @Override
//        public void run()
//        {
//            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
//            updatedTime = timeSwapBuff - timeInMilliseconds;
//            secs = (int) (updatedTime / 1000);
//            mins = (secs / 60);
//            secs = (secs % 60) / 2;
//            txtOTPTimer.setText("" + mins + ":" + String.format("%02d", secs));
//
//            if (mins == 0 && secs == 0)
//            {
//                lnrOTP.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                edtrcvOTP.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                btnSKIP.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                btnresentotp.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                edtrcvOTP.setEnabled(false);
//                btnSKIP.setEnabled(false);
//                btnresentotp.setEnabled(false);
//                txtOTPTimer.setVisibility(View.INVISIBLE);
//                otpvariable = true;
//                isOtpVerified = false;
//                customHandler.removeCallbacks(this);
//                return;
//            }
//            customHandler.post(this);
//        }
//    };
//
//
//    private void CheckDenyBySociety(String mobile2) {
//        // TODO Auto-generated method stub
//        denySociety = false;
//        try {
//            cursor = constant.getDenySociety(mobile2);
//
//            denyvisitor_name = "";
//            denyvisitor_no = "";
//            denyclient_id = "";
//
//            if (cursor.getCount() > 0) {
//                for (int i = 0; i < cursor.getCount(); i++) {
//                    denyvisitor_name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.visitor_name));
//                    denyvisitor_no = cursor.getString(cursor.getColumnIndex(DatabaseHelper.visitor_no));
//                    denyclient_id = cursor.getString(cursor.getColumnIndex(DatabaseHelper.client_id));
//
//                    cursor.moveToNext();
//                }
//                denySociety = true;
//            } else {
//                denySociety = false;
//            }
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }
//        //return denyUser;
//    }
//
//
//    private void setListeners()
//    {
//        rlBack.setOnClickListener(this);
//        btnSubmit.setOnClickListener(this);
//        btnResident.setOnClickListener(this);
//        imgEdit.setOnClickListener(this);
//        imgProfile.setOnClickListener(this);
//        btnSKIP.setOnClickListener(this);
//        btnresentotp.setOnClickListener(this);
//
//        final VectorDrawableCompat vec_right= VectorDrawableCompat.create(getResources(),R.drawable.ic_check_black_24dp,context.getTheme());
//        final VectorDrawableCompat vec_wrong=VectorDrawableCompat.create(getResources(),R.drawable.ic_cancel_black_24dp,context.getTheme());
//        edtrcvOTP.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//            }
//            @Override
//            public void onTextChanged(CharSequence cs, int i, int i1, int i2)
//            {
//            }
//            @SuppressLint("NewApi")
//            @Override
//            public void afterTextChanged(Editable editable)
//            {
//                String txtotp=editable.toString().trim();
//                if(txtotp.length()==4)
//                {
//                    if(txtotp.equals(userSessionManager.getOTP()))
//                    {
//                        edtrcvOTP.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, vec_right, null);
//                        otpvariable = true;
//                        isOtpVerified = true;
//                        lnrOTP.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                        edtrcvOTP.setEnabled(false);
//                        btnSKIP.setEnabled(false);
//                        btnresentotp.setEnabled(false);
//                        txtOTPTimer.setVisibility(View.INVISIBLE);
//                        customHandler.removeCallbacks(updateTimerThread);
//                    }
//                    else
//                    {
//                        edtrcvOTP.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, vec_wrong, null);
//                    }
//
//                }
//            }
//        });
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.rlBack:
//                customHandler.removeCallbacks(updateTimerThread);
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(edtName.getWindowToken(), 0);
//                finish();
//                break;
//
//            case R.id.btnSubmit:
//
//                boolean isvalid=true;
//                name = edtName.getText().toString().trim();
//                mobile = edtMobile.getText().toString().trim();
//                vno = edtVNo1.getText().toString().trim().concat(edtVNo2.getText().toString().trim().concat(edtVNo3.getText().toString().trim().concat(edtVNo4.getText().toString().trim())));
//                vtype = spinner.getSelectedItem().toString().trim();
//                other_flat = edtOptionalFlat.getText().toString().trim();
//                other_type = edtOther.getText().toString().trim();
//                if(liner_spnrDelType.getVisibility() == View.VISIBLE)
//                {
//                    deltype = spnrDelType.getSelectedItem().toString();
//                }
//
//                if (!isChosen)
//                {
//                    toast = Toast.makeText(CheckInActivity.this, getResources().getString(R.string.PLEASE_CAPTURE_IMAGE), Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                    isvalid=false;
//                }
//                if(!otpvariable)
//                {
//                    edtrcvOTP.setError(getResources().getString(R.string.VERIFY_OTP_OR_SKIP));
//                    edtrcvOTP.requestFocus();
//                    isvalid=false;
////                    toast = Toast.makeText(CheckInActivity.this, getResources().getString(R.string.VERIFY_OTP_OR_SKIP), Toast.LENGTH_LONG);
////                    toast.setGravity(Gravity.CENTER, 0, 0);
////                    toast.show();
//                }
//                if(name==null || name.isEmpty())
//                {
//                    edtName.setError(getResources().getString(R.string.VISI_NAME));
//                    edtName.requestFocus();
//                    isvalid=false;
////                    toast = Toast.makeText(CheckInActivity.this, getResources().getString(R.string.VISI_NAME), Toast.LENGTH_LONG);
////                    toast.setGravity(Gravity.CENTER, 0, 0);
////                    toast.show();
//                }
//                if(mobile==null || mobile.isEmpty())
//                {
//                    toast = Toast.makeText(CheckInActivity.this, getResources().getString(R.string.MOBILE_NUMBER), Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                    isvalid=false;
//                }
//                if(vtype.contains("Select"))
//                {
//                    ((TextView)spinner.getChildAt(0)).setError(getResources().getString(R.string.SELECT_VISITOR_TYPE));
//                    toast = Toast.makeText(CheckInActivity.this, getResources().getString(R.string.SELECT_VISITOR_TYPE), Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                    isvalid=false;
//                }
//                if(vtype.equalsIgnoreCase("Other") && (other_type==null || other_type.isEmpty()))
//                {
//                    edtOther.setError(getResources().getString(R.string.SPINNER_OPTIONAL));
//                    edtOther.requestFocus();
//                    isvalid=false;
////                    toast = Toast.makeText(CheckInActivity.this, getResources().getString(R.string.SPINNER_OPTIONAL), Toast.LENGTH_LONG);
////                    toast.setGravity(Gravity.CENTER, 0, 0);
////                    toast.show();
//                }
//                if(liner_spnrDelType.getVisibility() == View.VISIBLE && deltype.contains("Select"))
//                {
//                    ((TextView)spnrDelType.getChildAt(0)).setError(getResources().getString(R.string.SELECTDELIVERYTYPE));
//                    toast = Toast.makeText(CheckInActivity.this, getResources().getString(R.string.SELECTDELIVERYTYPE), Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                    isvalid=false;
//                }
//
//                if (btnResident.getText().toString().trim()==null || btnResident.getText().toString().trim().isEmpty())
//                {
//                    btnResident.setError(getResources().getString(R.string.SELECT_RESIDENCE_DETAILS));
//                    btnResident.requestFocus();
//                    isvalid=false;
//
//                }
//                if(isvalid)
//                {
//                    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//                    boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
//                    if (isConnected)
//                    {
//                        checkin();
//                    } else {
//                        toast = Toast.makeText(context, getResources().getString(R.string.check_internet), Toast.LENGTH_LONG);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();
//                    }
//                }
//
//                break;
//            case R.id.btnResident:
//                String spinrvalue=spinner.getSelectedItem().toString();
//                jsonvalue="";
//                String checktag;
//                if(spinrvalue.equalsIgnoreCase("Delivery") || spinrvalue.equalsIgnoreCase("Service"))
//                {
//                    checktag="1";
//                }
//                else
//                {
//                    checktag="0";
//                }
//                Intent i = new Intent(CheckInActivity.this, ListActivity.class);
//                i.putExtra("mobile", edtMobile.getText().toString());
//                i.putExtra("checktag", checktag);
//                startActivityForResult(i, 10);
//
//                break;
//
//            case R.id.imgEdit:
////                Intent i1 = new Intent(CheckInActivity.this, SearchActivity.class);
//                String spinrvalueimg=spinner.getSelectedItem().toString();
//
//                if(spinrvalueimg.equalsIgnoreCase("Delivery") || spinrvalueimg.equalsIgnoreCase("Service"))
//                {
//                    checktag="1";
//                }
//                else
//                {
//                    checktag="0";
//                }
//                Intent i1 = new Intent(CheckInActivity.this, ListActivity.class);
//                i1.putExtra("mobile", edtMobile.getText().toString());
//                i1.putExtra("checktag", checktag);
//                startActivityForResult(i1, 10);
//                break;
//
//            case R.id.imgProfile:
//                startDialog();
//                break;
//
//            case R.id.btnSKIP:
//
//                otpvariable = true;
//                isOtpVerified = false;
//                lnrOTP.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                edtrcvOTP.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                btnSKIP.setBackgroundColor(Color.parseColor("#DCDCDC"));
//                btnresentotp.setBackgroundColor(Color.parseColor("#DCDCDC"));
//
//                edtrcvOTP.setEnabled(false);
//                btnSKIP.setEnabled(false);
//                btnresentotp.setEnabled(false);
//                txtOTPTimer.setVisibility(View.INVISIBLE);
//                customHandler.removeCallbacks(updateTimerThread);
//
//                break;
//            case R.id.resentotp:
//                customHandler.removeCallbacks(updateTimerThread);
//                mobile_verify();
//                break;
//            default:
//                break;
//        }
//    }
//
//    public void mobile_verify()
//    {
//        new progressDialog(CheckInActivity.this,true);
//        apiInterface = APIClient.getClient().create(APIInterface.class);
//        SharedPreferences sp = getSharedPreferences("logindetail", Activity.MODE_PRIVATE);
//        String username = sp.getString("username", "");
//        String password = sp.getString("password", "");
//        String guard_id=sp.getString("gaurd_id", "");
//        String client_id=sp.getString("client_id", "");
//        String deviceId = Secure.getString(CheckInActivity.this.getContentResolver(), Secure.ANDROID_ID);
//        String method="authenticate";
//
//        Call<JsonArray> call = apiInterface.mob_verify(deviceId,username,password,method,
//                edtMobile.getText().toString(),society_name,client_id,Integer.toString(userSessionManager.getLang()));
//
//        call.enqueue(new Callback<JsonArray>()
//        {
//            @Override
//            public void onResponse(Call<JsonArray> call, Response<JsonArray> response)
//            {
//                int code;
//                String msg=getResources().getString(R.string.error_msg);
//                if(response.body().toString()==null || response.body().toString().isEmpty())
//                {
//                    Toast toast = Toast.makeText(CheckInActivity.this,msg, Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                }
//                else
//                {
//                    try {
//                        Gson gson=new Gson();
//                        String result=gson.toJson(response.body());
//                        JSONArray jsonArray=new JSONArray(result);
//                        JSONObject joResult=jsonArray.getJSONObject(0);
//                        code = Integer.parseInt(joResult.getString("error_code"));
//                        if (code==0)
//                        {
//                            String vericode = joResult.getString("verificationcode");
//                            userSessionManager.saveOTP(vericode, edtMobile.getText().toString());
//                            startTime = SystemClock.uptimeMillis();
//                            timeInMilliseconds = 1000 * 240;
//                            timeSwapBuff = 1000 * 240;
//                            updatedTime = 1000 * 240;
//                            resendLimit = 0;
//                            secs=0;
//                            mins=0;
//                            customHandler.post(updateTimerThread);
//                            toast = Toast.makeText(CheckInActivity.this,
//                                    getResources().getString(R.string.SENT_SUCCESS),
//                                    Toast.LENGTH_LONG);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
//                        }
//                        else
//                        {
//                            toast = Toast.makeText(CheckInActivity.this,
//                                    getResources().getString(R.string.CHECK_IN_ERROR),
//                                    Toast.LENGTH_LONG);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                new progressDialog(CheckInActivity.this,false);
//            }
//
//            @Override
//            public void onFailure(Call<JsonArray> call, Throwable t)
//            {
//                call.cancel();
//                new progressDialog(CheckInActivity.this,false);
//                Toast toast = Toast.makeText(CheckInActivity.this,getResources().getString(R.string.connection_fail) , Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        });
//    }
//
//
//    private void startDialog()
//    {
//        serverResponseCode = 0;
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE, "New Picture");
//        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
//        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//        String imgU = imageUri.toString();
//        vImg = imgU.replaceAll("\\D+", "");
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//        startActivityForResult(intent, CAMERA_REQUEST);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//        if (requestCode == 10 && resultCode == RESULT_OK)
//        {
//            try
//            {
//                btnResident.setError(null);
//                jsonvalue=data.getStringExtra("jsonvalue");
//                JSONArray jsonArray = new JSONArray(jsonvalue);
//                Log.d("shiv","ARRAY = "+ jsonArray.toString());
//                multiple_data = jsonvalue;
//                if(jsonArray.length()>0)
//                {
//                    btnResident.setText(jsonArray.length() +" Member is Selected");
//                }
//                else
//                {
//                    btnResident.setText(null);
//                    btnResident.setHint(getResources().getString(R.string.SEARCH_RESIDENCE_DETAILS));
//                }
//            }
//            catch (JSONException e)
//            {
//                e.printStackTrace();
//            }
//        }
//        else if(requestCode == 10 && resultCode == RESULT_CANCELED)
//        {
//            if(jsonvalue.isEmpty())
//            {
//                btnResident.setText(null);
//                btnResident.setHint(getResources().getString(R.string.SEARCH_RESIDENCE_DETAILS));
//            }
//        }
//        else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_CANCELED)
//        {
//            isChosen = false;
//            toast = Toast.makeText(getApplicationContext(), "Cancelled",Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
//        }
//        else  if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK)
//        {
//            File actualImage=null;
//            File compressedImage=null;
//            isChosen = true;
//            Image_Compressor compressor=new Image_Compressor();
//            try {
//                datauri = imageUri;
//                actualImage = FileUtil.from(this, datauri);// it is caching immage
//                imagename = actualImage.getName();
//                Glide.with(context)
//                        .load(datauri)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true)
//                        .placeholder(R.drawable.ic_person_black_24dp)
//                        .error(R.drawable.user_search)
//                        .centerCrop()
//                        .bitmapTransform(new GlideCircleTransformation(context))
//                        .into(imgProfile);
//                compressedImage=compressor.customCompressImage(actualImage,this);
//                actualImage.delete();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
//            if (isConnected)
//            {
//                new UploadfileAsync().execute(compressedImage);
//            }
//            else
//            {
//                toast = Toast.makeText(context, getResources().getString(R.string.check_internet),Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//        else
//        {
//            toast = Toast.makeText(getApplicationContext(), "Nothing Selected",Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
//        }
//
//    }
//    private class UploadfileAsync extends AsyncTask<File, String, File>
//    {
//        File result;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected File doInBackground(File... param)
//        {
//            try {
//                Image_upload.uploadFile(0,CheckInActivity.this,param[0],imagename,vImg,"","");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(File s)
//        {
//            super.onPostExecute(s);
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
//
//    public void checkin()
//    {
//        new progressDialog(CheckInActivity.this,true);
//
//        LayoutInflater inflater = getLayoutInflater();
//        View layout = inflater.inflate(R.layout.cust_toast,(ViewGroup) findViewById(R.id.toast_layout_root));
//        final ImageView imagetoast =  layout.findViewById(R.id.img);
//        final TextView text =  layout.findViewById(R.id.txtmsg);
//        final Toast toast = new Toast(getApplicationContext());
//        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(layout);
//
//        apiInterface = APIClient.getClient().create(APIInterface.class);
//        SharedPreferences sp = getSharedPreferences("logindetail", Activity.MODE_PRIVATE);
//        String username = sp.getString("username", "");
//        String password = sp.getString("password", "");
//        String gaurd_id = sp.getString("gaurd_id", "");
//        String client_id = sp.getString("client_id", "");
//        String guard_mob = sp.getString("mobileno", "");
//        String method="checkin";
//        String deviceId = Secure.getString(CheckInActivity.this.getContentResolver(),Secure.ANDROID_ID);
//        java.util.Date date = new java.util.Date();
//        String verify = isOtpVerified? "1" : "2";
//        Log.d("shiv","vvvvvv"+ isOtpVerified);
//        Log.d("shiv","verify"+ verify);
//        Log.d("http","verify"+ verify);
//
//        Call<JsonArray> call = apiInterface.checkin(username,password,method,deviceId,
//                new Timestamp(date.getTime()).toString(),name,vfrom,mobile,vtype,deltype,vmsg,user_id,client_id,
//                imagename,fname,lname,guard_mob,gaurd_id,vno,gcm_id,society_name,wingno,flatno,wingno,other_flat,
//                other_type,Integer.toString(userSessionManager.getLang()),axix_id,multiple_data,verify);
//        call.enqueue(new Callback<JsonArray>()
//        {
//            @Override
//            public void onResponse(Call<JsonArray> call, Response<JsonArray> response)
//            {
//                int code;
//                String msg=getResources().getString(R.string.error_msg);
//                if(response.body().toString()==null || response.body().toString().isEmpty())
//                {
//                    Toast toast = Toast.makeText(CheckInActivity.this,msg, Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                }
//                else
//                {
//                    try {
//                        Gson gson= new Gson();
//                        String result=gson.toJson(response.body());
//                        JSONArray jsonArray = new JSONArray(result);
//                        JSONObject jsonObject = jsonArray.getJSONObject(0);
//                        code = Integer.parseInt(jsonObject.getString("error_code"));
//                        // msg = jsonObject.getString("message");
//                        if (code == 0)
//                        {
//                            userSessionManager.getOTPFlush();
//
//                            imagetoast.setImageResource(R.drawable.ic_check_green_24dp);
//                            text.setText( getResources().getString(R.string.user_checkin));
//                            toast.show();
//
//                            Intent intent = new Intent(CheckInActivity.this, MainActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(intent);
//                            finish();
//                        }
//                        else {
//                            imagetoast.setImageResource(R.drawable.ic_wrong_red_24dp);
//                            text.setText( getResources().getString(R.string.user_not_checkin));
//                            toast.show();
//                        }
//
//                    } catch (JSONException e) {
//                        imagetoast.setImageResource(R.drawable.ic_wrong_red_24dp);
//                        text.setText( getResources().getString(R.string.connection_fail));
//                        toast.show();
//                        e.printStackTrace();
//                    }
//                }
//                new progressDialog(CheckInActivity.this,false);
//            }
//
//            @Override
//            public void onFailure(Call<JsonArray> call, Throwable t)
//            {
//
//                call.cancel();
//                new progressDialog(CheckInActivity.this,false);
//                imagetoast.setImageResource(R.drawable.ic_wrong_red_24dp);
//                text.setText( getResources().getString(R.string.connection_fail));
//                toast.show();
//
//            }
//        });
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        customHandler.removeCallbacks(updateTimerThread);
//    }
//}
//
