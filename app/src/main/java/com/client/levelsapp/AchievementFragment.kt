package com.client.levelsapp

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class AchievementFragment : Fragment() {
    var totalscore = 0
    var prefconfig: SharedPrefrenceClass? = null
    var mActivity: Activity? = null
    private lateinit var layoutManager: RecyclerView.LayoutManager
    var recycler_menu: RecyclerView? = null
    var upLabelArr = arrayOf(
        "Addition / 1 Star",
        "Two Levels / 3 Stars",
        "Open Level 3",
        "Open Level 5",
        "5 Stars Challenge",
        "Level 1 to 4 / 8 Stars",
        "In All easy / 12 Stars",
        "Open Level 7",
        "15 Stars Challenge",
        "25 Stars Challenge",
        "Level 1 / Score 60",
        "Open 2nd Tricky level ",
        "2nd tricky / Score 70",
        "In Two Tricky/Score 150",
        "In Three Tricky/Score 200",
        "Open 4th Tricky level ",
        "In All tricky / Score 300 ",
        "In All tricky / Score 400 "
    )

    companion object {
        var homefragment: AchievementFragment? = null
        fun getInstance(): AchievementFragment {
            homefragment = AchievementFragment()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefconfig = SharedPrefrenceClass(context!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.activity_achievement, container, false)
        recycler_menu = view.findViewById(R.id.recycler_view) as RecyclerView
        for (i in 0..17) {
            totalscore += prefconfig!!.readLevelscoreeasy(i)
        }
        for (i in 0..17) {
            when (upLabelArr[i]) {
                "Addition / 1 Star" -> {
                    if (prefconfig!!.readLevelscoreeasy(0) >= 300) {
                        prefconfig!!.writeachievement(0, 1)
                    } else {
                        prefconfig!!.writeachievement(0, 0)
                    }
                }
                "Two Levels / 3 Stars" -> {
                    if (prefconfig!!.readLevelscoreeasy(0) >= 900 && prefconfig!!.readLevelscoreeasy(1) >= 900) {
                        prefconfig!!.writeachievement(1, 1)
                    } else {
                        prefconfig!!.writeachievement(1, 0)
                    }
                }
                "Open Level 3" -> {
                    if ((prefconfig!!.readLevelscoreeasy(0) + prefconfig!!.readLevelscoreeasy(1)) >= 900) {
                        prefconfig!!.writeachievement(2, 1)
                    } else {
                        prefconfig!!.writeachievement(2, 0)
                    }
                }
                "Open Level 5" -> {
                    if ((prefconfig!!.readLevelscoreeasy(0) + prefconfig!!.readLevelscoreeasy(1) + prefconfig!!.readLevelscoreeasy(
                            2
                        ) + prefconfig!!.readLevelscoreeasy(3)) >= 2100
                    ) {
                        prefconfig!!.writeachievement(3, 1)
                    } else {
                        prefconfig!!.writeachievement(3, 0)
                    }
                }
                "5 Stars Challenge" -> {
                    if (totalscore >= 1500) {
                        prefconfig!!.writeachievement(4, 1)
                    } else {
                        prefconfig!!.writeachievement(4, 0)
                    }
                }
                "Level 1 to 4 / 8 Stars" -> {
                    if ((prefconfig!!.readLevelscoreeasy(0) + prefconfig!!.readLevelscoreeasy(1) + prefconfig!!.readLevelscoreeasy(
                            2
                        ) + prefconfig!!.readLevelscoreeasy(3)) >= 2400
                    ) {
                        prefconfig!!.writeachievement(5, 1)
                    } else {
                        prefconfig!!.writeachievement(5, 0)
                    }
                }
                "In All easy / 12 Stars" -> {
                    if (totalscore >= 2400) {
                        prefconfig!!.writeachievement(6, 1)
                    } else {
                        prefconfig!!.writeachievement(6, 0)
                    }
                }
                "Open Level 7" -> {
                    if ((prefconfig!!.readLevelscoreeasy(0) + prefconfig!!.readLevelscoreeasy(1) + prefconfig!!.readLevelscoreeasy(
                            2
                        ) + prefconfig!!.readLevelscoreeasy(3)
                                + prefconfig!!.readLevelscoreeasy(4) + prefconfig!!.readLevelscoreeasy(5)) >= 3300
                    ) {
                        prefconfig!!.writeachievement(7, 1)
                    } else {
                        prefconfig!!.writeachievement(7, 0)
                    }
                }
                "15 Stars Challenge" -> {
                    if (totalscore >= 4500) {
                        prefconfig!!.writeachievement(8, 1)
                    } else {
                        prefconfig!!.writeachievement(8, 0)
                    }
                }
                "25 Stars Challenge" -> {
                    if (totalscore >= 7500) {
                        prefconfig!!.writeachievement(9, 1)
                    } else {
                        prefconfig!!.writeachievement(9, 0)
                    }
                }
                "Level 1 / Score 60" -> {
                    if (prefconfig!!.readLevelscoretricky(0) >= 60) {
                        prefconfig!!.writeachievement(10, 1)
                    } else {
                        prefconfig!!.writeachievement(10, 0)
                    }
                }
                "Open 2nd Tricky level " -> {
                    if (prefconfig!!.readLevelscoreeasy(0) >= 300) {

                        prefconfig!!.writeachievement(11, 1)
                    } else {
                        prefconfig!!.writeachievement(11, 0)
                    }
                }
                "2nd tricky / Score 70" -> {
                    if (prefconfig!!.readLevelscoretricky(1) >= 70) {
                        prefconfig!!.writeachievement(12, 1)
                    } else {
                        prefconfig!!.writeachievement(12, 0)
                    }
                }
                "In Two Tricky/Score 150" -> {
                    var totaltwotricky_score =
                        prefconfig!!.readLevelscoretricky(0) + prefconfig!!.readLevelscoretricky(1)
                    if (totaltwotricky_score >= 150) {
                        prefconfig!!.writeachievement(13, 1)
                    } else {
                        prefconfig!!.writeachievement(13, 0)
                    }
                }
                "In Three Tricky/Score 200" -> {
                    var totalthreetricky_score =
                        prefconfig!!.readLevelscoretricky(0) + prefconfig!!.readLevelscoretricky(1) + prefconfig!!.readLevelscoretricky(
                            2
                        )
                    if (totalthreetricky_score >= 200) {
                        prefconfig!!.writeachievement(14, 1)
                    } else {
                        prefconfig!!.writeachievement(14, 0)
                    }
                }
                "Open 4th Tricky level " -> {
                    if ((prefconfig!!.readLevelscoreeasy(0)+prefconfig!!.readLevelscoreeasy(1)+prefconfig!!.readLevelscoreeasy(2)) >= 1500) {

                        prefconfig!!.writeachievement(15, 1)
                    } else {
                        prefconfig!!.writeachievement(15, 0)
                    }
                }
                "In All tricky / Score 300 " -> {
                    var totalalltricky_score =
                        prefconfig!!.readLevelscoretricky(0) + prefconfig!!.readLevelscoretricky(1) + prefconfig!!.readLevelscoretricky(
                            2
                        ) + prefconfig!!.readLevelscoretricky(3)
                    if (totalalltricky_score >= 300) {
                        prefconfig!!.writeachievement(16, 1)
                    } else {
                        prefconfig!!.writeachievement(16, 0)
                    }
                }
                "In All tricky / Score 400 " -> {
                    var totalalltricky_score =
                        prefconfig!!.readLevelscoretricky(0) + prefconfig!!.readLevelscoretricky(1) + prefconfig!!.readLevelscoretricky(
                            2
                        ) + prefconfig!!.readLevelscoretricky(3)
                    if (totalalltricky_score >= 400) {
                        prefconfig!!.writeachievement(17, 1)
                    } else {
                        prefconfig!!.writeachievement(17, 0)
                    }
                }
            }
        }

        layoutManager = LinearLayoutManager(context)
        recycler_menu!!.layoutManager = layoutManager
        val adapter = AchievementAdapter(context!!, upLabelArr)
        recycler_menu!!.adapter = adapter

        return view
    }
}