package com.client.levelsapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardedVideoAd
import java.util.*
import kotlin.collections.ArrayList
import android.R.attr.defaultValue
import android.R.attr.key
import kotlinx.android.synthetic.main.back_dialog.*


class LevelViewFragment : Fragment() {

    var mActivity: Activity? = null
    internal var rewardedVideoAd: RewardedVideoAd? = null
    private var interstitial: InterstitialAd? = null
    internal lateinit var adIRequest: AdRequest


    companion object {
        var homefragment: HomeFragment? = null
        fun getInstance(): HomeFragment {
            homefragment = HomeFragment()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        MobileAds.initialize(context, resources.getString(R.string.admob_app_id))
        adIRequest = AdRequest.Builder().build()
        loadInterstitialAd()

        val view: View = inflater.inflate(R.layout.levelview_fragment, container, false)
        var txt_title_name = view.findViewById(R.id.txt_title_name) as TextView
        var txt_easy = view.findViewById(R.id.txt_easy) as TextView
        var txt_tricky = view.findViewById(R.id.txt_tricky) as TextView
        var txt_highscores = view.findViewById(R.id.txt_highscores) as TextView

        val bundle = this.arguments
        val myValue = bundle!!.getString("title")
        Log.d("title", "myValue")

        txt_title_name.text = myValue?.let { MainActivity.setName(it) }
        var level_no = 0
//        val myValue = this.arguments!!.getString("title")
//        if (bundle!!.getInt("getLevelNumber") != null) {
            level_no = bundle!!.getInt("getLevelNumber")
//        }

        if (level_no >= 4) {
            txt_tricky.visibility = View.GONE
        } else {
            txt_tricky.visibility = View.VISIBLE
        }

        txt_tricky.setOnClickListener {
            var intent = Intent(context, MainActivity::class.java)
            intent.putExtra("redirect", "3")
            intent.putExtra("title", myValue)
            intent.putExtra("level", "tricky")
            intent.putExtra("level_no", "level_no")
            context!!.startActivity(intent)
            displayInterstitial()
        }

        txt_easy.setOnClickListener {
            var intent = Intent(context, MainActivity::class.java)
            intent.putExtra("redirect", "1")
            intent.putExtra("title", myValue)
            intent.putExtra("level", "easy")
            intent.putExtra("level_no", "level_no")
            startActivity(intent)

            displayInterstitial()
        }

        txt_highscores.setOnClickListener {

            var intent = Intent(context, MainActivity::class.java)
            intent.putExtra("redirect", "4")
            intent.putExtra("title", myValue)
            intent.putExtra("level", "easy")

            intent.putExtra("level_no", level_no)
            context!!.startActivity(intent)
            activity!!.finish()
            displayInterstitial()
        }
        return view
    }

    private fun loadInterstitialAd() {
        interstitial = InterstitialAd(context)
        interstitial!!.adUnitId = getString(R.string.interstitial_ad)
        interstitial!!.loadAd(adIRequest)
    }

    private fun displayInterstitial() {
        if (interstitial!!.isLoaded) {
            Log.d("TAG", "displayInterstitial")
            interstitial!!.show()
        }
    }


}