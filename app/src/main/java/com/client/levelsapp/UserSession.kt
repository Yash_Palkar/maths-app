package com.client.levelsapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class UserSession {
    val PREFER_NAME = "AndroidSalesPref"
    var pref: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null
    var _context: Context? = null
    var PRIVATE_MODE = 0
    val IS_USER_LOGIN:String="Is_Login"
    val KEY_NAME:String="User_mail"
    val KEY_CREATED:String="created_at"
    val KEY_UPDATE:String="updated_at"
    val KEY_DAY_TODAY:String=""
    @SuppressLint("CommitPrefEdits")
    constructor(_context: Context?) {
        this._context = _context
        pref = _context!!.getSharedPreferences(PREFER_NAME,PRIVATE_MODE)
        editor = pref?.edit()
    }

    constructor()

    fun loginSession(islogin: Boolean?,name: String?, created_at: String?, updated_at: String, curr_date: String) {

        if (islogin != null) {
            editor!!.putBoolean(IS_USER_LOGIN, islogin)
        }
        editor!!.putString(KEY_NAME, name)
        editor!!.putString(KEY_CREATED, created_at)
        editor!!.putString(KEY_UPDATE, updated_at)
        editor!!.putString(KEY_DAY_TODAY, curr_date)
        editor!!.commit()
    }

    fun isUserLoggedIn(): Boolean {
        pref = _context!!.getSharedPreferences(PREFER_NAME,PRIVATE_MODE)
        var value=pref!!.getBoolean(IS_USER_LOGIN, false)
        return value
    }
    fun getEmail(): String {
        return pref!!.getString(
            KEY_NAME,
            ""
        ).toString()
    }
    fun getCreated(): String {
        return pref!!.getString(
            KEY_CREATED,
            ""
        ).toString()
    }

    }


