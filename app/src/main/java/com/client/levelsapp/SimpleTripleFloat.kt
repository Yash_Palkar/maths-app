package com.client.levelsapp

interface SimpleTripleFloat {
    fun apply(x1: Int, x2: Int, x3: Int, x4: Int): Float
}