package com.client.levelsapp

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.customtabs.BuildConfig
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ToggleButton

class Menu : AppCompatActivity() {
    var back: ImageView?=null
    var table_id: Button? = null
    var formula_id: Button? = null
    var mathtrick_id: Button? = null
    var achiv_id: Button? = null
    var reset_id: Button? = null
    var rateUs: Button? = null
    var share: Button? = null
    var volume: Button? = null
    var moreAps: Button? = null
    var i: Intent? = null
    var isMute:Boolean?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        table_id = findViewById<Button>(R.id.table_id)
        formula_id = findViewById<Button>(R.id.formula_id)
        mathtrick_id = findViewById<Button>(R.id.mathtrick_id)
        achiv_id = findViewById<Button>(R.id.achiv_id)
        reset_id = findViewById<Button>(R.id.reset_id)
        rateUs = findViewById(R.id.rateUs)
        volume = findViewById(R.id.volume)
        share = findViewById(R.id.share)
        moreAps = findViewById(R.id.moreAps)
        back=findViewById<ImageView>(R.id.backarrow)
        var pref = PreferenceManager.getDefaultSharedPreferences(this)

        var isMute = pref.getBoolean("MuteState", false)
        if (isMute == true) {
            volume!!.setBackgroundResource(R.drawable.mutelogo)
        } else {
            volume!!.setBackgroundResource(R.drawable.soundlogo)
        }


        back!!.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, HomeActivity::class.java)


            startActivity(intent)
        })

        table_id!!.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, MainActivity::class.java)
            intent.putExtra("redirect", "5")
            intent.putExtra("tableId", "table_id")
            startActivity(intent)
        })

        formula_id!!.setOnClickListener(View.OnClickListener {
           // var intent = Intent(this, MainActivity::class.java)
           var intent = Intent(this, formulaAllOptions::class.java)
            intent.putExtra("redirect", "6")
            intent.putExtra("formulaId", "formula_id")
            startActivity(intent)
        })
        mathtrick_id!!.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, MainActivity::class.java)
            intent.putExtra("redirect", "7")
            intent.putExtra("mathsTrick", "mathtrick_id")
            startActivity(intent)
        })
        achiv_id!!.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, MainActivity::class.java)
            intent.putExtra("redirect", "8")
            intent.putExtra("achiveId", "achiv_id")
            startActivity(intent)
        })
        reset_id!!.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, MainActivity::class.java)
            intent.putExtra("redirect", "9")
            intent.putExtra("resetId", "reset_id")
            startActivity(intent)
        })

        share!!.setOnClickListener(View.OnClickListener {

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            var shareMessage =
                "\nLet me recommend you this application\n\n" //todo by sachin share app link added ..
            shareMessage =
                shareMessage + "https://play.google.com/store/apps/details?id=com.client.levelsapp&hl=en_IN"+ BuildConfig.APPLICATION_ID + "\n\n"
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            sendIntent.type = "text/plain"
            startActivity(sendIntent)

        })
        rateUs!!.setOnClickListener(View.OnClickListener {

            i = Intent(android.content.Intent.ACTION_VIEW)
            i!!.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.client.levelsapp&hl=en_IN" + BuildConfig.APPLICATION_ID + "\n\n"))
            startActivity(i)

        })

        moreAps!!.setOnClickListener(View.OnClickListener {
            i = Intent(android.content.Intent.ACTION_VIEW)
            i!!.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Techathalon&hl=en_IN"))
            startActivity(i)

        })

        volume!!.setOnClickListener(View.OnClickListener {
            // TODO Auto-generated method stub
            volume!!.setSelected(true);
            var pref = PreferenceManager.getDefaultSharedPreferences(this)

            var isMute = pref.getBoolean("MuteState", false)


            if (isMute != null) {

                if (isMute == true) {
                    // Sound  is disabled

                    volume!!.isSoundEffectsEnabled = false
                    var manager1 = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                    manager1.setStreamMute(AudioManager.STREAM_MUSIC, false)
                    volume!!.setBackgroundResource(R.drawable.soundlogo)
                    var pref = PreferenceManager.getDefaultSharedPreferences(this)
                    var editor = pref.edit()
                    editor
                        .putBoolean("MuteState", false)
                        .apply()

                } else {
                    // Sound is enabled
                    volume!!.isSoundEffectsEnabled = true
                    var manager2 = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                    manager2.setStreamMute(AudioManager.STREAM_MUSIC, true)
                    volume!!.setBackgroundResource(R.drawable.mutelogo)
                    var pref = PreferenceManager.getDefaultSharedPreferences(this)
                    var editor = pref.edit()
                    editor
                        .putBoolean("MuteState", true)
                        .apply()

                }
            } else {

                var manager1 = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                manager1.setStreamMute(AudioManager.STREAM_MUSIC, true)
                volume!!.setBackgroundResource(R.drawable.mutelogo)

                var pref = PreferenceManager.getDefaultSharedPreferences(this)
                var editor = pref.edit()
                editor
                    .putBoolean("MuteState", true)
                    .apply()



            }


        })


    }
}
