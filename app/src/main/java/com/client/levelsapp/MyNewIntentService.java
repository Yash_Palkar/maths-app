package com.client.levelsapp;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

public class MyNewIntentService extends IntentService {
    protected static final  String channelId="Personal_Notification";
    protected static  final String  channelName="Astrology";
    protected static  final String  channel_desc="Astrology Notification";

    private static final int NOTIFICATION_ID = 3;

    public MyNewIntentService() {
        super("MyNewIntentService");
    }

    @Override
    public void onHandleIntent(Intent intent) {


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {


            NotificationChannel notificationChannel= new NotificationChannel(channelId,channelName,NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(channel_desc);

            NotificationManager notificationManager;
            notificationManager = (NotificationManager)this.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }
//
        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(context,channelId)

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,channelId);
        builder.setContentTitle("Maths App");
        builder.setContentText("Its Time To Play");
        builder.setSmallIcon(R.drawable.app_icon);
        builder.setAutoCancel(true);
        //  builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setSound(notificationSoundURI);
        Intent notifyIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 2, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //to be able to launch your activity from the notification
        builder.setContentIntent(pendingIntent);
        Notification notificationCompat = builder.build();
        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
        managerCompat.notify(NOTIFICATION_ID, notificationCompat);
    }
}
