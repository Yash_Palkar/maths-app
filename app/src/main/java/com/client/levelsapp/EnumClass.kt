package com.client.levelsapp

class EnumClass {
    enum class OperationInt(private var text: String) : SimpleCalculation {


        ADDITION("+") {
            override fun apply(x1: Int, x2: Int): Int {
                return x1 + x2
            }
        },
        SUBTRACTION("-") {
            override fun apply(x1: Int, x2: Int): Int {
                return x1 - x2
            }
        },
        MULTIPLICATION("*") {
            override fun apply(x1: Int, x2: Int): Int {
                return x1 * x2
            }
        };

        private fun OperationInt(text: String) {
            this.text = text
        }
        override fun toString(): String {
            return text
        }

    }
    enum class OperationFloat(private var text: String) : SimpleDivision {

        DIVISION("/") {
            override fun apply(x1: Int, x2: Int): Float {
                return (x1 / x2).toFloat()
            }
        };

        private fun OperationFloat(text: String) {
            this.text = text
        }
        override fun toString(): String {
            return text
        }
    }
    enum class OperationDoubleInt(private var text: String) : SimpleDoubleCalculation {
        DOUBLE_ADD("+ +") {
            override fun apply(x1: Int, x2: Int, x3: Int): Int {
                return x1 + x2 + x3
            }
        },
        DOUBLE_SUBTRACT("- -") {
            override fun apply(x1: Int, x2: Int, x3: Int): Int {
                return x1 - x2 - x3
            }
        },
        DOUBLE_MULTIPLY("* *") {
            override fun apply(x1: Int, x2: Int, x3: Int): Int {
                return x1 * x2 * x3
            }
        },
        ADD_SUBTRACT("+ -") {
            override fun apply(x1: Int, x2: Int, x3: Int): Int {
                return x1 + x2 - x3
            }
        },
        ADD_MULTIPLY("+ *") {
            override fun apply(x1: Int, x2: Int, x3: Int): Int {
                return x1 + x2 * x3
            }
        },
        SUBTRACT_MULTIPLY("- *") {
            override fun apply(x1: Int, x2: Int, x3: Int): Int {
                return x1 - x2 * x3
            }
        };

        private fun OperationDoubleInt(text: String) {
            this.text = text
        }
        override fun toString(): String {
            return text
        }

    }
    enum class OperationDoubleFloat(private var text: String) : SimpleDoubleDivision {
        ADD_DIVIDE("+ /") {
            override fun apply(x1: Int, x2: Int, x3: Int): Float {
                return (x1 + x2 / x3).toFloat()
            }
        },
        SUBTRACT_DIVIDE("- /") {
            override fun apply(x1: Int, x2: Int, x3: Int): Float {
                return (x1 - x2 / x3).toFloat()
            }
        },
        MULTIPLY_DIVIDE("* /") {
            override fun apply(x1: Int, x2: Int, x3: Int): Float {
                return (x1 * x2 / x3).toFloat()
            }
        },
        DOUBLE_DIVIDE("/ /") {
            override fun apply(x1: Int, x2: Int, x3: Int): Float {
                return (x1 / x2 / x3).toFloat()
            }
        };

        private fun OperationDoubleFloat(text: String) {
            this.text = text
        }
        override fun toString(): String {
            return text
        }
    }
    enum class OperationTripleInt (private var text: String) : SimpleTripleInt {

        ADD_SUBTRACT_MULTIPLY("+ - *") {
            override fun apply(x1: Int, x2: Int, x3: Int, x4: Int): Int {
                return ((x1 + x2) - x3) * x4
            }
        };
        private fun OperationTripleInt(text: String) {
            this.text = text
        }
        override fun toString(): String {
            return text
        }
    }
    enum class OperationTripleFloat(private var text: String) : SimpleTripleFloat {
        SUBTRACT_MULTIPLY_DIVIDE("- * /") {
            override fun apply(x1: Int, x2: Int, x3: Int, x4: Int): Float {
                return (((x1 - x2) * x3) / x4).toFloat()
            }
        },
        MULTIPLY_DIVIDE_ADD("* / +") {
            override fun apply(x1: Int, x2: Int, x3: Int, x4: Int): Float {
                return (((x1 * x2) / x3) + x4).toFloat()
            }
        };

        private fun OperationTripleFloat(text: String) {
            this.text = text
        }
        override fun toString(): String {
            return text
        }
    }
    enum class OperationBodmass(private var text: String) : SimpleBodmass {

        BODMASS("+ - * /") {
            override fun apply(x1: Int, x2: Int, x3: Int, x4: Int, x5: Int): Float {
                return ((((x1 + x2) - x3) * x4) / x5).toFloat()
            }
        };
        private fun OperationTripleInt(text: String) {
            this.text = text
        }
        override fun toString(): String {
            return text
        }
    }
}