package com.client.levelsapp

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView

class PrivacyFragment : Fragment() {
    var mActivity: Activity? = null

    companion object {
        var homefragment: TrickyFragment? = null
        fun getInstance(): TrickyFragment {
            homefragment = TrickyFragment()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.fragment_privacy, container, false)
        var webview = view.findViewById(R.id.webview) as WebView
        webview.loadUrl("https://gathademo.com/privacypolicy/?app_id=16")
        mActivity!!.setContentView(webview)
        return view
    }

}