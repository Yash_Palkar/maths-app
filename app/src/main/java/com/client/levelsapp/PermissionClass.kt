package com.client.levelsapp

import android.Manifest
import android.R
import android.app.Activity
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.widget.Toast

class PermissionClass {

    companion object{ var runtimePermissionHelper: PermissionClass? = null}

    var PERMISSION_REQUEST_CODE = 1
    private var activity: Activity? = null
    val PERMISSION_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
    val PERMISSION_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE
    private var requiredPermissions: ArrayList<String>? = null
    private var ungrantedPermissions = ArrayList<String>()


    constructor(activity: Activity) {
        this.activity = activity
    }


    fun getInstance(activity: Activity): PermissionClass {
        if (runtimePermissionHelper == null) {
            runtimePermissionHelper = PermissionClass(activity)
        }
        return runtimePermissionHelper!!
    }


    private fun initPermissions() {
        requiredPermissions = ArrayList()
        requiredPermissions!!.add(PERMISSION_WRITE_EXTERNAL_STORAGE)
        requiredPermissions!!.add(PERMISSION_READ_EXTERNAL_STORAGE)
        //Add all the required permission in the list
    }

    fun requestPermissionsIfDenied() {
        ungrantedPermissions = getUnGrantedPermissionsList()
        if (canShowPermissionRationaleDialog()) {
            showMessageOKCancel("Please grant permission so that we can show you better results",
                DialogInterface.OnClickListener { dialog, which -> askPermissions() })
            return
        }
        askPermissions()
    }

    fun requestPermissionIfDenied(permission: String) {
        if (canShowPermissionRationaleDialog(permission)) {
            showMessageOKCancel("Please grant permission so that we can show you better results",
                DialogInterface.OnClickListener { dialog, which -> askPermission(permission) })
            return
        }
        askPermission(permission)
    }

    fun setActivity(activity: Activity) {
        this.activity = activity
    }

    fun canShowPermissionRationaleDialog(): Boolean {
        var shouldShowRationale = false
        for (permission in ungrantedPermissions) {
            val shouldShow = ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission)
            if (shouldShow) {
                shouldShowRationale = true
            }
        }
        return shouldShowRationale
    }

    fun canShowPermissionRationaleDialog(permission: String): Boolean {
        var shouldShowRationale = false
        val shouldShow = ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission)
        if (shouldShow) {
            shouldShowRationale = true
        }
        return shouldShowRationale
    }

    private fun askPermissions() {
        if (ungrantedPermissions!!.size > 0) {
            ActivityCompat.requestPermissions(
                activity!!,
                ungrantedPermissions.toArray(arrayOfNulls(ungrantedPermissions.size)),
                PERMISSION_REQUEST_CODE
            )
        }
    }

    private fun askPermission(permission: String) {
        ActivityCompat.requestPermissions(activity!!, arrayOf(permission), PERMISSION_REQUEST_CODE)
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(activity!!)
            .setMessage(message)
            .setPositiveButton(R.string.ok, okListener)
            .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialogInterface, i ->
                Toast.makeText(
                    activity,
                    "Please grant permission so that we can show you better results",
                    Toast.LENGTH_SHORT
                ).show()
//                val intent = Intent(activity, ErrorActivity::class.java)
//                intent.putExtra("permissions_denied", true)
//                activity!!.startActivity(intent)
                activity!!.finish()
            })
            .create()
            .show()
    }


    fun isAllPermissionAvailable(): Boolean {
        var isAllPermissionAvailable = true
        initPermissions()
        for (permission in requiredPermissions!!) {
            if (ContextCompat.checkSelfPermission(activity!!, permission) != PackageManager.PERMISSION_GRANTED) {
                isAllPermissionAvailable = false
                break
            }
        }
        return isAllPermissionAvailable
    }

    fun getUnGrantedPermissionsList(): ArrayList<String> {
        val list = ArrayList<String>()
        for (permission in requiredPermissions!!) {
            val result = ActivityCompat.checkSelfPermission(activity!!, permission)
            if (result != PackageManager.PERMISSION_GRANTED) {
                list.add(permission)
            }
        }
        return list
    }

    fun isPermissionAvailable(permission: String): Boolean {
        var isPermissionAvailable = true
        if (ContextCompat.checkSelfPermission(activity!!, permission) != PackageManager.PERMISSION_GRANTED) {
            isPermissionAvailable = false
        }
        return isPermissionAvailable
    }
}