package com.client.levelsapp

class CalculateMethods {
    companion object {

        fun calculateInt(op: EnumClass.OperationInt, x1: Int, x2: Int): Int {
            return (op.apply(x1, x2))
        }

        fun calculateFloat(op: EnumClass.OperationFloat, x1: Int, x2: Int): Float {
            return (op.apply(x1, x2))
        }

        fun calculateDoubleInt(op: EnumClass.OperationDoubleInt, x1: Int, x2: Int, x3: Int): Int {
            return (op.apply(x1, x2, x3))
        }

        fun calculateDoubleFloat(op: EnumClass.OperationDoubleFloat, x1: Int, x2: Int, x3: Int): Float {
            return (op.apply(x1, x2, x3))
        }

        fun calculateTripleInt(op: EnumClass.OperationTripleInt, x1: Int, x2: Int, x3: Int, x4: Int): Int {
            return (op.apply(x1, x2, x3, x4))
        }

        fun calculateTripleFloat(
            op: EnumClass.OperationTripleFloat,
            x1: Int,
            x2: Int,
            x3: Int,
            x4: Int
        ): Float {
            return (op.apply(x1, x2, x3, x4))
        }

        fun calculateBodmass(
            op: EnumClass.OperationBodmass,
            x1: Int,
            x2: Int,
            x3: Int,
            x4: Int,
            x5: Int
        ): Float {
            return (op.apply(x1, x2, x3, x4, x5))
        }
    }
}