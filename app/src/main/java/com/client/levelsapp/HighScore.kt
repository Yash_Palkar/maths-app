package com.client.levelsapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

class HighScore : Fragment() {

    var mActivity: Activity? = null
    var prefConfig: SharedPrefrenceClass? = null
    var btn_dismiss: TextView? = null

    companion object {
        var homefragment: HighScore? = null
        fun getInstance(): HighScore {
            homefragment = HighScore()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefConfig = SharedPrefrenceClass(context!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.highscore_dialog, container, false)
        var level_title = view.findViewById(R.id.level_title) as TextView
        var txt_easy_score = view.findViewById(R.id.txt_easy_score) as TextView
        var txt_tricky_score = view.findViewById(R.id.txt_tricky_score) as TextView
        btn_dismiss = view.findViewById(R.id.btn_dismiss) as TextView
        val myValue = this.arguments!!.getString("title")
        val myLevel_no = this.arguments!!.getInt("level_no")
        level_title.text = myValue?.let { MainActivity.setName(it) }

//         to do by sachin for hiding the score in double activity

        if (myLevel_no in 4..7) {
            txt_easy_score.text =
                "Easy Level : " + prefConfig!!.readLevelscoreeasy(myLevel_no!!).toString()

            txt_tricky_score.setVisibility(View.GONE);
        } else {
            txt_tricky_score.text =
                "Tricky Level : " + prefConfig!!.readLevelscoretricky(myLevel_no!!).toString()


            txt_easy_score.text =
                "Easy Level : " + prefConfig!!.readLevelscoreeasy(myLevel_no!!).toString()

        }
//

//        txt_easy_score.text =
//            "Easy Level : " + prefConfig!!.readLevelscoreeasy(myLevel_no!!).toString()
//        txt_tricky_score.text =
//            "Tricky Level : " + prefConfig!!.readLevelscoretricky(myLevel_no!!).toString()

        btn_dismiss!!.setOnClickListener {
            var intent = Intent(context, MainActivity::class.java)
            intent.putExtra("redirect", "0")
            intent.putExtra("title", myValue)
            context!!.startActivity(intent)
            activity!!.finish()

        }
//        txt_tricky.setOnClickListener {
//            var intent = Intent(context, MainActivity::class.java)
//            intent.putExtra("redirect", "3")
//            intent.putExtra("title", myValue)
//            intent.putExtra("level", "tricky")
//            context!!.startActivity(intent)
//        }
//
//        txt_easy.setOnClickListener {
//            var intent = Intent(context, MainActivity::class.java)
//            intent.putExtra("redirect", "1")
//            intent.putExtra("title", myValue)
//            intent.putExtra("level", "easy")
//            context!!.startActivity(intent)
//        }

        return view
    }
}